<?php
	// require_once('header.php');
	
	if(isset($_POST['edit'])){
      if(!empty($_POST['password_lama']) && !empty($_POST['password_baru']) && !empty($_POST['password_baru_lagi'])){
		$epassword_lama = mysqli_real_escape_string($conn, $_POST['password_lama']);
        $epassword_lama = enkripsi_password($epassword_lama);
		$epassword_baru = mysqli_real_escape_string($conn, $_POST['password_baru']);
        $epassword_baru = enkripsi_password($epassword_baru);
		$epassword_baru_lagi = mysqli_real_escape_string($conn, $_POST['password_baru_lagi']);
		$epassword_baru_lagi = enkripsi_password($epassword_baru_lagi);
		
		// password benar
		if($epassword_lama==$password_lama){
			if($epassword_baru==$epassword_baru_lagi){
				if($epassword_baru!=$password_lama){
					$result = mysqli_query($conn, 
						  "UPDATE users
						  SET password='".$epassword_baru."'
						  WHERE username='".$username."'");
						if(!$result){
							$warning="Gagal terkoneksi ke database".$conn->error;
						}else{
							$success="Password berhasil diubah.";
							header('Location:../logout.php');
						}
				}else{
					$warning="Password baru tidak boleh sama dengan password sebelumnya";
				}
			}else{
				$warning="Password Baru dan Password Baru (Lagi) tidak sama";
			}
		}else{
			$warning="Password lama salah";
		}
      }else{
		$warning = "Harap isi semua isian yang ada";
	  }
    }
?>
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="index.php">Dashboard</a></li>
	  <li class="active">Ubah Password</li>
	</ol>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
			<h3 class="panel-title">Ubah Password</h3>
		  </div>
		  <div class="panel-body">
			<?php if(isset($success)) echo '<div class="alert alert-success" role="alert">'.$success.'</div>' ?>
			<?php if(isset($warning)) echo '<div class="alert alert-danger" role="alert">'.$warning.'</div>' ?>
			<form method="POST" class="form-horizontal form-label-left" autocomplete="on" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Password Lama</label>
					<div class="input-group">
						<input type="password" name="password_lama" class="form-control" placeholder="*****************" aria-describedby="basic-addon2">
						<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Password Baru</label>
					<div class="input-group">
						<input type="password" name="password_baru" class="form-control" placeholder="*****************" aria-describedby="basic-addon2">
						<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Password Baru (Lagi)</label>
					<div class="input-group">
						<input type="password" name="password_baru_lagi" class="form-control" placeholder="*****************" aria-describedby="basic-addon2">
						<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					</div>
				</div>
				
				<div class="ln_solid"></div>

				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
					  <button type="submit" name="edit" class="btn btn-success">Submit</button>
					</div>
				</div>

			</form>
		  </div>
		</div>
	</div>
</div>
 