<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<?php
if (isset($_GET['nis'])) {
	$query = "SELECT * FROM siswa WHERE nis=".$_GET['nis']."";
	$result = $conn->query($query);
	while ($dt = $result->fetch_object()) {
		$query2 = "SELECT tahun_ajaran FROM kelas WHERE kd_kelas='".$dt->kelas_diterima."'";
		$result2 = $conn->query($query2);
		$kls = $result2->fetch_object();
		// var_dump($kls);
		$tahun_ajaran = $kls->tahun_ajaran;
		$nis = $dt->nis;
		$nisn = $dt->nisn;
		$nama = $dt->nama;
		// $tempat_lahir = $dt->tempat_lahir;
		// $tanggal_lahir = $dt->tanggal_lahir;
		// $agama = $dt->agama;
		// $desa = $dt->desa;
		// $kecamatan = $dt->kecamatan;
		// $kabupaten = $dt->kabupaten;
		$jenis_kel = $dt->jenis_kel;
		// $anak_ke = $dt->anak_ke;
		// $dari_bersaudara = $dt->dari_bersaudara;
		// $status_anak = $dt->status_anak;
		// $telepon = $dt->telepon;
		// $nokk = $dt->nokk;
		// $nik_wali = $dt->nik_wali;
		$kelas_diterima = $dt->kelas_diterima;
		$tanggal_diterima = $dt->tanggal_diterima;
		// $sekolah_asal = $dt->sekolah_asal;
		// $alamat_sekolah_asal = $dt->alamat_sekolah_asal;		
	}
}


if (isset($_POST['simpan'])){
	$status_post = test_input($_POST['status']);
	if($status_post==''){
		$error_status='Status harus diisi';
		$valid_status=FALSE;
	}else{
		$valid_status=TRUE;	
	}

	$tahun_ajaran=test_input($_POST['tahun_ajaran']);	
	if($tahun_ajaran==''){
		$error_tahun_ajaran='Tahun Pelajaran harus diisi';
		$valid_tahun_ajaran=FALSE;
	}else{
		$valid_tahun_ajaran=TRUE;	
	}

	$panjang_nis = 4;	
	$nis=test_input($_POST['nis']);	
	if($nis==''){	
		$error_nis='NIS harus diisi';
		$valid_nis=FALSE;
	}elseif (strlen($nis)!=$panjang_nis) {
		$error_nis='Harap cek format NIS. (cth: 1234)';
		$valid_nis = FALSE;
	}else{
		$valid_nis=TRUE;
	}

	$nisn=test_input($_POST['nisn']);	
	if($nisn==''){
		$error_nisn='NISN harus diisi';
		$valid_nisn=FALSE;
	}else{
		$valid_nisn=TRUE;		
	}

	$query = "SELECT nis, nisn FROM siswa";
	$result1 = $conn->query($query);
	while ($dt=$result1->fetch_object()) {		
		if ($dt->nis == $nis) {
			$error_nis='NIS Sudah Digunakan';
			$valid_nis = FALSE;
		}elseif ($dt->nisn == $nisn) {
			$error_nisn ='NIS Sudah Digunakan';
			$valid_nisn = FALSE;
		}
	}

	$nama=test_input($_POST['nama']);
	if($nama==''){
		$error_nama='Nama Lengkap harus diisi';
		$valid_nama=FALSE;
	}else{
		$valid_nama=TRUE;			
	}

	if (isset($_POST['jenis_kel'])) {
		$jenis_kel=test_input($_POST['jenis_kel']);
	// 		if($jenis_kel==''){
	// 			$error_jenis_kel='Pilih jenis kelamin';
	// 			$valid_jenis_kel=FALSE;
	// 		}else{
		$valid_jenis_kel=TRUE;
	// 		}
	}else {
		$error_jenis_kel='Pilih Jenis Kelamin';
		$valid_jenis_kel=FALSE;
	}

	$kelas_diterima=test_input($_POST['kelas_diterima']);
	if($kelas_diterima=='' || $kelas_diterima=='none'){
		$error_kelas_diterima='Kelas Diterima harus diisi';
		$valid_kelas_diterima=FALSE;
	}else{
		$valid_kelas_diterima= TRUE; 			
	}

	$tanggal_diterima=test_input($_POST['tanggal_diterima']);
	if($tanggal_diterima==''){
		$error_tanggal_diterima='Tanggal harus diisi';
		$valid_tanggal_diterima=FALSE;
	}else{
		$valid_tanggal_diterima = TRUE; 			
	}

	$sekolah_asal=test_input($_POST['sekolah_asal']);
		// if($sekolah_asal==''){
		// 	$error_sekolah_asal='harus diisi';
		// 	$valid_sekolah_asal=FALSE;
		// }else{
	$valid_sekolah_asal = TRUE; 			
		// }

	$alamat_sekolah_asal=test_input($_POST['alamat_sekolah_asal']);
		// if($alamat_sekolah_asal==''){
		// 	$error_alamat_sekolah_asal='harus diisi';
		// 	$valid_alamat_sekolah_asal=FALSE;
		// }else{
	$valid_alamat_sekolah_asal = TRUE;			
		// }

	$sekolah_tujuan=test_input($_POST['sekolah_tujuan']);
		// if($sekolah_asal==''){
		// 	$error_sekolah_asal='harus diisi';
		// 	$valid_sekolah_asal=FALSE;
		// }else{
	$valid_sekolah_tujuan = TRUE; 			
		// }

	$alamat_sekolah_tujuan=test_input($_POST['alamat_sekolah_tujuan']);
		// if($alamat_sekolah_asal==''){
		// 	$error_alamat_sekolah_asal='harus diisi';
		// 	$valid_alamat_sekolah_asal=FALSE;
		// }else{
	$valid_alamat_sekolah_tujuan = TRUE;			
		// }

	$keterangan=test_input($_POST['keterangan']);
		// if($alamat_sekolah_asal==''){
		// 	$error_alamat_sekolah_asal='harus diisi';
		// 	$valid_alamat_sekolah_asal=FALSE;
		// }else{
	$valid_keterangan = TRUE;			
		// }

	$result=false;
	if($valid_tahun_ajaran && $valid_nis && $valid_nisn && $valid_nama && $valid_jenis_kel && $valid_kelas_diterima && $valid_tanggal_diterima && $valid_sekolah_asal && $valid_alamat_sekolah_asal && $valid_sekolah_tujuan && $valid_alamat_sekolah_tujuan && $valid_keterangan && $valid_status) {

		$tahun_ajaran = $conn->real_escape_string($tahun_ajaran);
		$nis = $conn->real_escape_string($nis);
		$nisn = $conn->real_escape_string($nisn);
		$nama = $conn->real_escape_string($nama);
		$jenis_kel = $conn->real_escape_string($jenis_kel);
		$kelas_diterima = $conn->real_escape_string($kelas_diterima);
		$tanggal_diterima = $conn->real_escape_string($tanggal_diterima);
		$sekolah_asal = $conn->real_escape_string($sekolah_asal);
		$alamat_sekolah_asal = $conn->real_escape_string($alamat_sekolah_asal);	
		$sekolah_tujuan = $conn->real_escape_string($sekolah_tujuan);
		$alamat_sekolah_tujuan = $conn->real_escape_string($alamat_sekolah_tujuan);	
		$keterangan = $conn->real_escape_string($keterangan);
		$status = $conn->real_escape_string($status_post);

		// $query="INSERT INTO siswa (nis,nisn, nama, jenis_kel, kelas_diterima, tanggal_diterima) VALUES ('$nis','$nisn', '$nama', '$jenis_kel', '$kelas_diterima', '$tanggal_diterima') ";
		// $result = $conn->query($query);

		// $query1 = "INSERT INTO di_kelas (nis, kd_kelas, tahun_ajaran) VALUES('$nis', '$kelas_diterima', '$tahun_ajaran')";
		// $result1 = $conn->query($query1);
		// var_dump($result1);die();

		$query2="INSERT INTO mutasi (tahun_ajaran, nis, status, sekolah_asal, alamat_sekolah_asal, sekolah_tujuan, alamat_sekolah_tujuan, tanggal, keterangan) VALUES ('$tahun_ajaran', '$nis', '$status', '$sekolah_asal', '$alamat_sekolah_asal', '$sekolah_tujuan', '$alamat_sekolah_tujuan', '$tanggal_diterima','$keterangan')";
		$result2=$conn->query($query2);
		// s 

		if ($result2) {
			echo "<script>alert('Data Berhasil')</script>";
			echo "<script>window.location.replace('".base_url('kesiswaan/mutasi/mutasi_siswa.php')."')</script>";
		}
	}
}

function test_input($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
?>

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Daftar Mutasi</h2>
				<div class="clearfix"></div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<p class="text-muted font-20 m-b-50">
						Menadata kepindahaan sekolah siswa<br>
						*Siswa Masuk <br>
						*Siswa Keluar
					</p>
					<div class="col-md-10">
						<button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success" id="btn-tbh-guru"
						data-toggle="tooltip" title="Menambahkan Guru"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah
					</button>
				</div>
			</div>
		</div>
		<?php
		require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
		$db = new mysqli($db_host, $db_user, $db_pass, $db_name);
		if ($db->connect_errno) {
			die("Could not connet to database:<br/> ".$db->connect_error);
		}
		$query = "SELECT * FROM mutasi";
		$query = "SELECT * FROM siswa JOIN mutasi ON siswa.nis=mutasi.nis ";
		$result = $db->query($query);
		$i = 1;
		?>
		<table id="datatable" class="table table-striped table-bordered">
          <thead >
            <tr >
              <th style=" text-align: center;" width="5%">No</th>
              <th style=" text-align: center;" width="10%">NIS</th>
              <th style=" text-align: center;" width="15%">Nama</th>
              <th style=" text-align: center;" width="10%">Status</th>
              <th style=" text-align: center;" width="15%%">Keterangan</th>
              <th style=" text-align: center;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while ($row = $result->fetch_object())
            {
              echo "<tr>";
              echo "<td style=' text-align: center;'>".$i."</td>";
              echo "<td style=' text-align: center;''>".$row->nis."</td>";
              echo "<td>".$row->nama."</td>";
              echo "<td>".$row->status."</td>";
              echo "<td>".$row->keterangan."</td>";
              echo "<td style='text-align: center;' colspan='2'>";
              echo "</td>";
              echo "</tr>";
              $i++;
            }
            ?>
          </tbody>
        </table>


		

	</div>
</div>
</div>
</div>

<!-- ======================================================================= -->
<!-- MODAL GURU -->
<!-- ======================================================================= -->
<div class="modal fade" id="modalTambah" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times</button>
				<h4 class="modal-title">Tambah Status Mutasi Siswa</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" method="post" id="form-login">

					<div class="box-body">
						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Status *</label>
							<div class="col-sm-8">
								<select class="form-control" name="status">
									<option value="masuk">Masuk</option>
									<option value="keluar">Keluar</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="control-label col-md-4 col-sm-4 col-xs-12" >NIS *</label>
						<div class="col-sm-3">
							<input class="form-control" type="text" id="nis" name="nis" maxlength="<?php echo $jml_kar_nis?>" placeholder="Contoh : 6356" value="<?php if(isset($nis)){echo $nis;} ?>" <?php if (isset($_GET['nis'])) {echo "readonly";} ?>/>
							<?php
							if(isset($error_nis)){
								echo '<span id="helpBlock" class="text-danger">'.$error_nis.'</span>';
							}
							?>
						</div>
						<label class="control-label col-md-1 col-sm-2 col-xs-12" >NISN*</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="nisn" maxlength="<?php echo $jml_kar_nisn?>" 
							placeholder="Contoh : 0047363119" value="<?php if(isset($nisn)){echo $nisn;} ?>" <?php if (isset($_GET['nis'])) {echo "readonly";} ?>/>
							<?php
							if(isset($error_nisn)){
								echo '<span id="helpBlock" class="text-danger">'.$error_nisn.'</span>';
							}
							?>
						</div>
					</div>

					<div class="form-group  form-group-sm">
						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama">Nama Lengkap
							<span class="required">*</span></label>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" maxlength="<?php echo $jml_kar_nama?>" value="<?php if(isset($nama)){echo $nama;} ?>"/>
								<?php
								if(isset($error_nama)){
									echo '<span id="helpBlock" class="text-danger">'.$error_nama.'</span>';
								}
								?>
							</div>
						</div>

					<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Jenis Kelamin <span class="required">*</span></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<div class="col-md-12" style="padding: 0">
									<input type="radio" id="jenis_kel" name="jenis_kel" value="L" <?php 
									if (isset($jenis_kel) && $jenis_kel == "L") {
									 	echo "checked";
									 } ?>/> Laki-Laki 
									<input type="radio" id="jenis_kel" name="jenis_kel" value="P" <?php 
									if (isset($jenis_kel) && $jenis_kel == "P") {
									 	echo "checked";
									 } ?>/> Perempuan									
								</div>																
								<?php
									if(isset($error_jenis_kel)){
										echo '<span id="helpBlock" class="text-danger">'.$error_jenis_kel.'</span>';
									}
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="tahun_ajaran_masuk">Tahun Pelajaran
								<span class="required">*</span></label>
								<div class="col-md-7 col-sm-7 col-xs-12">
									<input type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control col-md-7 col-xs-12"  placeholder="____/____" value="<?php if(isset($tahun_ajaran)){echo $tahun_ajaran;} ?>"/>
									<?php
									if(isset($error_tahun_ajaran)){
										echo '<span id="helpBlock" class="text-danger">'.$error_tahun_ajaran.'</span>';
									}
									?>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12" >Kelas Diterima *</label>
								<div class="col-sm-7">
									<select class="form-control" id="kelas_diterima" name="kelas_diterima" value="<?php if (isset($kelas_diterima)){echo $kelas_diterima; } ?>">
										<option value="none"> -- Pilih Kelas -- </option>
									</select>
									<?php
									if(isset($error_kelas_diterima)){
										echo '<span id="helpBlock" class="text-danger">'.$error_kelas_diterima.'</span>';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12">Tanggal *</label>&nbsp;<span <?php if(isset($error_tanggal_diterima)) echo $error_tanggal_diterima;?></span>
								<div class="col-sm-7">
									<input class="form-control" type="date" name="tanggal_diterima" value="<?php if(isset($tanggal_diterima)) {echo $tanggal_diterima;} ?>">
								</div>
							</div>
							<div class="form-group form-group-sm">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Pindah dari</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="sekolah_asal" placeholder="Nama Sekolah Asal" maxlength="<?php echo $jml_kar_nama_sekolah_asal ?>" value="<?php if(isset($sekolah_asal)){echo $sekolah_asal;} ?>" />
									<?php
			                        if(isset($error_sekolah_asal)){
											echo '<span id="helpBlock" class="text-danger">'.$error_sekolah_asal.'</span>';
										}
			                        ?>
								</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat Sekolah Asal *</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="alamat_sekolah_asal" placeholder="Alamat Sekolah Tujuan" maxlength="<?php echo $jml_kar_alamat_sekolah_asal ?>" value="<?php if(isset($alamat_sekolah_asal)){echo $alamat_sekolah_asal;} ?>" />
									<?php
			                        if(isset($error_alamat_sekolah_asal)){
											echo '<span id="helpBlock" class="text-danger">'.$error_alamat_sekolah_asal.'</span>';
										}
			                        ?>
								</div>
						<!-- </div> -->

						</div>
							<div class="form-group form-group-sm">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Pindah ke</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="sekolah_tujuan" placeholder="Nama Sekolah Tujuan" maxlength="<?php echo $jml_kar_nama_sekolah_tujuan ?>" value="<?php if(isset($sekolah_tujuan)){echo $sekolah_tujuan;} ?>" />
									<?php
			                        if(isset($error_sekolah_tujuan)){
											echo '<span id="helpBlock" class="text-danger">'.$error_sekolah_tujuan.'</span>';
										}
			                        ?>
								</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat Sekolah Tujuan *</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="alamat_sekolah_tujuan" placeholder="Alamat Sekolah Asal" maxlength="<?php echo $jml_kar_alamat_sekolah_tujuan ?>" value="<?php if(isset($alamat_sekolah_tujuan)){echo $alamat_sekolah_tujuan;} ?>" />
									<?php
			                        if(isset($error_alamat_sekolah_tujuan)){
											echo '<span id="helpBlock" class="text-danger">'.$error_alamat_sekolah_tujuan.'</span>';
										}
			                        ?>
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12">Keterangan *</label>&nbsp;<span <?php if(isset($error_keterangan)) echo $error_keterangan;?></span>
								<div class="col-sm-7">
									<textarea name="keterangan" rows="5" cols="30" maxlength="100"><?php if(isset($keterangan)) {echo $keterangan;} ?></textarea>
								</div>
							</div>
						<!-- </div> -->
						<div class="modal-footer">
							<div class="col-sm-12">
								<div class="form-group">
									<button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
									<button type="submit" class="btn btn-primary" name="simpan">Simpan</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

<!-- 
		<html lang="en">
		<head>
		</head>
		</html>

 -->

<script type="text/javascript">
	// alert("sdasd");
	if ($("#tahun_ajaran").val()!="") {
		var thn = $("#tahun_ajaran").val();		
		var nis = $("#nis").val();
		// alert(nis);
		$.ajax({
			url:"../siswa/ajax_func/ajax_func.php?listkelas=1&thn="+thn+"&nis="+nis,
			type:"GET",
			dataType:"html",
			
			beforeSend: function(){
				$("#kelas_diterima").html('Loading...');
			},
			success: function(data){
				$("#kelas_diterima").html(data);
			},
			error: function(){
				$("#kelas_diterima").html("");
			}
		});
	}		
</script>
<script type="text/javascript">	
	$("#tahun_ajaran").blur(function(){		
		if($("#tahun_ajaran").val()==undefined){
			var thn='';
		}else{
			var thn = $("#tahun_ajaran").val();
		}		
		$.ajax({
			url:"../siswa/ajax_func/ajax_func.php?listkelas=1&thn="+thn,
			type:"GET",
			dataType:"html",
			
			beforeSend: function(){
				$("#kelas_diterima").html('Loading...');
			},
			success: function(data){
				$("#kelas_diterima").html(data);
			},
			error: function(){
				$("#kelas_diterima").html("");
			}
		});
	});

	if($("#tahun_ajaran").val()!=null){		
		var thn = $("#tahun_ajaran").val();		
		var split = thn.split("/");
		
		if (split[0]!=null) {			
			var min = new Date();				
			min = split[0]+'-01-01';				
			document.getElementById("tanggal_diterima").setAttribute("min", min);
		}if(split[1]!=null){
			var max = new Date();
			max = split[1]+'-12-31';
			document.getElementById("tanggal_diterima").setAttribute("max", max);
		}
	}

	$("#tahun_ajaran").change(function(){
		if($("#tahun_ajaran").val()==undefined){
			var thn='';
		}else{
			var thn = $("#tahun_ajaran").val();
			var split = thn.split("/");
			if (split[0]!="") {
				var min = new Date();				
				min = split[0]+'-01-01';				
				document.getElementById("tanggal_diterima").setAttribute("min", min);
			}if(split[1]!=""){
				var max = new Date();
				max = split[1]+'-12-31';
				document.getElementById("tanggal_diterima").setAttribute("max", max);
			}
		}		
	})
</script>
		<?php
		include("../footer.php");

		?>
