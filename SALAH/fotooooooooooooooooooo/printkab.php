<?php 
	require_once('fpdf181/fpdf.php');

	$tanggal_awal = $_GET['awal'];
	$tanggal_akhir = $_GET['akhir'];

	$db_host="localhost";
	$db_username="root";
	$db_password="";
	$db_database="sirawanlaka";
	$con = mysqli_connect($db_host, $db_username, $db_password, $db_database);
	if(mysqli_connect_errno()) {
	    die('Could not connect to database : <br/>'.$mysqli_connect_error());
	}

	if ($tanggal_awal == '' && $tanggal_akhir == '') {
		$query  = "SELECT * FROM jalan WHERE status='Y' AND id_kategori = '3' ORDER BY tanggal ASC";
	} else {
		$query  = "SELECT * FROM jalan WHERE status='Y' AND id_kategori = '3' AND tanggal >= '$tanggal_awal' AND tanggal <= '$tanggal_akhir' ORDER BY tanggal ASC";
	}
	$jln_query = $con->query($query);
	$pdf = new FPDF();
	$pdf->AddPage();	
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(85);$pdf->Cell(23,10,'SISTEM INFORMASI PENGELOLAAN DATA DAERAH RAWAN KECELAKAAN',0,1,'C');
	$pdf->Cell(85);$pdf->Cell(23,10,'DI KABUPATEN BATANG',0,1,'C');
	$pdf->setLineWidth(1.3);
	$pdf->Line(17,35,190,35);

	$pdf->Cell(85);$pdf->Cell(20,32,'RIWAYAT PENDANAAN',0,1,'C');

	$pdf->SetFont('Arial','B',10);
	$pdf->setLineWidth(0.3);
	$pdf->setY(60);
	$pdf->Cell(5);
	$pdf->Cell(7,10,'NO',1,0);
	$pdf->Cell(23,10,'TANGGAL',1,0);
	$pdf->Cell(150,10,'LOKASI',1,0);
	$pdf->Ln();

	$i = 1;
	while($hasil = $jln_query->fetch_object()){
		$pdf->Cell(5);
		$pdf->Cell(7,10,$i,1,0);
		$pdf->Cell(23,10,$hasil->tanggal,1,0);
		$pdf->Cell(150,10,$hasil->lokasi,1,0);
		$pdf->Ln();
		$i++;
	}
	$pdf->Output();
	// $sapi_type = php_sapi_name();
	// if (substr($sapi_type, 0, 3) == 'cli') {
	//     echo "You are using CLI PHP\n";
	// } else {
	//     echo "You are not using CLI PHP\n";
	// }