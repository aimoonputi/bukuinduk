<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>


	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title">DATA SISWA</h3>
			  </div>
			  <div class="panel-body">
				<div class="row">
				  <div class="col-xs-12 col-md-6">
					<a href="#" class="thumbnail">
					 <!--  <?php
						if (file_exists($folder_foto_masuk.''.$nis.'.jpg')) {
							echo '<img src="assets/images/foto_masuk/'.$nis.'.jpg?'.time().'" />';
						}elseif (file_exists($folder_foto_masuk.''.$nis.'.jpeg')) {
							echo '<img src="assets/images/foto_masuk/'.$nis.'.jpeg?'.time().'" />';
						}elseif (file_exists($folder_foto_masuk.''.$nis.'.png')) {
							echo '<img src="assets/images/foto_masuk/'.$nis.'.png?'.time().'" />';
						}else{
							echo '<img src="assets/images/foto_masuk/default.jpg?'.time().'" />';
						}
					  ?> -->
					</a>
				  </div>
				  <div class="col-xs-12 col-md-6">
					<a href="#" class="thumbnail">
					  <!-- <?php
						if (file_exists($folder_foto_lulus.''.$nis.'.jpg')) {
							echo '<img src="assets/images/foto_lulus/'.$nis.'.jpg?'.time().'" />';
						}elseif (file_exists($folder_foto_lulus.''.$nis.'.jpeg')) {
							echo '<img src="assets/images/foto_lulus/'.$nis.'.jpeg?'.time().'" />';
						}elseif (file_exists($folder_foto_lulus.''.$nis.'.png')) {
							echo '<img src="assets/images/foto_lulus/'.$nis.'.png?'.time().'" />';
						}else{
							echo '<img src="assets/images/foto_lulus/default.jpg?'.time().'" />';
						}
					  ?> -->
					</a>
				  </div>
				</div>
				<div class="form-horizontal">
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Tahun Pelajaran (Masuk)</label>
						<div class="col-sm-7">
						</div>
					</div>
				<!-- 	<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Status Masuk</label>
						<div class="col-sm-7">
							<?php 
								$query=mysqli_query($conn, "SELECT * FROM mutasi WHERE nis='$nis' AND status='m'");
								if(mysqli_num_rows($query)==0){
									$status="Siswa Baru";
								}else{
									$status="Siswa Pindahan";
									if($level==0 || $level==3){
										$status.=" (<a href='../abm/detail_siswa.php?nis=".$nis."'>cek mutasi</a>)";
									}
								}
								echo $status;
							?>
						</div>
					</div> -->
<!-- 					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Status Sekarang</label>
						<div class="col-sm-7">
							<?php 
							if($kelulusan_status==0 && $kelulusan_status!=NULL){
								$status='Tidak Lulus';
							}elseif($kelulusan_status==1){
								$status='Lulus (Alumni)';
							}else{
								$query=mysqli_query($conn, "SELECT * FROM mutasi WHERE nis='$nis' AND status='k'");
								if(mysqli_num_rows($query)==0){
									$status="Aktif";
								}else{
									$query=$query->fetch_object();
									$status="Pindah pada ".$query->tanggal;
									if($level==0 || $level==3){
										$status.=" (<a href='../abm/detail_siswa.php?nis=".$nis."'>cek mutasi</a>)";
									}
								}
							}
							echo $status;
							?>
						</div>
					</div> -->
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">NIS / NISN</label>
						<div class="col-sm-7">
					<!-- 		<?php echo $nis.' / ';
							if($nisn=='')
								echo '-';
							else
								echo $nisn;
							?> -->
						</div>
					</div>
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Nama Lengkap</label>
						<div class="col-sm-7">
							<!-- <?php echo $nama ?> -->
						</div>
					</div>
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Orang Tua</label>
						<div class="col-sm-7">
							<!-- <?php echo $nama_ayah.' & '.$nama_ibu ?> -->
						</div>
					</div>
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Wali Siswa</label>
						<div class="col-sm-7">
							<!-- <?php if($nama_wali=='') echo ''; else echo $nama_wali; ?> -->
						</div>
					</div>
					
				</div>
			  </div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title" align="center">KELAS</h3>
			  </div>
			  <div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Tahun</th>
								<th>Kelas & Wali</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<div class="form-group form-group-sm">
						<div class="col-sm-12">
						</div>
					</div>
				</div>
			  </div>
			</div>
			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title" align="center">KELULUSAN </h3>
			  </div>
			  <div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran" >Status Kelulusan</label>
						<div class="col-sm-7">
							<div id="result_edit_kelulusan">
							<!-- <?php
							if($kelulusan_status==0 && $kelulusan_status!=NULL){
								echo 'Tidak Lulus';
							}elseif($kelulusan_status==1){
								echo 'Lulus';
							}else{
								echo 'Belum Ada';
							}
							?> -->
							</div>
						</div>
					</div>
					<div class="form-group form-group-sm">
						<label class="col-sm-5" for="filter_thn_ajaran">Nomor STL/STTB</label>
						<div class="col-sm-7">
							<input type="text" name="no_stl" id="no_stl" class="form-control" value="<?php if(isset($kelulusan_status)) echo $kelulusan_no_stl; ?>" placeholder="cth : " />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<div class="col-sm-12">
<!-- 						<?php
						if($level==0 || $level==3){
							if($jenjang_terkini==12){
							?>
								<button id="lulus" name="status_kelulusan" class="btn btn-success">Lulus</button> 
								<button id="tidak_lulus" name="status_kelulusan" class="btn btn-danger">Tidak Lulus</button> 
							<?php
							}else{
								echo '<div class="alert alert-danger">Siswa Belum Menempuh Kelas XII</div>';
							}
						}
						?> -->
						</div>
					</div>
				</div>
			  </div>
			</div>

			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title" align="center">MUTASI</h3>
			  </div>
			  <div class="panel-body">
					<div class="form-group form-group-sm">
						<label class="col-sm-5" " >Status Mutasi</label>
					</div>
						<!-- <div class="col-sm-7"></div> -->
					<div class="form-group form-group-sm">
						<label class="col-sm-5" " >Tahun Ajaran</label>
					</div>
						<!-- <div class="col-sm-7"></div> -->
					<div class="form-group form-group-sm">
						<label class="col-sm-5" " >Keterangan</label>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</div>
<?php
	include_once('../footer.php');
?>