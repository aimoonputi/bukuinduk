<?php
  ob_start();
  error_reporting(1);
	
	if(isset($cetak_buku_induk)) require_once('connect.php');
	else require_once('../connect.php');
	
	$conn = mysqli_connect($db_host, $db_username, $db_password, $db_database);
	if(mysqli_connect_errno()){
		die('Could not connect to database : <br/>'.mysqli_connect_error());
	}

	$site_name="Buku Induk";
	
	if(isset($ajax_func)) require_once('../../session.php');
	elseif(isset($cetak_buku_induk)) require_once('../session.php');
	else require_once('../session.php');
	
	$folder_foto_masuk="assets/images/foto_masuk/";	
	$folder_foto_lulus="assets/images/foto_lulus/";	
	$kategori_mapel="Mata Pelajaran";
	$jml_mapel=15;
	$kategori_mulok="Muatan Lokal";
	$jml_mulok=7;
	$nilai_kosong="";
	$catatan_tuntas="TUNTAS";
	$catatan_tidak_tuntas="TIDAK TUNTAS";
	
	$jml_kar_default=50;
	$jml_kar_thn=9;
	
	$jml_kar_nis=17;
	$jml_kar_nisn=10;
	$jml_kar_nip=18;
	$jml_kar_nokk=16;
	$jml_kar_nik_wali=16;
	
	$jml_kar_nama=50;
	$jml_kar_jml_anak=2;
	$jml_kar_desa=50;
	$jml_kar_kec=50;
	$jml_kar_kab=50;
	$jml_kar_agama=10;
	$jml_kar_status_anak=15;
	$jml_kar_alamat=200;
	$jml_kar_kd_pos=5;
	$jml_kar_telp=15;
	$jml_kar_pekerjaan=20;
	$jml_kar_nama_sch=30;
	$jml_kar_penghasilan=14;
	
	$jml_kar_nama_kelas=15;
	
	
	// Tahun Ajaran
	$query=mysqli_query($conn,"SELECT tahun FROM berada_di_kelas GROUP BY tahun ORDER BY tahun DESC");
	$query=$query->fetch_object();
	$tahun_terbaru=$query->tahun;
	
	$bulan_mulai=7;
	if(date('m')>=$bulan_mulai){
		$tahun_seharusnya=(date('Y')).'/'.(date('Y')+1);
	}else{
		$tahun_seharusnya=(date('Y')-1).'/'.(date('Y'));
	}
	
	if($tahun_terbaru!=$tahun_seharusnya){
		$thnterbaru=explode("/",$tahun_terbaru);
		$thnseharusnya=explode("/",$tahun_seharusnya);
		if($thnterbaru[0]>$thnseharusnya[0]) $perlu_update=2;
		else $perlu_update=1;
	}else{
		$perlu_update=0;
	}
	
	function cek_kelulusan($conn,$nis){
		$query=mysqli_query($conn,"SELECT * FROM kelulusan WHERE nis='".$nis."'");
		if(mysqli_num_rows($query)==0){
			return '99';// angka acak yang jarang digunakan
		}else{
			$query=$query->fetch_object();
			return $query->status;
		}
		
	}
	function konversi_bulan($bulan){
		if($bulan=='Januari')
			return '01';
		elseif($bulan=='Februari')
			return '02';
		elseif($bulan=='Maret')
			return '03';
		elseif($bulan=='April')
			return '04';
		elseif($bulan=='Mei')
			return '05';
		elseif($bulan=='Juni')
			return '06';
		elseif($bulan=='Juli')
			return '07';
		elseif($bulan=='Agustus')
			return '08';
		elseif($bulan=='September')
			return '09';
		elseif($bulan=='Oktober')
			return '10';
		elseif($bulan=='November')
			return '11';
		elseif($bulan=='Desember')
			return '12';
		else
			return $bulan;
	}

	function test_input($conn,$data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		$data = mysqli_real_escape_string($conn,$data);
		return $data;
	}
	
	function baca_angka_satuan($angka){
		if($angka==1){
			return 'satu';
		}elseif($angka==2){
			return 'dua';
		}elseif($angka==3){
			return 'tiga';
		}elseif($angka==4){
			return 'empat';
		}elseif($angka==5){
			return 'lima';
		}elseif($angka==6){
			return 'enam';
		}elseif($angka==7){
			return 'tujuh';
		}elseif($angka==8){
			return 'delapan';
		}elseif($angka==9){
			return 'sembilan';
		}else{
			return '';
		}
	}
	
	function baca_angka($angka){
		if($angka==100){
			return 'Seratus';
		}elseif($angka=='-' || $angka==''){
			return '';
		}elseif($angka==10){
			return 'Sepuluh';
		}elseif($angka==11){
			return 'Sebelas';
		}elseif($angka==12){
			return 'Dua Belas';
		}elseif($angka==13){
			return 'Tiga Belas';
		}elseif($angka==14){
			return 'Empat Belas';
		}elseif($angka==15){
			return 'Lima Belas';
		}elseif($angka==16){
			return 'Enam Belas';
		}elseif($angka==17){
			return 'Tujuh Belas';
		}elseif($angka==18){
			return 'Delapan Belas';
		}elseif($angka==19){
			return 'Sembilan Belas';
		}elseif($angka==0){
			return 'Nol';
		}else{
			if($angka>=20 && $angka<100){
				$angka=''.$angka.'';
				return baca_angka_satuan($angka[0]).' puluh '.baca_angka_satuan($angka[1]);
			}
		}
	}
	
	function baca_romawi_kelas($kelas){
		$kelas=explode(" ",$kelas);
		if(in_array("X",$kelas)){
			return "SEPULUH";
		}elseif(in_array("XI",$kelas)){
			return "SEBELAS";
		}elseif(in_array("XII",$kelas)){
			return "DUA BELAS";
		}else{
			return "-";
		}
	}
	
	function ambil_jenjang_kelas($kelas){
		$kelas=explode(" ",$kelas);
		if(in_array("X",$kelas)){
			return "10";
		}elseif(in_array("XI",$kelas)){
			return "11";
		}elseif(in_array("XII",$kelas)){
			return "12";
		}else{
			return "00";
		}
	}
	
	function ambil_romawi_kelas($kelas){
		$kelas=substr($kelas,2,2);
		if($kelas==10){
			return 'X';
		}elseif($kelas==11){
			return 'XI';
		}elseif($kelas==12){
			return 'XII';
		}else{
			return ' ';
		}
	}
	
	function ambil_jurusan($kelas){
		$kelas=explode(" ",$kelas);
		if(in_array("IPA",$kelas)){
			return "IPA";
		}elseif(in_array("IPS",$kelas)){
			return "IPS";
		}elseif(in_array("Bahasa",$kelas)){
			return "Bahasa";
		}else{
			return "";
		}
	}
	
	function ambil_kd_jurusan($kelas){
		$kelas=explode(" ",$kelas);
		if(in_array("IPA",$kelas)){
			return "A";
		}elseif(in_array("IPS",$kelas)){
			return "S";
		}elseif(in_array("Bahasa",$kelas)){
			return "B";
		}elseif(in_array("-",$kelas)){
			return "-";
		}else{
			return "-";
		}
	}
	
	function c2h($angka){
		if($angka==1)
			return 'a';
		elseif($angka==2)
			return 'b';
		elseif($angka==3)
			return 'c';
		elseif($angka==4)
			return 'd';
		elseif($angka==5)
			return 'e';
	}
	
	function c2bulan($angka){
		if($angka==1)
			return 'Januari';
		elseif($angka==2)
			return 'Februari';
		elseif($angka==3)
			return 'Maret';
		elseif($angka==4)
			return 'April';
		elseif($angka==5)
			return 'Mei';
		elseif($angka==6)
			return 'Juni';
		elseif($angka==7)
			return 'Juli';
		elseif($angka==8)
			return 'Agustus';
		elseif($angka==9)
			return 'September';
		elseif($angka==10)
			return 'Oktober';
		elseif($angka==11)
			return 'November';
		elseif($angka==12)
			return 'Desember';
		else
			return '';
	}
	
?>
