//===========================================================================
// sipetani.js v0.0.2
//===========================================================================

$(document).ready(function(){
    // Mengaktifkan tooltip
    $("body").tooltip({
        selector: "[data-toggle='tooltip']",
        trigger: "hover"
    });

    // Menangani body bergeser saat modal ditutup
    $("body").on("hidden.bs.modal", function(){
        $(this).css("padding-right", "0px");
    });

    // Modal ubah password
    $("#btn-ubh-pass").click(function(){
        $("#mdl-ubh-pass").modal();
    });

    // Submit form ubah password dgn ajax
    $(".form-ajx-pass").submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            dataType: "html",
            success: function(data){
                $(".modal").modal("hide");
                $("#knt-pass-not").html(data);
            }
        });
    });

    // Modal Keluar
    $("#btn-klr").click(function(){
        $("#mdl-klr").modal();
    });
});
