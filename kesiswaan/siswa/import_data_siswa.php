<?php
  $index = "false";
  include("../header.php");
  include("../../koneksi.php");
?>


<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Import Data Siswa</h2>
	       <!--  <ul class="pull-right nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	        </ul> -->
        <div class="clearfix"></div>
      </div>
      <div class="panel panel-default" style="background-color: #2a3f54">
        <div class="panel-body">
          <p class="text-muted font-20 m-b-50" style="color: #f5f5f5 ">
            Melakukan Penambahan Data Siswa Secara Massal dengan Format Excel
          </p>
          <div class="col-md-10">
          <a class="btn btn-primary btn-sm" style="background-color: #51a6b5" href="/BukuInduk/kesiswaan/format_import_data_siswa.xlsx"><i class="fa fa-download" ></i> FORMAT EXCEL</a>
      </div>
        </div>
      </div>

      <div class="panel panel-default" style="background-color: #51a6b5"> <br>
      <div class="col-md-0 col-xs-12" ></div>
		<form class="form-horizontal" name="myForm" id="myForm" enctype="multipart/form-data" action="fungsi_import_data_siswa.php" method="POST">
			
            <div class="form-horizontal form-label-left">
        	<div class="form-group" style="color: #f5f5f5 ">
			    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tahun_ajaran_masuk">Tahun Pelajaran</label>
			    <div class="col-md-4 col-sm-4 col-xs-12" >
			    	<select class="form-control" id="tahun-ajaran" name="tahun_ajaran" value="<?php if (isset($kelas_diterima)){echo $kelas_diterima; } ?>">
							<option value="none">- Pilih tahun pelajaran -</option>
							<?php
							$query=mysqli_query($conn,"SELECT * FROM kelas GROUP BY tahun_ajaran ORDER BY tahun_ajaran DESC");
							while($query1=$query->fetch_object()){
								echo '<option value="'.$query1->tahun_ajaran.'"';
								echo '>'.$query1->tahun_ajaran.'</option>';
							}
							?>
					</select>
			    </div>
			</div>

			<!-- <div class="form-horizontal form-label-left"></div> -->
			<div class="form-group" style="color: #f5f5f5 ">
				<label class="control-label col-md-4 col-sm-4 col-xs-12" for="filter_thn_ajaran">Unggah File</label>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<input class="form-control" type="file" id="fileexcel" name="fileexcel" required />
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-12" ></label>
				<div class="col-sm-4">
					<input type="submit" class="btn btn-primary form-control" id="masukkan" name="masukkan" value="Simpan" style="background-color: #2a3f54"/>
				</div>
			</div>

			<label> 
		</form>
		</label>
		</div>
		</div>
    </div>
  </div>
</div>
</div>

<script>
	$(document).ready(function(){
		$('#tahun-ajaran').change(function(){
			if($("#tahun-ajaran").val()=="none"){
				var thn='';
			}else{
				var thn = $("#tahun-ajaran").val();
			}
			$.ajax({
				url:"../../ajax/get_kelas.php?thn="+thn,
				type:"GET",
				dataType:"html",
				
				beforeSend: function(){
					$("#kelas").html('Loading...');
				},
				success: function(data){
					$("#kelas").html(data);
				},
				error: function(){
					$("#kelas").html("");
				}
			});
		});
	});
</script>

<?php if (isset($_SESSION['alert'])): ?>
  	<script>alert('<?php echo $_SESSION['alert'] ?>')</script>
    <?php unset($_SESSION['alert']) ?>
<?php endif; ?>

<?php
	include_once('../footer.php');
?>