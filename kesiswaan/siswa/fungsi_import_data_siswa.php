<?php

session_start();
include("../../koneksi.php");

if (isset($_POST['masukkan'])) {
    $tahun_ajaran = $_POST['tahun_ajaran'];    
    $fileexcel = $_FILES['fileexcel']['name'];
    $temp = $_FILES['fileexcel']['tmp_name'];
    $file_type = pathinfo($fileexcel, PATHINFO_EXTENSION);
    $target_file = '../upload/';
    $upload_ok = 1;

    $allowed_type = array('xls','xlsx');
    if (!in_array($file_type, $allowed_type)) {
        $psn = "Hanya dapat mengunggah file bertipe xls, xlsx";
        $upload_ok = 0;
		$_SESSION['alert'] = $psn;
    	header('location: import_data_siswa.php');
    }

    if($upload_ok == 1){
    	move_uploaded_file($temp,$target_file.$fileexcel);
    	$read_temp = $target_file.$fileexcel;
    	require_once('../PHPExcel-1.8.1/Classes/PHPExcel.php');
    	require_once('../PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');
    	$read = PHPExcel_IOFactory::createReaderForFile($read_temp);
    	$read->setReadDataOnly(true);
    	$objExcel = $read->load($read_temp);
    	$WorkSheet = $objExcel->setActiveSheetIndex(0);
    	$highestRow = $objExcel->getActiveSheet()->getHighestDataRow();
    	$highestColumn = $objExcel->getActiveSheet()->getHighestDataColumn();
    	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);    	
        for($row = 2;$row <= $highestRow; ++$row){
    		$val = array();
    		for($col = 0 ; $col <= $highestColumnIndex ; ++$col){
    			$cell = $WorkSheet->getCellByColumnAndRow($col,$row);
    			if($col != 4){
    				$val[] = $cell->getValue();
    			}
    			else{
    				$val[] = date('Y-m-d',PHPExcel_Shared_Date::ExceltoPHP($cell->getValue()));
    			}
    		}
    		$data[] = "('".$val[0]."','".$val[1]."','".$val[2]."','".$val[3]."','".$val[4]."','".$val[5]."','".$val[6]."','".$val[7]."','".$val[8]."','".$val[9]."','".$val[10]."','".$val[11]."','".$val[12]."','".$val[13]."','".$val[14]."','".$val[21]."','".$val[26]."','".$val[27]."','".$val[28]."','".$val[29]."')";
    	    $data_dikelas[] = "('".$val[0]."','".$tahun_ajaran."')";
            $query_cek_user = "SELECT * FROM users WHERE username='".$val[0]."'";
            $result_cek_user = $conn->query($query_cek_user);
            if (mysqli_num_rows($result_cek_user) == 0) {
                $data_user[] = "('".$val[0]."', '".md5($val[0])."', '".$val[2]."', 'Siswa')";
            }
            $query2 = "SELECT nokk FROM orangtua_siswa WHERE nokk=".$val[14]."";
            $result2 = $conn->query($query2);
            // print(mysqli_num_rows($result2));
            if (mysqli_num_rows($result2) == 0) {                
                
                $data1[] = "('".$val[14]."','".$val[15]."','".$val[16]."','".$val[17]."','".$val[18]."','".$val[19]."','".$val[20]."')";    
            }        	
            $query3 = "SELECT nik_wali FROM wali_siswa WHERE nik_wali=".$val[21]."";
            $result3 = $conn->query($query3);
            // print(mysqli_num_rows($result3));
            // print("<br>");
            if (mysqli_num_rows($result3) == 0) {
                // print("sammpai sini");
                $data2[] = "('".$val[21]."','".$val[22]."','".$val[23]."','".$val[24]."','".$val[25]."')";
            }
    		
    	}

    	$query = "INSERT INTO siswa(nis, nisn, nama, tempat_lahir, tanggal_lahir, agama, desa, kecamatan, kabupaten, jenis_kel, anak_ke, dari_bersaudara, status_anak, telepon, nokk, nik_wali, kelas_diterima, tanggal_diterima, sekolah_asal, alamat_sekolah_asal) VALUES";
    	$query1 = "INSERT INTO orangtua_siswa(nokk, nama_ayah, nama_ibu, alamat_ortu, telepon_ortu, pekerjaan_ayah, pekerjaan_ibu) VALUES";
    	$query2 = "INSERT INTO wali_siswa(nik_wali, nama_wali, alamat_wali, pekerjaan_wali, telepon_wali) VALUES";
        $query_dikelas = "INSERT INTO di_kelas(nis, tahun_ajaran) VALUES";
        $query_user = "INSERT INTO users(username, password, nama, level) VALUES";
    	$query .= implode(",",$data).";";
        $result = mysqli_query($conn, $query);
        $query_dikelas .= implode(",",$data_dikelas).";";
        // print($query_dikelas); die;
        $result_dikelas = mysqli_query($conn, $query_dikelas);
        if (isset($data1)) {
            // print("data1");print("<br>");
            $query1 .= implode(",",$data1).";";
            $result1 = mysqli_query($conn, $query1);
        }else{$result1=TRUE;}
        // var_dump($data1); die();
    	if (isset($data2)) {
            // print("data2");print("<br>");
            $query2 .= implode(",",$data2).";";
            $result2 = mysqli_query($conn, $query2);
        }else{$result2=TRUE;}
        if (isset($data_user)) {
            $query_user .= implode(",",$data_user).";";
            $result_user = mysqli_query($conn, $query_user);
        }else{$result_user=TRUE;}

        if ($result && $result1 && $result2 && $result_user) {
            $psn = 'Data berhasil ditambah';
            $_SESSION['alert'] = $psn;
    		header('location: daftar_siswa.php');
        }else {
            // die("Could not query the database: <br/>". $conn->error);
            $psn = 'Ada NIS yang sudah digunakan (Hapus terlebih dahulu dari File Excel)';
            $_SESSION['alert'] = $psn;
            header('location: import_data_siswa.php');
        }
    }
}
?>