<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar Siswa</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
      <p class="text-muted font-13 m-b-30">
      </p>

      <div class="btn-group" role="group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-print"></i> Cetak Buku Induk
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a >Siswa Yang Tampil di Halaman Ini</a></li>
              <li><a >Semua Siswa Yang Ditemukan</a></li>
            </ul>
      </div>

      <!-- <div class="">
        <select class="form-control" id="filter_thn_ajaran">
          <option value="all">Semua Tahun</option>
            <?php
            $query=mysqli_query($conn,"SELECT * FROM kelas GROUP BY tahun_ajaran ORDER BY tahun_ajaran DESC");
            while($query1=$query->fetch_object()){
              echo '<option value="'.$query1->tahun_ajaran.'"';
              echo '>'.$query1->tahun_ajaran.'</option>';
            }
          ?>
        </select>
      </div> -->


        <?php                    
          $i = 1;

          // ==================================================================
          // PROSES HAPUS
          // ==================================================================
          if (isset($_POST["hapus-submit"])) {
            $nis = test_input($_POST['hapus-nis']);
            $query_select = "SELECT * FROM siswa WHERE nis = '$nis'";
            $result_select = $conn->query($query_select);
            $row = $result_select->fetch_object();    

            $query_select_nokk = "SELECT * FROM siswa WHERE nokk = '".$row->nokk."'";
            $result_select_nokk = $conn->query($query_select_nokk);
            $query_select_wali = "SELECT * FROM siswa WHERE nik_wali = '".$row->nik_wali."'";
            $result_select_wali = $conn->query($query_select_wali);
            // var_dump($result_select_nokk->num_rows);die();
            // var_dump($result_select_wali->num_rows);die();
            if ($result_select_nokk->num_rows == 1) {
              $query_delete_ortu = "DELETE FROM orangtua_siswa WHERE nokk='".$row->nokk."'";
              $result_delete_ortu = $conn->query($query_delete_ortu);
            }
            if ($result_select_wali->num_rows == 1) {
              $query_delete_wali = "DELETE FROM wali_siswa WHERE nik_wali='".$row->nik_wali."'";
              $result_delete_wali = $conn->query($query_delete_wali);
            }
            
            $query = "DELETE FROM siswa WHERE nis = '$nis'";
            $query1 = "DELETE FROM di_kelas WHERE nis = '$nis'";
            $query2 = "DELETE FROM users WHERE username = '$nis'";
            $query3 = "DELETE FROM mutasi WHERE nis = '$nis'";
              $result = $conn->query($query);
              $result1 = $conn->query($query1);
              $result2 = $conn->query($query2);
              $result3 = $conn->query($query3);
              if ($result && $result1) {
                echo "<script>alert('Data Siswa Berhasil Dihapus')</script>";
              }else {
                die("Could not query the database: <br/>". $conn->error);
              }
          }

          $query1 = "SELECT * FROM siswa";
          $result = $conn->query($query1);
          // $fetch = $result->fetch_object();

          function test_input($data){
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
          }
        ?>

        <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th style="text-align: center;"width="5%">No</th>
              <th style="text-align: center;"width="10%">NIS</th>
              <!-- <th style="text-align: center;"width="10%">NISN</th> -->
              <th style="text-align: center;"width="20%">Nama</th>
              <th style="text-align: center;"width="5%">L/P</th>
              <!-- <th style=" text-align: center;"width="5%">Tahun Ajaran</th> -->
              <th style="text-align: center;"width="15%">Foto Masuk</th>
              <th style="text-align: center;"width="15%">Foto Lulus</th>
              <th style="text-align: center;"width="20%">Aksi</th>
            </tr>
          </thead>
          <tbody id="daftar-siswa">
            <?php
            while ($row = $result->fetch_object())
            {
              echo "<tr>";
              echo "<td style=' text-align: center;'>".$i."</td>";
              echo "<td style=' text-align: center;'>".$row->nis."</td>";
              // echo "<td style=' text-align: center;'>".$row->nisn."</td>";
              echo "<td>".$row->nama."</td>";
              echo "<td style=' text-align: center;'>".$row->jenis_kel."</td>";
              // echo "<td style=' text-align: center;''>".$row->tahun_ajaran."</td>";
              // echo "<td colspan='3'>";
              if ($row->foto_masuk == '') {
                echo "<td  style=' text-align: center;'>Belum Ada Foto</td>";  
                  // "<td><a href='".base_url("kesiswaan/siswa/tambah_foto_siswa.php")."'>Belum Ada Foto</a></td>"; 
              } else {
                echo "<td  style=' text-align: center;'><img src=".base_url("kesiswaan/siswa/foto_masuk/".$row->foto_masuk)." class='tampil-foto-siswa'></td>";
              }
              if ($row->foto_keluar == '') {
                echo "<td  style=' text-align: center;'>Belum Ada Foto</td>";
                  // "<td><a href='".base_url("kesiswaan/siswa/tambah_foto_siswa.php")."'>Belum Ada Foto</a></td>";
              } else {
                echo "<td  style=' text-align: center;'> <img src=".base_url("kesiswaan/siswa/foto_keluar/".$row->foto_keluar)." class='tampil-foto-siswa'></td>";
              }
              echo '<td  style=" text-align: center;">
                    <a href="'.base_url("kesiswaan/siswa/detail_data_siswa.php?nis=".$row->nis."").'"><button title="Lihat Detail Siswa" class="btn btn-info"><i class="fa fa-eye"></i> </button></a>
                    <button target="_blank" title="Print Buku Induk Siswa" href="" class="btn btn-default"> <i class="fa fa-print"></i></button>
                    <a href="'.base_url("kesiswaan/siswa/edit_data_siswa.php?nis=".$row->nis."").'"><button title="Edit Data Siswa" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i> </button></a>
                    <button class="btn-hapus-siswa btn btn-danger" title="Hapus Data Siswa" data-toggle="modal" data-nis='.$row->nis.' data-target="#modal-hapus-siswa" id="hapus-siswa"> <i class="fa fa-trash"></i></button>
                </td>';
              echo "</td>";
              echo "</tr>";
              $i++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- ================================================================ -->
<!-- MODAL HAPUS Siswa -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-hapus-siswa" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Hapus Siswa</h4>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" style="text-align:center;">Apakah anda yakin ingin menghapus siswa ini?</h4>
        <!-- <p>Apakah Anda yakin menghapus siswa ini?</p> -->
      </div>
      <div class="modal-footer">
        <form role="form" method="post" class="form-hapus-siswa" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
          <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
          <input type="hidden" name="hapus-nis" id="hapus-nis">
          <button type="submit" class="btn btn-success" name="hapus-submit">Hapus</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
	// ======================================================
	// INISIALISASI MODAL HAPUS SISWA
	// ======================================================
    $(".btn-hapus-siswa").on("click", function(){
        var nis = $(this).attr("data-nis");
		    $("#hapus-nis").val(nis);
        $("#modal-hapus-siswa").modal('');
    });

    $('#filter_thn_ajaran').change(function(){
      var tahun = $("#filter_thn_ajaran").val();

      $.ajax({
        url:"ajax_func/filter.php?tahun="+tahun,
        type:"GET",
        dataType:"html",

        beforeSend: function(){
          $("#daftar-siswa").html('Loading...');
        },
        success: function(data){
          $("#daftar-siswa").html(data);
        },
        error: function(){
          $("#daftar-siswa").html("gagal memuat data");
        }
      });
    });

});
</script>

<?php if (isset($_SESSION['alert'])){?>
      <script>alert('<?php echo $_SESSION['alert'] ?>')</script>
<?php unset($_SESSION['alert']);
}

include("../footer.php");
?>
