<?php
  $index = "false";
  include("../header.php");
  include("../../koneksi.php");
?>


<?php
// ==================================================================
// PROSES EDIT
// ==================================================================
if (isset($_POST["ubah-submit"])) {
  $nis = test_input($_POST['ubah-nis']);
  $jenis_foto = test_input($_POST['ubah-jenis_foto']);

  if ($jabatan == "") {
    $error_jabatan = "jabatan harus diisi";
    $valid_jabatan = FALSE;
  }else {
    $valid_jabatan = TRUE;
  }

  if ($valid_nis && $valid_jenis_foto) {
    $nip = $conn->real_escape_string($nip);
    $jenis_foto = $conn->real_escape_string($jenis_foto);
      $query = "UPDATE siswa SET foto_masuk = '$foto_masuk', foto_keluar = '$foto_keluar'";
      $result = $conn->query($query);
      if ($result) {
        echo "<script>alert('Foto Berhasil Diubah')</script>";
      }else {
        die("Could not query the database: <br/>". $conn->error);
      }
  }
}

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
<style type="text/css">
  .tampil-foto{
    max-width: 150px;
  }
</style>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Tambah Foto Siswa</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
<!--       <div class="col-md-10">
        <button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success" id="btn-tbh-foto"
        data-toggle="tooltip" title="Menambahkan Foto">
          <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah
        </button>
      </div> -->

      	 <?php
          // require $_SERVER["DOCUMENT_ROOT"] . "/Buku Induk/koneksi.php";
          $query = "SELECT * FROM siswa";
          $result = $conn->query($query);
          $i = 1;
         ?>

          <table id="datatable" class="table table-striped table-bordered">
          <thead class="text-center">
            <tr>
              <!-- <th width="5%">No</th>
              <th width="15%">NIS</th>
              <th width="25%">Foto Masuk</th>
              <th width="25%">Foto Lulus</th> -->
              <!-- <th hidden>#</th> -->
              <th width="5%" style=" text-align: center;">No</th>
              <th width="10%" style=" text-align: center;">NIS</th>
              <th width="15%" style=" text-align: center;">Nama Siswa</th>
              <th width="35%" style=" text-align: center;">Foto Masuk</th>
              <th width="35%" style=" text-align: center;">Foto Lulus</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while ($row = $result->fetch_object())
            {
              echo "<tr>";
              echo "<td style=' text-align: center;'>".$i."</td>";
              echo "<td style=' text-align: center;'>".$row->nis."</td>";
              echo "<td style=' text-align: center;'>".$row->nama."</td>";
              if($row->foto_masuk == "" && $row->foto_keluar == ""){
                echo '<td style=" text-align: center;">Belum ada foto terupload <br><button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success btn-tambah-masuk" data-toggle="tooltip" data-nis='.$row->nis.' title="Menambahkan Foto"> <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah</button></td>';
                echo '<td style=" text-align: center;">Belum ada foto terupload <br><button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success btn-tambah-lulus" data-toggle="tooltip" data-nis='.$row->nis.' title="Menambahkan Foto"> <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah</button></td>';
              }
              else if($row->foto_masuk == ""){
                echo '<td style=" text-align: center;">Belum ada foto terupload <br><button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success btn-tambah-masuk" data-toggle="tooltip" data-nis='.$row->nis.' title="Menambahkan Foto"> <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah</button></td>';
                echo "<td style=' text-align: center;'><img src=".base_url("kesiswaan/siswa/foto_keluar/".$row->foto_keluar)." class='tampil-foto'> 
                  <button class='btn btn-warning btn-ubah-foto' title='Edit Foto Siswa' data-nis='".$row->nis."' data-jenis-foto='lulus'><i class='fa fa-pencil-square-o'></i>Ubah</button>
                  <button class='btn btn-danger btn-hapus-foto' data-nis='".$row->nis."' data-jenis-foto='lulus' data-foto='".$row->foto_keluar."' title='Hapus Foto Siswa'><i class='fa fa-trash'></i>Hapus</button>
                </td>";
              }
              else if($row->foto_keluar == ""){
                echo "<td style=' text-align: center;'><img src=".base_url("kesiswaan/siswa/foto_masuk/".$row->foto_masuk)." class='tampil-foto'> 
                  <button class='btn btn-warning btn-ubah-foto' title='Edit Foto Siswa' data-nis='".$row->nis."' data-jenis-foto='masuk'><i class='fa fa-pencil-square-o'></i>Ubah</button>
                  <button class='btn btn-danger btn-hapus-foto' data-nis='".$row->nis."' data-jenis-foto='masuk' data-foto='".$row->foto_masuk."' title='Hapus Foto Siswa'><i class='fa fa-trash'></i>Hapus</button>
                </td>";
                echo '<td style=" text-align: center;">Belum ada foto terupload <br> <button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success btn-tambah-lulus" data-toggle="tooltip" data-nis='.$row->nis.' title="Menambahkan Foto"> <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah</button></td>';
              }
              else{
                echo "<td style=' text-align: center;'><img src=".base_url("kesiswaan/siswa/foto_masuk/".$row->foto_masuk)." class='tampil-foto'> <br>
                  <button class='btn btn-warning btn-ubah-foto' title='Edit Foto Siswa' data-nis='".$row->nis."' data-jenis-foto='masuk'><i class='fa fa-pencil-square-o'></i>Ubah</button>
                  <button class='btn btn-danger btn-hapus-foto' data-nis='".$row->nis."' data-jenis-foto='masuk' data-foto='".$row->foto_masuk."' title='Hapus Foto Siswa'><i class='fa fa-trash'></i>Hapus</button>
                </td>";
                echo "<td style=' text-align: center;'><img src=".base_url("kesiswaan/siswa/foto_keluar/".$row->foto_keluar)." class='tampil-foto'> <br>
                  <button class='btn btn-warning btn-ubah-foto' title='Edit Foto Siswa' data-nis='".$row->nis."' data-jenis-foto='lulus'><i class='fa fa-pencil-square-o'></i>Ubah</button>
                  <button class='btn btn-danger btn-hapus-foto' data-nis='".$row->nis."' data-jenis-foto='lulus' data-foto='".$row->foto_keluar."' title='Hapus Foto Siswa'><i class='fa fa-trash'></i>Hapus</button>
                </td>";                
              }
              echo "</tr>";
              $i++;
            }
            ?>
          </tbody>
        </table>
    </div>
  </div>
</div>

		<!-- ======================================================================= -->
		<!-- MODAL TAMBAH FOTO -->
		<!-- ======================================================================= -->
		<div class="modal fade" id="modalTambah" role="dialog">
		  <div class="modal-dialog modal-md">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Tambah Foto Siswa</h4>
		      </div>
		      <div class="modal-body">
		        <form class="form-horizontal" method="post" id="form-foto" enctype="multipart/form-data" action="fungsi_tambah_foto_siswa.php">
      					<div class="form-group">
      						<label class="col-sm-3 control-label" for="filter_thn_ajaran">Jenis Foto</label>
      							<div class="col-sm-8">
      								<select class="form-control" id="jenis_foto" name="jenis_foto" readonly style="cursor: not-allowed;">
      									<option value="masuk">Foto Masuk</option>
      									<option value="lulus">Foto Lulus</option>
      								</select>
      							</div>
      					</div>
      					<div class="box-body">
      						<div class="form-group">
      						<label class="col-sm-3 control-label">NIS :</label>
      							<div class="col-sm-8">
      								<input type="text" name="nis" class="form-control" placeholder="6256" id="tambah_nis" required readonly style="cursor: not-allowed;" />
      							</div>
      						</div>
      					</div>
      					<div class="form-group">
      						<label class="col-sm-3 control-label" for="filter_thn_ajaran">Upload Foto</label>
      						<div class="col-sm-8">
      							<input class="form-control" type="file" id="foto" name="foto" />
      							<p class="help-block">* ukuran maksimal foto 2MB </br> * format foto : jpg, jpeg, png </p> 
      						</div>
      					</div>
      		      </div>

                <div class="modal-footer">
                  <form role="form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
                  <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                  <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
                  </div>
                </form>
                </div>
		        </form>
		    </div>
		  </div>
		</div>
	</div>

    <!-- ======================================================================= -->
    <!-- MODAL UBAH FOTO -->
    <!-- ======================================================================= -->
     <div class="modal fade" id="modal-ubah-foto" role="dialog">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Ubah Foto Siswa</h4>
          </div>
          <div class="modal-body">
          <form class="form-horizontal" method="post" id="form-foto" enctype="multipart/form-data" action="fungsi_ubah_foto_siswa.php">
          <!-- <div class="form-group">
              <div class="col-sm-8">
                <input class="form-control" type="hidden" id="ubah-jenis-foto" name="jenis_foto">
              </div>
          </div> -->
          <!-- <div class="box-body">
            <div class="form-group">
              <div class="col-sm-8">
                <input type="hidden" name="nis" class="form-control" id="ubah-nis" />
              </div>
            </div>
          </div> -->
          <div class="form-group">
            <label class="col-sm-3 control-label" for="filter_thn_ajaran">Jenis Foto</label>
              <div class="col-sm-8">
                <select class="form-control" id="ubah-jenis-foto" name="jenis_foto" readonly style="cursor: not-allowed;">
                  <option value="masuk">Foto Masuk</option>
                  <option value="lulus">Foto Lulus</option>
                </select>
              </div>
          </div>
          <div class="box-body">
            <div class="form-group">
            <label class="col-sm-3 control-label">NIS :</label>
              <div class="col-sm-8">
                <input type="text" name="nis" class="form-control" id="ubah-nis" required readonly style="cursor: not-allowed;" />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="filter_thn_ajaran">Upload Foto</label>
            <div class="col-sm-8">
              <input class="form-control" type="file" id="foto" name="foto" />
              <p class="help-block">* ukuran maksimal foto 2MB </br> * format foto : jpg, jpeg, png </p> 
            </div>
          </div>
          </div>
            <div class="modal-footer">
                  <form role="form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
                  <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                  <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
                   <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
                  </div>
                </form>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
    <!-- MODAL HAPUS FOTO -->
    <!-- ======================================================================= -->
     <div class="modal fade" id="modal-hapus-foto" role="dialog">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Hapus Foto Siswa</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="post" id="form-foto" enctype="multipart/form-data" action="fungsi_hapus_foto_siswa.php">
          <div class="form-group">
              <div class="col-sm-8">
                <input class="form-control" type="hidden" id="hapus-jenis-foto" name="jenis_foto">
              </div>
          </div>
          <h4 class="modal-title" style="text-align:center;">Apakah anda yakin ingin menghapus foto ini?</h4>
          <!-- <h5>Apakah Anda ingin menghapus foto ini?</h5> -->
          <div class="box-body">
            <div class="form-group">
              <div class="col-sm-8">
                <input type="hidden" name="nis" class="form-control" id="hapus-nis" />
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <div class="col-sm-8">
                <input type="hidden" name="foto" class="form-control" id="hapus-foto" />
              </div>
            </div>
          </div>
          <div class="modal-footer">
                  <form role="form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
                  <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                  <button type="submit" class="btn btn-primary" name="hapus-submit">Hapus</button>
                  <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
                </form>
                </div>
              </div>
            </div>
       <!--    </div> -->
          
        </div>
      </div>
    </div>
  </div>


<script>
$(document).ready(function() {

    $(".btn-ubah-foto").on("click", function(){
        var nis = $(this).data("nis");
        var jenisFoto = $(this).data("jenis-foto");        
        $("#ubah-nis").val(nis);
        $("#ubah-jenis-foto").val(jenisFoto);
        $("#modal-ubah-foto").modal();
    });

    $(".btn-hapus-foto").on("click", function(){
        var nis = $(this).data("nis");
        var jenisFoto = $(this).data("jenis-foto");
        var foto = $(this).data("foto");
        $("#hapus-nis").val(nis);
        $("#hapus-jenis-foto").val(jenisFoto);
        $("#hapus-foto").val(foto);
        $("#modal-hapus-foto").modal();
    });

    $(".btn-tambah-masuk").click(function(){
      var nis = $(this).data('nis');
      $("#tambah_nis").val(nis);
      $("#jenis_foto").val("masuk");
    });
    $(".btn-tambah-lulus").click(function(){
      var nis = $(this).data('nis');
      $("#tambah_nis").val(nis);
      $("#jenis_foto").val("lulus");
    });

  // function ubah_foto(nis,foto,status){
  //   $('#modal-ubah-foto').modal('show');
  //   $.ajax({
  //     url : '/BukuInduk/ajax/edit_foto.php?nis='+nis+'&foto='+foto+'&status='+status,
  //     type : 'GET',
  //     success : function(response){
  //       $('#jenis_foto').val(response.jenis_foto);
  //       $('#nis_siswa').val(response.nis);
  //     }
  //   })
  // }
});
</script>

<?php if (isset($_SESSION['alert'])): ?>
  	<script>alert('<?php echo $_SESSION['alert'] ?>')</script>
    <?php unset($_SESSION['alert']) ?>
<?php endif; ?>

<?php
	include_once('../footer.php');
?>