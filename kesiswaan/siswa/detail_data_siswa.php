<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>


<?php
// if(isset($_GET['nis'])){
$nis=$_GET['nis'];		

$query="SELECT * FROM (siswa s LEFT JOIN orangtua_siswa o ON s.nokk=o.nokk) LEFT JOIN wali_siswa ws ON s.nik_wali=ws.nik_wali WHERE nis='$nis'";	
$result = $conn->query($query);
while ($row = $result->fetch_object()) {
	$query2 = "SELECT tahun_ajaran FROM di_kelas WHERE kd_kelas='".$row->kelas_diterima."' AND nis='".$row->nis."'";
	$result2 = $conn->query($query2);
	$tahun_ajaran = "";
	if($result2->num_rows){
		$kls = $result2->fetch_object();
		$tahun_ajaran = $kls->tahun_ajaran;
	}		
	$nis = $row->nis;
	$kelas_diterima = $row->kelas_diterima;
	$nisn = $row->nisn;
	$nama = $row->nama;
	$tempat_lahir = $row->tempat_lahir;
	$tanggal_lahir = $row->tanggal_lahir;
	$jenis_kel = $row->jenis_kel;
	$foto_masuk = $row->foto_masuk;
	$foto_keluar = $row->foto_keluar;
	$tanggal_diterima = $row->tanggal_diterima;
	$nama_ayah = $row->nama_ayah;
	$nama_ibu = $row->nama_ibu;
	$nama_wali = $row->nama_wali;
}

// $query1="SELECT * FROM di_kelas WHERE nis='".$nis."'";
// $result1 = $conn->query($query1);
// while ($row1 = $result1->fetch_object()) {
// 	echo $row1->kd_kelas;
// 	echo "<br>";
// }

?>
<style type="text/css">
.tampil-foto{
	max-width: 0px;
}
</style>

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-md-12" >
		<div class="x_panel">
			<div class="x_title" >
				<h2 >Detail Data Siswa</h2>
				<div class="clearfix"></div>
			</div>
			<div class="panel panel-default col-md-12 col-md-6"  >
				<div class="panel-heading">
					<h2 class="panel-title">DATA SISWA</h2>
				</div>

				<?php
				$nis = $_GET['nis'];
				$query = "SELECT * FROM siswa";
				$result = $conn->query($query);
				$row = $result->fetch_object();
				$i = 1; 
				?>
				
				<div class="panel-body">
					<div class="row" class="col-xs-12 col-md-12"	>
					</div>
					<div class="form-horizontal">
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<h4 style="text-align:center;">Foto Masuk</h4>
								<a href="#" class="thumbnail" style="text-align:center;">
									<!-- <img src="<?php echo base_url('kesiswaan/siswa/foto_masuk/'.$foto_masuk) ; ?>" class="tampil-foto"> -->
									<?php
									if ($foto_masuk == '') {
										echo "<td>Belum Ada Foto</td>";  
									} else {
										echo "<td  style=' text-align: align-center;'><img src=".base_url("kesiswaan/siswa/foto_masuk/".$foto_masuk)." class='tampil-foto'></td>";
									}
									?>
								</a>
							</div>
							<div class="col-xs-12 col-md-6">
								<h4 style="text-align:center;">Foto Lulus</h4>
								<a href="#" class="thumbnail" style="text-align:center;">
									<!-- <img src="<?php echo base_url('kesiswaan/siswa/foto_keluar/'.$foto_keluar); ?>" class="tampil-foto"> -->
								<?php
									if ($foto_keluar == '') {
										echo "<td>Belum Ada Foto</td>";  
									} else {
										echo "<td  style=' text-align: center;'><img src=".base_url("kesiswaan/siswa/foto_keluar/".$foto_keluar)." class='tampil-foto'></td>";
									}
									?>
								</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Tahun Pelajaran (Masuk)  </label>
							<div class="col-sm-7">
								<?php echo $tahun_ajaran; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">NIS / NISN</label>
							<div class="col-sm-7">
								<?php echo $nis.' / ';
								if($nisn=='')
									echo '-';
								else
									echo $nisn;
								?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Nama Siswa</label>
							<div class="col-sm-7">
								<?php echo $nama; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Tempat / Tanggal Lahir</label>
							<div class="col-sm-7">
								<?php echo $tempat_lahir.' / ';
								if($nisn=='')
									echo '-';
								else
									echo $tanggal_lahir;
								?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Jenis Kelamin</label>
							<div class="col-sm-7">
								<?php echo $jenis_kel; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Tanggal Diterima</label>
							<div class="col-sm-7">
								<?php echo $tanggal_diterima; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Kelas Diterima</label>
							<div class="col-sm-7">
								<?php echo $kelas_diterima; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5" for="filter_thn_ajaran">Nama Ayah</label>
							<div class="col-sm-7">
								<?php echo $nama_ayah; ?>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-5" for="filter_thn_ajaran">Nama Ibu</label>
							<div class="col-sm-7">
								<?php echo $nama_ibu; ?>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-5" for="filter_thn_ajaran">Wali Siswa</label>
							<div class="col-sm-7">
								<?php echo $nama_wali; ?>
							</div>
						</div>
						<div class="form-group">
							<?php 
							echo '<a href="'.base_url("kesiswaan/siswa/daftar_siswa.php").'"><button class="btn btn-default btn-md">Kembali</button></a>'; ?>
							<?php 
							echo '<a class="pull-right" href="'.base_url("kesiswaan/siswa/edit_data_siswa.php?nis=".$nis."").'"><button class="btn btn-default btn-md">Ubah Data Siswa</button></a>'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default col-md-12 col-md-6">
				<div class="panel-heading">
					<h3 class="panel-title">RIWAYAT KELAS</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Tahun Pelajaran</th>
									<th>Kelas</th>
									<th>Wali Kelas</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i=1;
								$nis=$_GET['nis'];	
								$query="SELECT * FROM (di_kelas dk LEFT JOIN kelas k ON dk.kd_kelas=k.kd_kelas) LEFT JOIN guru g ON k.wali_kelas=g.nip_guru WHERE dk.nis='$nis'";
								$result = $conn->query($query);
								while ($row = $result->fetch_object()) {										
									echo '<tr>';
										echo '<td>'.$i.'</td>';
										echo '<td>'.$row->tahun_ajaran.'</td>';
										echo '<td>';
											if($row->tingkat_kelas!=''){
												echo $row->tingkat_kelas;
											}
											if($row->nama_kelas!=''){
												echo ' '.$row->nama_kelas.'';
											}'</td>';
										echo '<td>'.$row->nama_guru.'</td>';echo '</td>';
									echo '</tr>';
									$i++;
								}
								?>
							</tbody>
						</table>
						<div class="form-group form-group-sm">
							<div class="col-sm-12">

							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
							<?php 
							echo '<a href="'.base_url("kesiswaan/kelas/penempatan_kelas.php").'"><button class="btn btn-default btn-md">Penempatan Kelas</button></a>'; ?>
						</div>
					</div>
			</div>

		</div>
	</div>
</div>
</div>



<?php
include_once('../footer.php');
?>