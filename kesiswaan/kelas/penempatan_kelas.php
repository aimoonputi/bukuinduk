<?php
  $index = "false";
  include("../header.php");
  include("../../koneksi.php");
?>



<!-- <div class="page-title">
</div> -->
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Penempatan Siswa Per-Kelas</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
     <div class="panel panel-default" style="background-color: #2a3f54">
        <div class="panel-body">
          <p class="text-muted font-20 m-b-50" style="color: #f5f5f5 ">
            Melakukan penempatan siswa pada kelas tertentu <br>
            Penempatan dapat dilakukan secara perorangan ataupun massal
          </p>
        </div>
      </div>

<div class="panel panel-default" style="background-color: #51a6b5"> <br>
<div class="form-horizontal form-label-left" >
	<form class="form-horizontal" method="post" id="form-penempatan-kelas" action="fungsi_penempatan_kelas.php">
 	<div class="form-group" >
	    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tahun_ajaran_masuk" style="color: #2a3f54">Tahun Pelajaran</label>
	    <div class="col-md-4 col-sm-4 col-xs-12">
	    	<select class="form-control"  id="tahun-ajaran" name="tahun_ajaran" value="<?php if (isset($kelas_diterima)){echo $kelas_diterima; } ?>">
					<option value="none">- Pilih tahun pelajaran -</option>
					<?php
					$query=mysqli_query($conn,"SELECT * FROM kelas GROUP BY tahun_ajaran ORDER BY tahun_ajaran DESC");
					while($query1=$query->fetch_object()){
						echo '<option value="'.$query1->tahun_ajaran.'"';
						echo '>'.$query1->tahun_ajaran.'</option>';
					}
					?>
			</select>
	    </div>
	</div>


	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12" style="color: #2a3f54 ">Kelas</label>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<select class="form-control" id="kelas" name="kelas" value="<?php if (isset($kelas_diterima)){echo $kelas_diterima; } ?>">
					<option value="none">- Pilih Kelas -</option>
				</select>
			</div>
	</div>

    <div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12" for="filter_thn_ajaran" style="color: #2a3f54 ">NIS</label>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<textarea rows="10" class="form-control" name="nis" placeholder="Masukkan NIS Siswa"></textarea>
			<label class="text-muted" style="color: #2a3f54"> Pisahkan NIS dengan tanda koma (,)
			</label>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
		<div class="col-sm-4">
			<input type="submit" class="btn btn-primary form-control" style="background-color: #2a3f54" id="masukkan" name="submit" value="Tempatkan Siswa" href="penempatan_kelas.php" >
		</div>
	</div>
	</form>
</div> <br>
</div>

<script>
	$(document).ready(function(){
		$('#tahun-ajaran').change(function(){
			if($("#tahun-ajaran").val()=="none"){
				var thn='';
			}else{
				var thn = $("#tahun-ajaran").val();
			}
			$.ajax({
				url:"ajax/get_kelas.php?thn="+thn,
				type:"GET",
				dataType:"html",
				
				beforeSend: function(){
					$("#kelas").html('Loading...');
				},
				success: function(data){
					$("#kelas").html(data);
				},
				error: function(){
					$("#kelas").html("");
				}
			});
		});
	});
</script>

<?php if (isset($_SESSION['alert'])): ?>
	<script>alert("<?php echo $_SESSION['alert'] ?>")</script>
	<?php unset($_SESSION['alert']) ?>
<?php endif ?>

<?php
	include_once('../footer.php');
?>