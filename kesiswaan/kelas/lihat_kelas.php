<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<?php
// ==================================================================
// PROSES SUBMIT
// ==================================================================
if (isset($_POST["submit"])) {
  $tingkat_kelas = test_input($_POST['tingkat_kelas']);
  $nama_kelas = test_input($_POST['nama_kelas']);
  $wali_kelas = test_input($_POST['wali_kelas']);
  $tahun_ajaran = test_input($_POST['tahun_ajaran']);
  
 
   $panjang_tahun_ajaran = 9; 
  if ($tahun_ajaran == "") {
    // $error_tahun_ajaran = "Tahun harus diisi";
    // $valid_tahun_ajaran = FALSE;
  }elseif (strlen($tahun_ajaran)!=$panjang_tahun_ajaran) {
    // $error_tahun_ajaran='Harap Cek format Tahun Ajaran (Contoh: 2016/2017)';
    echo "<script>alert('Harap Cek format Tahun Ajaran (Contoh: 2016/2017)')</script>";
    $valid_tahun_ajaran = FALSE;
  }else {
    $valid_tahun_ajaran = TRUE;
  }
  
  // $panjang_tahun_ajaran = 9; 
  // if ($tahun_ajaran == "") {
  // }elseif (strlen($tahun_ajaran)!=$panjang_tahun_ajaran) {
  //   echo "<script>alert('Harap Cek format Tahun Ajaran (Contoh: 2016/2017)')</script>";
  //   $valid_tahun_ajaran = FALSE;
  // }elseif(!preg_match("/^[0-9]*$/",$tahun_ajaran)) {
  //   echo "<script>alert('Tahun Ajaran hanya mengizinkan angka')</script>";
  //   $valid_tahun_ajaran= FALSE;
  // }else {
  //   $valid_tahun_ajaran = TRUE;
  // }


  if ($tingkat_kelas == "") {
    $error_tingkat_kelas = "Tingkat Kelas harus diisi";
    $valid_tingkat_kelas = FALSE;
  }else {
    $valid_tingkat_kelas = TRUE;
  }
  if ($nama_kelas == "") {
    $error_nama_kelas = "Nama Kelas harus diisi";
    $valid_nama_kelas = FALSE;
  }else {
    $valid_nama_kelas = TRUE;
  }
  if ($wali_kelas == "") {
    $error_wali_kelas = "Wali Kelas harus diisi";
    $valid_wali_kelas = FALSE;
  }else {
    $valid_wali_kelas = TRUE;
  }

  if ($valid_tingkat_kelas && $valid_nama_kelas && $valid_wali_kelas && $valid_tahun_ajaran) {
    $tahun_ajaran = $conn->real_escape_string($tahun_ajaran);
    $tingkat_kelas = $conn->real_escape_string($tingkat_kelas);
    $nama_kelas = $conn->real_escape_string($nama_kelas);
    $wali_kelas = $conn->real_escape_string($wali_kelas);

    $exp_thn=explode("/",$tahun_ajaran);
		$exp_thn=substr($exp_thn[0],2,2);
    if ($tingkat_kelas == 'VII') {
      $no_kelas = 7;
    } else if ($tingkat_kelas == 'VIII') {
      $no_kelas = 8;
    } else {
      $no_kelas = 9;
    }
    $kd_kelas = $exp_thn.$no_kelas.$nama_kelas;

    $query = "INSERT INTO kelas (kd_kelas, tingkat_kelas, nama_kelas, wali_kelas, tahun_ajaran)
    VALUES ('$kd_kelas', '$tingkat_kelas', '$nama_kelas', '$wali_kelas', '$tahun_ajaran')";
    $result = $conn->query($query);
    if ($result) {
      // echo "<script>alert('Data Kelas Berhasil Ditambah')</script>";
    }else {
      echo "<script>alert('Kelas Yang Ditambahkan Sudah Ada')</script>";
    }
  }
}

// ==================================================================
// PROSES EDIT
// ==================================================================
if (isset($_POST["ubah-submit"])) {
  $tahun_ajaran = test_input($_POST['ubah-tahun_ajaran']);
  $tingkat_kelas = test_input($_POST['ubah-tingkat_kelas']);
  $nama_kelas = test_input($_POST['ubah-nama_kelas']);
  $wali_kelas = test_input($_POST['ubah-wali_kelas']);
  $kd_kelas_lama = $_POST['ubah-kd_kelas'];

  if ($tahun_ajaran == "") {
    $error_tahun_ajaran= "tahun_ajaran harus diisi";
    $valid_tahun_ajaran = FALSE;
  }else {
    $valid_tahun_ajaran = TRUE;
  }
  if ($tingkat_kelas == "") {
    $error_tingkat_kelas = "tingkat_kelas harus diisi";
    $valid_tingkat_kelas = FALSE;
  }else {
    $valid_tingkat_kelas = TRUE;
  }
  if ($nama_kelas == "") {
    $error_nama_kelas= "nama_kelas harus diisi";
    $valid_nama_kelas = FALSE;
  }else {
    $valid_nama_kelas = TRUE;
  }
  if ($wali_kelas == "") {
    $error_wali_kelas= "wali_kelas harus diisi";
    $valid_wali_kelas = FALSE;
  }else {
    $valid_wali_kelas = TRUE;
  }

  if ($valid_tahun_ajaran && $valid_tingkat_kelas && $valid_nama_kelas && $valid_wali_kelas) {
    $tahun_ajaran = $conn->real_escape_string($tahun_ajaran);
    $tingkat_kelas = $conn->real_escape_string($tingkat_kelas);
    $nama_kelas = $conn->real_escape_string($nama_kelas);
    $wali_kelas = $conn->real_escape_string($wali_kelas);
    $kd_kelas_lama = $conn->real_escape_string($kd_kelas_lama);

    $exp_thn=explode("/",$tahun_ajaran);
    $exp_thn=substr($exp_thn[0],2,2);
    if ($tingkat_kelas == 'VII') {
      $no_kelas = 7;
    } else if ($tingkat_kelas == 'VIII') {
      $no_kelas = 8;
    } else {
      $no_kelas = 9;
    }
    $kd_kelas = $exp_thn.$no_kelas.$nama_kelas;

	$query = "UPDATE kelas SET tahun_ajaran = '$tahun_ajaran', tingkat_kelas= '$tingkat_kelas', nama_kelas= '$nama_kelas', wali_kelas= '$wali_kelas',kd_kelas='$kd_kelas' WHERE kd_kelas='$kd_kelas_lama'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data Kelas Berhasil Diubah')</script>";
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
  }
}

// ==================================================================
// PROSES HAPUS
// ==================================================================
if (isset($_POST["hapus-submit"])) {
  $kd_kelas = test_input($_POST['hapus-kd_kelas']);

	$query = "DELETE FROM kelas WHERE kd_kelas = '$kd_kelas'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data Kelas Berhasil Dihapus')</script>";
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
}

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar Kelas</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
      
      <div class="panel panel-default" style="background-color: #2a3f54" >
        <div class="panel-body" >
          <p class="text-muted font-20 m-b-50" style="color: #f5f5f5 ">
            Menambahkan Data Kelas Baru dengan Memasukkan: Tahun Ajaran, Kelas dan Nama Wali Kelas
          </p>
          <div class="col-md-10">
            <button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success" style="background-color: #51a6b5" id="btn-tbh-kelas"
            data-toggle="tooltip" title="Menambahkan kelas"> <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah</button>
          </div>
        </div>
      </div>


      
      <div class="">
        <select class="form-control" id="filter_thn_ajaran">
          <option value="all">Semua Tahun</option>
          <?php
          $query=mysqli_query($conn,"SELECT * FROM kelas GROUP BY tahun_ajaran ORDER BY tahun_ajaran DESC");
          while($query1=$query->fetch_object()){
            echo '<option value="'.$query1->tahun_ajaran.'"';
            echo '>'.$query1->tahun_ajaran.'</option>';
          }
          ?>
        </select>
      </div>

        <?php
          require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
          $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
          if ($db->connect_errno) {
            die("Could not connet to database:<br/> ".$db->connect_error);
          }
          $query = "SELECT kelas.*,guru.nama_guru FROM kelas INNER JOIN guru on kelas.wali_kelas = guru.nip_guru ORDER BY tahun_ajaran DESC";
          $result = $db->query($query);
          // var_dump($result);die;
          $i = 1;
        ?>
        <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th style=" text-align: center;" width="5%">No</th>
              <th style=" text-align: center;" width="10%">Tingkat Kelas</th>
              <th style=" text-align: center;" width="10%">Nama Kelas</th>
              <th style=" text-align: center;" width="20%">Wali Kelas</th>
              <th style=" text-align: center;" width="15%">Tahun Pelajaran</th>
              <th style=" text-align: center;" width="10%">Jumlah Siswa</th>
              <th style=" text-align: center;" width="20%">Aksi</th>
            </tr>
          </thead>

          <tbody id="daftar_kelas">
          <?php
          while ($row = $result->fetch_object())
          {
            echo "<tr>";
            echo "<td align=center>".$i."</td>";
            echo "<td align=center>".$row->tingkat_kelas."</td>";
            echo "<td align=center>".$row->nama_kelas."</td>";
            echo "<td>".$row->nama_guru ."</td>";
            echo "<td align=center>".$row->tahun_ajaran ."</td>";
            $query_jml_siswa = "SELECT * FROM di_kelas WHERE kd_kelas='".$row->kd_kelas."' AND tahun_ajaran='".$row->tahun_ajaran."'";
            $result_jml_siswa = mysqli_query($conn, $query_jml_siswa);
            // echo $row->kd_kelas."<br>".$row->tahun_ajaran."<br>";
            echo "<td align=center>".mysqli_num_rows($result_jml_siswa)."</td>";
            echo "<td align=center colspan='2'>";

            // echo "<button class='btn btn-info btn-lihat-siswa'";
            // echo "<i class='fa fa-eye'></i> Lihat </button>";
            echo "<button class='btn btn-warning btn-ubah-kelas'";
            echo "data-tahun_ajaran='".$row->tahun_ajaran."'";
    			  echo "data-tingkat_kelas='".$row->tingkat_kelas."'";
    			  echo "data-nama_kelas='".$row->nama_kelas."'";
    			  echo "data-wali_kelas='".$row->wali_kelas."' data-kd_kelas='".$row->kd_kelas."'>";
    			  echo "<i class='fa fa-edit'></i> Ubah </button>";

            echo "<button class='btn btn-danger btn-hapus-kelas'";
            echo "data-kd_kelas='".$row->kd_kelas."'>";
            echo "<i class='fa fa-remove'></i> Hapus</button>";

            echo "</td>";
            echo "</tr>";
            $i++;
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- MODAL TAMBAH KELAS -->
<!-- ======================================================================= -->
<div class="modal fade" id="modalTambah" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Tambah Kelas Baru</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="form-login">
          <div class="box-body">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-3 control-label">Tahun Ajaran :</label>
                <div class="col-sm-8">
                    <input type="text" name="tahun_ajaran" class="form-control" placeholder="Contoh : 2016/2017" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
                    <?php
                      if (isset($error_tahun_ajaran)){
                        echo '<span id="helpBlock" class="text-danger">'.$error_tahun_ajaran.'</span>';
                      } ?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Tingkat Kelas :</label>
              <div class="col-sm-8">
                  <select class="form-control" name="tingkat_kelas" required>
                    <option value="VII">VII</option>
                    <option value="VIII">VIII</option>
                    <option value="IX">IX</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama Kelas :</label>
              <div class="col-sm-8">
                  <select class="form-control" name="nama_kelas" required>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Wali Kelas :</label>
              <div class="col-sm-8">
                  <select class="form-control" name="wali_kelas" id="wali-kelas" required>
                  </select>
              </div>
            </div>
          </div>
      </div>
        <div class="modal-footer">
         <!--  <div class="col-sm-12">
            <div class="form-group"> -->
              <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
               <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- MODAL UBAH KELAS -->
<!-- ======================================================================= -->
<div class="modal fade" id="modal-ubah-kelas" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Ubah Kelas</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="form-ubah-kelas">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Tahun Ajaran :</label>
              <div class="col-sm-8">
                  <input id="ubah-tahun_ajaran" type="text" name="ubah-tahun_ajaran" class="form-control" required readonly style="cursor: not-allowed;">
              </div>
            </div>
          </div>

          <div class="box-group">
            <div class="form-group">
            <label class="col-sm-3 control-label">Tingkat Kelas :</label>
            <div class="col-sm-8">
                <select id="ubah-tingkat_kelas" class="form-control" name="ubah-tingkat_kelas" readonly style="cursor: not-allowed;">
                  <option value="VII">VII</option>
                  <option value="VIII">VIII</option>
                  <option value="IX">IX</option>
                </select>
            </div>
          </div>

          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama Kelas :</label>
              <div class="col-sm-8">
                  <select id="ubah-nama_kelas" class="form-control" name="ubah-nama_kelas" readonly style="cursor: not-allowed;">
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                  </select>
              </div>
            </div>
          </div>

          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Wali Kelas :</label>
              <div class="col-sm-8">
                  <select class="form-control" name="ubah-wali_kelas" id="ubah-wali-kelas" required>
                  </select>
              </div>
            </div>
          </div>
          <input id="ubah-kd_kelas" type="text" name="ubah-kd_kelas" required hidden>
          <!-- <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Wali Kelas :</label>
              <div class="col-sm-8">
                  <input id="ubah-wali_kelas" type="text" name="ubah-wali_kelas" class="form-control" placeholder="Masukkan Wali Kleas" required>
              </div>
            </div>
          </div> -->

        </div>
        <div class="modal-footer">
          <!-- <div class="col-sm-12">
            <div class="form-group"> -->
              <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>

<!-- ================================================================ -->
<!-- MODAL HAPUS guru -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-hapus-kelas" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Hapus Kelas</h4>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" style="text-align:center;">Apakah anda yakin menghapus kelas ini?</h4>
        <!-- <p>Apakah Anda yakin menghapus kelas ini?</p> -->
      </div>
      <div class="modal-footer">
        <form role="form" method="post" class="form-hapus-kelas" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
        <!-- <form role="form" method="post" class="form-hapus-kelas"> -->
          <input type="hidden" name="hapus-kd_kelas" id="hapus-kd_kelas">
          <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
          <button type="submit" class="btn btn-primary" name="hapus-submit">Hapus</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </form>
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {
    $('#btn-tbh-kelas').on('click', function() {
        $.ajax({
            url:"ajax/get_walikelas.php",
            type:"GET",
            dataType:"html",

            beforeSend: function(){
                $("#wali-kelas").html('Loading...');
            },
            success: function(data){
                $("#wali-kelas").html(data);
            },
            error: function(){
                $("#wali-kelas").html("gagal memuat data");
            }
        });
    });

	// ======================================================
	// INISIALISASI MODAL UBAH KLEAS
	// ======================================================
    $("#daftar_kelas").on("click", ".btn-ubah-kelas", function(){
        var tahun_ajaran = $(this).data("tahun_ajaran");
        var tingkat_kelas = $(this).data("tingkat_kelas");
        var nama_kelas = $(this).data("nama_kelas");
        var wali_kelas = $(this).data("nama_wali_kelas");
        var kd_kelas = $(this).data("kd_kelas");

		    $("#ubah-tahun_ajaran").val(tahun_ajaran);
        $("#ubah-tingkat_kelas").val(tingkat_kelas);
        $("#ubah-nama_kelas").val(nama_kelas);
        $("#ubah-wali_kelas").val(wali_kelas);
        $("#ubah-kd_kelas").val(kd_kelas);

        $("#modal-ubah-kelas").modal();

    });
 
    $("#daftar_kelas").on('click','.btn-ubah-kelas', function() {
        var wali_kelas = $(this).data("wali_kelas");
        $.ajax({
            url:"ajax/get_walikelas.php?wali="+wali_kelas,
            type:"GET",
            dataType:"html",

            beforeSend: function(){
                $("#ubah-wali-kelas").html('Loading...');
            },
            success: function(data){
                $("#ubah-wali-kelas").html(data);
            },
            error: function(){
                $("#ubah-wali-kelas").html("gagal memuat data");
            }
        });
    });

  // ======================================================
  // INISIALISASI MODAL HAPUS KELAS
  // ======================================================
    $("#daftar_kelas").on("click", ".btn-hapus-kelas", function(){
        var kd_kelas = $(this).data("kd_kelas");
    $("#hapus-kd_kelas").val(kd_kelas);

        $("#modal-hapus-kelas").modal();
    });


    $('#filter_thn_ajaran').change(function(){
			var tahun = $("#filter_thn_ajaran").val();

			$.ajax({
				url:"ajax/filter.php?kelolakelas=list&tahun="+tahun,
				type:"GET",
				dataType:"html",

				beforeSend: function(){
					$("#daftar_kelas").html('Loading...');
				},
				success: function(data){
					$("#daftar_kelas").html(data);
				},
				error: function(){
					$("#daftar_kelas").html("gagal memuat data");
				}
			});
		});
});
</script>

<?php
include("../footer.php");
?>
