
<?php
  $index = "false";
  include("../koneksi.php");
	
	$query=mysqli_query($conn,"SELECT * FROM siswa");
	$jml_siswa=mysqli_num_rows($query);

	$query=mysqli_query($conn,"SELECT * FROM siswa WHERE jenis_kel='L'");
	$jml_siswa_laki=mysqli_num_rows($query);

	$jml_siswa_perempuan=$jml_siswa-$jml_siswa_laki;
	
	$query=mysqli_query($conn,"SELECT * FROM guru");
	$jml_guru=mysqli_num_rows($query);
?>

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>DASHBOARD</h2>
				<ul class="pull-right nav navbar-right panel_toolbox">
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="panel panel-default" style="background-color: #2a3f54">
			<div class="panel-body" style="color: #f5f5f5 ">
				<h5>Selamat Datang
					<span><b><?php echo $_SESSION["nama"];?></b></span> 
					, di Buku Induk Siswa SMP Negeri 33 Semarang
				</h5>
			</div>
		</div>
		<div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" style="background-color: #2a3f54">
                  <div class="icon"><i class="fa fa-users"></i></div>
                  <div class="count"> <?php echo $jml_siswa; ?> </div>
                  <!-- <div class="main-text">  -->
                  <h3 style="color: #f7f7f7">Total Siswa</h3>
                  <p style="color: #f7f7f7">Jumlah siswa <br> di SMP Negeri 33 Semarang.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" style="background-color: #2a3f54">
                  <div class="icon"><i class="fa fa-male"></i></div>
                  <div class="count"> <?php echo $jml_siswa_laki; ?></div>
                  <h3 style="color: #f7f7f7">Laki-laki</h3>
                  <p style="color: #f7f7f7">Jumlah siswa laki-laki <br> di SMP Negeri 33 Semarang.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" style="background-color: #2a3f54">
                  <div class="icon"><i class="fa fa-female"></i></div>
                  <div class="count"> <?php echo $jml_siswa_perempuan; ?> </div>
                  <h3 style="color: #f7f7f7">Perempuan</h3>
                  <p style="color: #f7f7f7">Jumlah siswa perempuan <br> di SMP Negeri 33 Semarang.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" style="background-color: #2a3f54">
                  <div class="icon"><i class="fa fa-users"></i></div>
                  <div class="count">  <?php echo $jml_guru; ?> </div>
                  <h3 style="color: #f7f7f7">Total Guru</h3>
                  <p style="color: #f7f7f7">Jumlah guru <br> di SMP Negeri 33 Semarang.</p>
                </div>
              </div>
            </div>
			</div>
		</div>
	</div>

