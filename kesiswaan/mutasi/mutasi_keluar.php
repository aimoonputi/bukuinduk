<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");



$status="k";
// require_once('header.php');
//$error_nis = '';
$error_nama = '';
$error_jenis_kel = '';
$error_tahun_ajaran = '';
// $error_kelas = '';
$error_tanggal_pindah = '';
$error_status = '';
$error_keterangan = '';
$valid_nis='';
$sukses='';
$ceknis='';
//Connect to Database
require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
$db = new mysqli($db_host, $db_user, $db_pass, $db_name);
if ($db->connect_errno) {
	die("Could not connet to database:<br/> ".$db->connect_error);
}

if(isset($_POST["submit"])){
	$querymut="SELECT * FROM mutasi";
	$jml=$conn->query($querymut);
	$jml=$jml->num_rows;
	$id_mutasi='m-0'+1;

	//nis
		$panjang_nis = 4;	
		$nis=test_input($_POST['nis']);	
		if($nis==''){	
			$error_nis='NIS harus diisi';
			$valid_nis=FALSE;
		}elseif (strlen($nis)!=$panjang_nis) {
			$error_nis='Harap cek format NIS. (cth: 1234)';
			$valid_nis = FALSE;
		}else{
			$cek="SELECT * FROM mutasi WHERE nis='".$nis."' AND status='".$status."'";
			$rescek=$conn->query($cek);
			if($rescek->num_rows!=0){
				$error_nis = "Siswa dengan NIS tersebut sudah melakukan mutasi keluar. Harap cek kembali NIS";
				$valid_nis = FALSE;
			}else{
				$valid_nis=TRUE;
			}
		}

		//nisn
		$nisn=test_input($_POST['nisn']);	
		if($nisn==''){
			$error_nisn='NISN harus diisi';
			$valid_nisn=FALSE;
		}else{
			$valid_nisn=TRUE;		
		}

		//nama
		$nama = test_input ($_POST['nama']);
		if ($nama == ''){
			$error_nama = "Nama Lengkap harus diisi";
			$valid_nama = FALSE;
		}else{
			$valid_nama = TRUE;
		}

		if (isset($_POST['jenis_kel'])) {
			$jenis_kel=test_input($_POST['jenis_kel']);
		// 		if($jenis_kel==''){
		// 			$error_jenis_kel='Pilih jenis kelamin';
		// 			$valid_jenis_kel=FALSE;
		// 		}else{
			$valid_jenis_kel=TRUE;
		// 		}
		}else {
			$error_jenis_kel='Pilih Jenis Kelamin';
			$valid_jenis_kel=FALSE;
		}

		//thn
		$tahun_ajaran=test_input($_POST['tahun_ajaran']);	
		if($tahun_ajaran==''){
			$error_tahun_ajaran='Tahun Ajaran Pindah harus diisi';
			$valid_tahun_ajaran=FALSE;
		}else{
			$valid_tahun_ajaran=TRUE;	
		}

		//kelas
		// if(!isset($_POST['kelas'])){
		// 	$kelas='';
		// }else{
		// 	$kelas = test_input($_POST['kelas']);
		// }
		// if ($kelas == '' || $kelas == 'none'){
		// 	$error_kelas = "Silahkan pilih kelas";
		// 	$valid_kelas =FALSE;
		// }else{
		// 	$valid_kelas =TRUE;
		// }

		//tanggal
		$tanggal_pindah=test_input($_POST['tanggal_pindah']);
		if($tanggal_pindah==''){
			$error_tanggal_pindah='Tanggal pindah harus diisi';
			$valid_tanggal_pindah=FALSE;
		}else{
			$valid_tanggal_pindah=TRUE;
		}

		//sekolah tujuan
		$sekolah_tujuan = test_input ($_POST['sekolah_tujuan']);
		if ($sekolah_tujuan == ''){
			$error_sekolah_tujuan = "Nama Sekolah Tujuan harus diisi";
			$valid_sekolah_tujuan = FALSE;
		}else{
			$valid_sekolah_tujuan = TRUE;
		}
		//alamat sekolah tujuan
		$alamat_sekolah_tujuan = test_input ($_POST['alamat_sekolah_tujuan']);
		if ($alamat_sekolah_tujuan == ''){
			$error_alamat_sekolah_tujuan = "Alamat Sekolah Tujuan harus diisi";
			$valid_alamat_sekolah_tujuan = FALSE;
		}else{
			$valid_alamat_sekolah_tujuan = TRUE;
		}
		//keterangan
		$keterangan = test_input ($_POST['keterangan']);
		if ($keterangan == ''){
			$error_keterangan = "Keterangan Mutasi Pindah harus diisi";
			$valid_keterangan = FALSE;
		}else{
			$valid_keterangan = TRUE;
		}

		//input data into database
	if (($valid_nis==TRUE)  && ($valid_tahun_ajaran==TRUE) && ($valid_tanggal_pindah==TRUE) && ($valid_sekolah_tujuan==TRUE) && ($valid_alamat_sekolah_tujuan==TRUE) && ($valid_keterangan==TRUE)){
		//escape inputs data
		$nis = $conn->real_escape_string($nis);
		$jenis_kel = $conn->real_escape_string($jenis_kel);
		// $kelas = $conn->real_escape_string($kelas);
		$tanggal_pindah = $conn->real_escape_string($tanggal_pindah);
		$sekolah_tujuan = $conn->real_escape_string($sekolah_tujuan);
		$alamat_sekolah_tujuan = $conn->real_escape_string($alamat_sekolah_tujuan);
		$status = $conn->real_escape_string($status);
		$keterangan = $conn->real_escape_string($keterangan);
		//Asign a query
		$query = "INSERT INTO mutasi(id_mutasi, thn, nis, status, sch_tujuan, alamat_sch_tujuan, tanggal, keterangan) VALUES ('".$id_mutasi."','".$thn."','".$nis."','".$status."', '".$sch_tujuan."', '".$alamat_sch_tujuan."', '".$tanggal."','".$keterangan."')";
		// Execute the query
		$result = $db->query( $query );
		if (!$result){
		   die ("Tidak dapat menjalankan query database: <br />". $db->error);
		}else{
			$sukses=TRUE;
		}
		$pesan_sukses="Berhasil menambahkan data";
	}
	else{
		$sukses=FALSE;
	}
}



//cari data berdasarkan nis
if(isset($_POST["cek"])){
	//nis
	$nis = test_input ($_POST['nis']);
		if($nis==''){	
			$error_nis='NIS harus diisi';
			$valid_nis=FALSE;
		}elseif (strlen($nis)!=$panjang_nis) {
			$error_nis='Harap cek format NIS. (cth: 1234)';
			$valid_nis = FALSE;
		}else{
		$cek="SELECT * FROM mutasi WHERE nis='".$nis."' AND status='".$status."'";
		$rescek=$conn->query($cek);
		if($rescek->num_rows!=0){
			$error_nis = "Siswa dengan NIS tersebut sudah melakukan mutasi keluar. Harap cek kembali NIS";
			
		}else{
			$cek="SELECT * FROM siswa WHERE nis='".$nis."'";
			$rescek=$conn->query($cek);
			if($rescek->num_rows==0){
				$error_nis = "NIS tidak ditemukan. Harap cek kembali NIS";
			}else{
				$query1 = " SELECT * FROM siswa  WHERE nis='".$nis."'";
				// Execute the query
				$result1 = $db->query( $query1 );
				if (!$result1){
					die ("Could not query the database: <br />". $db->error);
				}else{
					while ($row = $result1->fetch_object()){
						$nama				= $row->nama;
						$jnskel				= $row->jns_kel;
					}
				}
				
				$query2 = " SELECT kelas.nama_kelas AS nama_kelas, berada_di_kelas.tahun AS tahun FROM berada_di_kelas  JOIN kelas ON berada_di_kelas.kd_kelas=kelas.kd_kelas WHERE berada_di_kelas.nis='".$nis."' ORDER BY tahun desc ";
				// Execute the query
				$result2 = $db->query( $query2 );
				if (!$result2){
					die ("Could not query the database: <br />". $db->error);
				}else{
					$row = $result2->fetch_object();
					$thn = $row->tahun;
					$kelas = $row->nama_kelas;
				}
				
				$ceknis=TRUE;
			}
		}
	}
}
?>



<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Data Mutasi Siswa Keluar</h2>
				<ul class="pull-right nav navbar-right panel_toolbox">
					<li><a class="collapse-link"></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="form-group">
						<label class="col-sm-3">NIS</label>&nbsp;<span class="label label-warning">* <?php //if(isset($error_nis)) echo $error_nis;?></span>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="nis" pattern="[0-9]{2}[.][0-9]{2}[.][0-9]{2}[.][0-9]{2}[.][0-9]{5}" placeholder="00.00.00.00.00000" autofocus value="<?php if(isset($nis)) {echo $nis;} ?>">
						</div>
						<div class="col-sm-2">
							<input class="btn btn-success form-control" type="submit" name="cek" value="Cek NIS" >
						</div>
					</div>
		</div>
	</div>
</div>


<?php
include_once('../footer.php');
?>