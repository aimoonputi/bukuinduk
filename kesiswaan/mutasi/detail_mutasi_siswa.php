<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Detail Mutasi Siswa Masuk</h2>
				<div class="clearfix"></div>
			</div>
			<p class="text-muted font-13 m-b-30">
			</p>
			<?php
			$nis = $_GET['nis'];
			// $query = "SELECT * FROM mutasi";
			$query = "SELECT * FROM siswa JOIN mutasi ON siswa.nis=mutasi.nis WHERE siswa.nis='$nis'";
			$result = $conn->query($query);
			$row = $result->fetch_object();
			// $i = 1; 
			?>

			<div class="panel panel-default col-md-6"> <br>
			<table id="datatable" class="table table-striped">
				<tbody>
					<!-- <div class="form-group form-group-sm">
							<label class="col-sm-5" for="filter_thn_ajaran">Nama Siswa</label>
							<div class="col-sm-7">
								<?php echo $row->nis
								?> </div> </div> -->	
					<?php
					// echo '<table border="0">';
					echo '<tr>';
					echo '<td>NIS</td>';
					echo '<td> : '.$row->nis.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>NISN</td>';
					echo '<td> : '.$row->nisn.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Nama</td>';
					echo '<td> : '.$row->nama.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Jenis Kelamin</td>';
					echo '<td> : '.$row->jenis_kel.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Tahun Ajaran (Masuk) </td>';
					echo '<td> : '.$row->tahun_ajaran.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Tanggal Diterima</td>';
					echo '<td> : '.$row->tanggal_diterima.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Kelas Diterima</td>';
					echo '<td> : '.$row->kelas_diterima.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Sekolah Asal</td>';
					echo '<td> : '.$row->sekolah_asal.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Alamat Sekolah Asal</td>';
					echo '<td> : '.$row->alamat_sekolah_asal.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>Keterangan</td>';
					echo '<td> : '.$row->keterangan.'</td>';
					echo '</tr>';
					echo '</td>';
					echo '</tr>';
					?>
				</tbody>	
			</table>
		</div>
	</div>
	<?php 
		echo '<table border="0">';
		echo '<a href="'.base_url("kesiswaan/mutasi/daftar_mutasi_siswa.php").'"><button class="btn btn-default btn-md">Kembali</button></a>'; ?>
</div>
</div>

<?php
include_once('../footer.php');
?>