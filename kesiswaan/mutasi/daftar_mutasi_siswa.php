<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Daftar Siswa</h2>
				<div class="clearfix"></div>
			</div>
			<p class="text-muted font-13 m-b-30">
				<!-- teks keterangan tentang tabel, terserah diisi ,m,mapa -->
			</p>


			<?php
			require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
			$db = new mysqli($db_host, $db_user, $db_pass, $db_name);
			if ($db->connect_errno) {
				die("Could not connet to database:<br/> ".$db->connect_error);
			}
			$query = "SELECT * FROM mutasi";
			$query = "SELECT * FROM siswa JOIN mutasi ON siswa.nis=mutasi.nis ";
			$result = $db->query($query);
			$i = 1;

			function test_input($data){
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}
			?>
			<table id="datatable" class="table table-striped table-bordered">
				<thead >
					<tr >
						<th style=" text-align: center;" width="5%">No</th>
						<th style=" text-align: center;" width="10%">NIS</th>
						<th style=" text-align: center;" width="20%">Nama</th>
						<th style=" text-align: center;" width="5%">Jenis Kelamin</th>
						<th style=" text-align: center;" width="5%">Status</th>
						<th style=" text-align: center;" width="5%">Tahun Ajaran</th>
						<th style=" text-align: center;" width="30%">Keterangan</th>
						<th style=" text-align: center;" width="20%">Aksi</th>
					</tr>
				</thead>
				<tbody id="hapus-nis">
					<?php
					while ($row = $result->fetch_object())
					{
						echo "<tr>";
						echo "<td style=' text-align: center;'>".$i."</td>";
						echo "<td style=' text-align: center;''>".$row->nis."</td>";
						echo "<td>".$row->nama."</td>";
						echo "<td style=' text-align: center;'>".$row->jenis_kel."</td>";
						echo "<td style=' text-align: center;''>".$row->status."</td>";
						echo "<td style=' text-align: center;''>".$row->tahun_ajaran."</td>";
						echo "<td>".$row->keterangan."</td>";

						echo '<td  style=" text-align: center;">
						<a href="'.base_url("kesiswaan/mutasi/detail_mutasi_siswa.php?nis=".$row->nis."").'"><button title="Lihat Detail Mutasi Siswa" class="btn btn-info"><i class="fa fa-eye"></i></button></a>
						<a href="'.base_url("kesiswaan/mutasi/edit_mutasi_siswa.php?nis=".$row->nis."").'"><button title="Edit Data Siswa" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i> </button></a>
						<button class="btn btn-danger btn-hapus-mutasi" title="Hapus Data Mutasi Siswa" data-toggle="modal" data-nis='.$row->nis.' data-target="#modal-hapus-mutasi" id="hapus-mutasi"> <i class="fa fa-trash"></i></button>
						</td>';
						echo "</td>";
						echo "</tr>";
						$i++;
					}
					?>

					<?php
					$i = 1;
					// ==================================================================
					// PROSES HAPUS
					// ==================================================================
					if (isset($_POST["hapus-submit"])) {
						$nis = test_input($_POST['hapus-nis']);
						// $query_select = "SELECT * FROM siswa WHERE nis = '$nis'";
						// $result_select = $conn->query($query_select);
						// $row = $result_select->fetch_object(); 

						$query = "DELETE FROM siswa WHERE nis = '$nis'";
						// $result = $conn-> query($query);

						$query = "DELETE FROM di_kelas WHERE nis = '$nis'";
						// $result = $conn-> query($query);

						$query = "DELETE FROM mutasi WHERE nis = '$nis'";
						// $result = $conn-> query($query);

						$result = $conn->query($query);
						// $result = $conn->query($query1);
						// $result = $conn->query($query2);

						if ($result) {
							echo "<script>alert('Data Siswa Berhasil Dihapus')</script>";
						}else {
							die("Could not query the database: <br/>". $conn->error);
						}
						// if ($result) {
						// 	echo "<script>alert('Data Guru Berhasil Dihapus')</script>";
						// }else {
						// 	die("Could not query the database: <br/>". $conn->error);
						// }
					}
					// $query = "SELECT * FROM siswa";
          // $result = $conn->query($query);
					// $query1 = "SELECT * FROM di_kelas";
          // $result = $conn->query($query1);
					// $query2 = "SELECT * FROM mutasi";
					// $result = $conn->query($query && $query1 && $query2);

						 // $query1 = "SELECT * FROM siswa";
       //    $result = $conn->query($query1);
					?>


				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- ================================================================ -->
<!-- MODAL HAPUS Siswa -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-hapus-mutasi" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Hapus Mutasi Siswa</h4>
			</div>
			<div class="modal-body">
				<h4 class="modal-title" style="text-align:center;">Apakah anda yakin ingin menghapus siswa ini?</h4>
			</div>
			<div class="modal-footer">
				<form role="form" method="post" class="form-hapus-mutasi" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
					<input type="hidden" name="hapus-nis" id="hapus-nis">
					<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
						<!-- <input type="hidden" name="hapus-nis" id="hapus-nis"> -->
						<button type="submit" class="btn btn-primary" name="hapus-submit">Hapus</button>
          <!-- <button type="submit" class="btn btn-danger" data-dismiss="modal">
          Batal</button> -->
          <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
  </form>
</div>
</div>
</div>
</div>


<script>
	$(document).ready(function() {
	// ======================================================
	// INISIALISASI MODAL HAPUS SISWA
	// ======================================================
	$(".btn-hapus-mutasi").on("click", function(){
		var nis = $(this).data("nis");
		$("#hapus-nis").val(nis);
		$("#modal-hapus-mutasi").modal();
	});
}
</script>

<?php if (isset($_SESSION['alert'])){?>
<script>alert('<?php echo $_SESSION['alert'] ?>')</script>
<?php unset($_SESSION['alert']);
}

include_once('../footer.php');
?>