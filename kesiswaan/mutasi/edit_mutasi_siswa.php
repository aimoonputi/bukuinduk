<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<?php
if (isset($_GET['nis'])) {
	$nis = $_GET['nis'];
}
if (!isset($_POST['simpan'])) {
	$query = "SELECT * FROM siswa WHERE nis=".$nis."";
	$result = $conn->query($query);
	while ($dt = $result->fetch_object()) {
		$query1 = "SELECT tahun_ajaran FROM kelas WHERE kd_kelas='".$dt->kelas_diterima."'";
		$result = $conn->query($query1);
		$kls = $result->fetch_object();
		if ($kls == null) {
			$tahun_ajaran = "";
		}else{
			$tahun_ajaran = $kls->tahun_ajaran;
		}	
		// $tahun_ajaran = $kls->tahun_ajaran;
		$nis = $dt->nis;
		$nisn = $dt->nisn;
		$nama = $dt->nama;
		$jenis_kel = $dt->jenis_kel;
		$kelas_diterima = $dt->kelas_diterima;
		$tanggal_diterima = $dt->tanggal_diterima;
		$sekolah_asal = $dt->sekolah_asal;
		$alamat_sekolah_asal = $dt->alamat_sekolah_asal;
		
		$query2 = "SELECT * FROM mutasi WHERE nis=".$nis."";
		$result = $conn->query($query2);
		while ($dt = $result->fetch_object()) {
		$keterangan = $dt->keterangan;
		$sekolah_asal = $dt->sekolah_asal;
		$alamat_sekolah_asal = $dt->alamat_sekolah_asal;
	}
}
}
else{
$status="m";
	// $query = "SELECT * FROM siswa WHERE nis=".$nis."";
	// $result = $conn->query($query);
	// while ($dt = $result->fetch_object()) {
// if (isset($_POST['simpan'])){
	$tahun_ajaran=test_input($_POST['tahun_ajaran']);	
	if($tahun_ajaran==''){
		$error_tahun_ajaran='Tahun Pelajaran harus diisi';
		$valid_tahun_ajaran=FALSE;
	}else{
		$valid_tahun_ajaran=TRUE;	
	}

	$panjang_nis = 4;	
	$nis=test_input($_POST['nis']);	
	if($nis==''){	
		$error_nis='NIS harus diisi';
		$valid_nis=FALSE;
	}elseif (strlen($nis)!=$panjang_nis) {
		$error_nis='Harap cek format NIS. (cth: 1234)';
		$valid_nis = FALSE;
	}else{
		$valid_nis=TRUE;
	}

	$nisn=test_input($_POST['nisn']);	
	if($nisn==''){
		$error_nisn='NISN harus diisi';
		$valid_nisn=FALSE;
	}else{
		$valid_nisn=TRUE;		
	}

	// $query = "SELECT nis, nisn FROM siswa";
	// $result = $conn->query($query);
	// while ($dt=$result->fetch_object()) {		
	// 	if ($dt->nis == $nis) {
	// 		$error_nis='NIS Sudah Digunakan';
	// 		$valid_nis = FALSE;
	// 	}elseif ($dt->nisn == $nisn) {
	// 		$error_nisn ='NIS Sudah Digunakan';
	// 		$valid_nisn = FALSE;
	// 	}
	// }

	$nama=test_input($_POST['nama']);
	if($nama==''){
		$error_nama='Nama Lengkap harus diisi';
		$valid_nama=FALSE;
	}elseif (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
        $error_nama='Nama hanya mengizinkan huruf dan spasi';
        $valid_nama=FALSE;
	}else{
		$valid_nama=TRUE;			
	}

	if (isset($_POST['jenis_kel'])) {
		$jenis_kel=test_input($_POST['jenis_kel']);
	// 		if($jenis_kel==''){
	// 			$error_jenis_kel='Pilih jenis kelamin';
	// 			$valid_jenis_kel=FALSE;
	// 		}else{
		$valid_jenis_kel=TRUE;
	// 		}
	}else {
		$error_jenis_kel='Pilih Jenis Kelamin';
		$valid_jenis_kel=FALSE;
	}

	$kelas_diterima=test_input($_POST['kelas_diterima']);
	if($kelas_diterima=='' || $kelas_diterima=='none'){
		$error_kelas_diterima='Kelas Diterima harus diisi';
		$valid_kelas_diterima=FALSE;
	}else{
		$valid_kelas_diterima= TRUE; 			
	}

	$tanggal_diterima=test_input($_POST['tanggal_diterima']);
	if($tanggal_diterima=='' || $tanggal_diterima=='none'){
		$error_tanggal_diterima='Tanggal Diterima harus diisi';
		$valid_tanggal_diterima=FALSE;
	}else{
		$valid_tanggal_diterima = TRUE; 			
	}

	$sekolah_asal=test_input($_POST['sekolah_asal']);
	if($sekolah_asal==''){
		$error_sekolah_asal='Nama Sekolah Asal harus diisi';
		$valid_sekolah_asal=FALSE;
	}else{
		$valid_sekolah_asal = TRUE; 			
	}

	$alamat_sekolah_asal=test_input($_POST['alamat_sekolah_asal']);
	if($alamat_sekolah_asal==''){
		$error_alamat_sekolah_asal='Alamat Sekolah Asal harus diisi';
		$valid_alamat_sekolah_asal=FALSE;
	}else{
		$valid_alamat_sekolah_asal = TRUE;			
	}

	$keterangan=test_input($_POST['keterangan']);
	if($keterangan==''){
		$error_keterangan='Keterangan Mutasi Masuk harus diisi';
		$valid_keterangan=FALSE;
	}else{
		$valid_keterangan = TRUE;			
	}

	$result=false;
	if($valid_tahun_ajaran && $valid_nis && $valid_nisn && $valid_nama && $valid_jenis_kel && $valid_kelas_diterima && $valid_tanggal_diterima && $valid_sekolah_asal && $valid_alamat_sekolah_asal && $valid_keterangan) {

		$tahun_ajaran = $conn->real_escape_string($tahun_ajaran);
		$nis = $conn->real_escape_string($nis);
		$nisn = $conn->real_escape_string($nisn);
		$nama = $conn->real_escape_string($nama);
		$jenis_kel = $conn->real_escape_string($jenis_kel);
		$kelas_diterima = $conn->real_escape_string($kelas_diterima);
		$tanggal_diterima = $conn->real_escape_string($tanggal_diterima);
		$sekolah_asal = $conn->real_escape_string($sekolah_asal);
		$alamat_sekolah_asal = $conn->real_escape_string($alamat_sekolah_asal);		
		$status = $conn->real_escape_string($status);
		$keterangan = $conn->real_escape_string($keterangan);

		// $query= "INSERT INTO siswa (nis, nisn, nama, jenis_kel, kelas_diterima, tanggal_diterima) VALUES ('$nis', '$nisn', '$nama', '$jenis_kel', '$kelas_diterima', '$tanggal_diterima')";
		// $result = $conn->query($query);

		$query = "UPDATE siswa SET nama='".$nama."', jenis_kel='".$jenis_kel."', kelas_diterima='".$kelas_diterima."', tanggal_diterima='".$tanggal_diterima."', sekolah_asal='".$sekolah_asal."', alamat_sekolah_asal='".$alamat_sekolah_asal."' WHERE nis='".$nis."'";	
		$result = $conn->query($query);

		// $query1 = "INSERT INTO di_kelas (nis, kd_kelas, tahun_ajaran) VALUES('$nis', '$kelas_diterima', '$tahun_ajaran')";
		// $result= $conn->query($query1);
		
		$query1 = "UPDATE di_kelas SET kd_kelas='".$kelas_diterima."' WHERE nis='".$nis."'";	
		$result = $conn->query($query1);

		// $query2="INSERT INTO mutasi (tahun_ajaran, nis, status, sekolah_asal, alamat_sekolah_asal, tanggal, keterangan) VALUES ('$tahun_ajaran', '$nis','$status', '$sekolah_asal', '$alamat_sekolah_asal', '$tanggal_diterima','$keterangan')";
		// $result=$conn->query($query2);

		$query2 = "UPDATE mutasi SET sekolah_asal='".$sekolah_asal."', alamat_sekolah_asal='".$alamat_sekolah_asal."', tanggal='".$tanggal_diterima."', keterangan='".$keterangan."' WHERE nis='".$nis."'";	
		$result = $conn->query($query2);

	}

	if ($result) {
		echo "<script>alert('Data Mutasi Berhasil di Ubah')</script>";
		echo "<script>window.location.replace('".base_url('kesiswaan/mutasi/daftar_mutasi_siswa.php')."')</script>";      
	}
}
function test_input($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}	
?>


<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Mutasi Siswa Masuk</h2>
				<ul class="pull-right nav navbar-right panel_toolbox">
					<li><a class="collapse-link"></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- <div id="wizard" class="form_wizard wizard_horizontal"> -->
				<form class="form-horizontal form-label-left" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12" >NIS *</label>
						<div class="col-sm-2">
							<input class="form-control" type="text" id="nis" name="nis" maxlength="<?php echo $jml_kar_nis?>" placeholder="Contoh : 6356" value="<?php if(isset($nis)){echo $nis;} ?>" readonly style="cursor: not-allowed;"/>
							<?php
							if(isset($error_nis)){
								echo '<span id="helpBlock" class="text-danger">'.$error_nis.'</span>';
							}
							?>
						</div>
						<label class="control-label col-md-1 col-sm-8 col-xs-12" >NISN*</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="nisn" maxlength="<?php echo $jml_kar_nisn?>" 
							placeholder="Contoh : 0047363119" value="<?php if(isset($nisn)){echo $nisn;} ?>" readonly style="cursor: not-allowed;"/>
							<?php
							if(isset($error_nisn)){
								echo '<span id="helpBlock" class="text-danger">'.$error_nisn.'</span>';
							}
							?>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama">Nama Lengkap
							<span class="required">*</span></label>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" maxlength="<?php echo $jml_kar_nama?>" value="<?php if(isset($nama)){echo $nama;} ?>"/>
								<?php
								if(isset($error_nama)){
									echo '<span id="helpBlock" class="text-danger">'.$error_nama.'</span>';
								}
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Jenis Kelamin <span class="required">*</span></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<div class="col-md-12" style="padding: 0">
									<input type="radio" id="jenis_kel" name="jenis_kel" value="L" <?php 
									if (isset($jenis_kel) && $jenis_kel == "L") {
										echo "checked";
									} ?>/> Laki-Laki 
									<input type="radio" id="jenis_kel" name="jenis_kel" value="P" <?php 
									if (isset($jenis_kel) && $jenis_kel == "P") {
										echo "checked";
									} ?>/> Perempuan									
								</div>																
								<?php
								if(isset($error_jenis_kel)){
									echo '<span id="helpBlock" class="text-danger">'.$error_jenis_kel.'</span>';
								}
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="tahun_ajaran_masuk">Tahun Pelajaran
								<span class="required">*</span></label>
								<div class="col-md-7 col-sm-7 col-xs-12">
									<input type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control col-md-7 col-xs-12"  placeholder="____/____" value="<?php if(isset($tahun_ajaran)){echo $tahun_ajaran;} ?>" readonly style="cursor: not-allowed;"/>
									<?php
									if(isset($error_tahun_ajaran)){
										echo '<span id="helpBlock" class="text-danger">'.$error_tahun_ajaran.'</span>';
									}
									?>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12" >Kelas Diterima *</label>
								<div class="col-sm-7">
									<select class="form-control" id="kelas_diterima" name="kelas_diterima" value="<?php if (isset($kelas_diterima)){echo $kelas_diterima; } ?>">
										<option value="none"> -- Pilih Kelas -- </option>
									</select>
									<?php
									if(isset($error_kelas_diterima)){
										echo '<span id="helpBlock" class="text-danger">'.$error_kelas_diterima.'</span>';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12">Tanggal *</label>&nbsp;<span <?php if(isset($error_tanggal_diterima)) echo $error_tanggal_diterima;?></span>
								<div class="col-sm-7">
									<input class="form-control" type="date" name="tanggal_diterima" value="<?php if(isset($tanggal_diterima)) {echo $tanggal_diterima;} ?>">
									<?php
									if(isset($error_tanggal_diterima)){
										echo '<span id="helpBlock" class="text-danger">'.$error_tanggal_diterima.'</span>';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12" >Pindah dari *</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="sekolah_asal" placeholder="Nama Sekolah Asal" maxlength="<?php echo $jml_kar_nama_sekolah_asal ?>" value="<?php if(isset($sekolah_asal)){echo $sekolah_asal;} ?>" />
									<?php
									if(isset($error_sekolah_asal)){
										echo '<span id="helpBlock" class="text-danger">'.$error_sekolah_asal.'</span>';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat Sekolah Asal *</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="alamat_sekolah_asal" placeholder="Alamat Sekolah Asal" maxlength="<?php echo $jml_kar_alamat_sekolah_asal ?>" value="<?php if(isset($alamat_sekolah_asal)){echo $alamat_sekolah_asal;} ?>" />
									<?php
									if(isset($error_alamat_sekolah_asal)){
										echo '<span id="helpBlock" class="text-danger">'.$error_alamat_sekolah_asal.'</span>';
									}
									?>
								</div>
								<!-- </div> -->
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12">Keterangan *</label>&nbsp;<span <?php if(isset($error_keterangan)) echo $error_keterangan;?></span>
								<div class="col-sm-7">
									<textarea name="keterangan" placeholder="Ikut orangtua pindah kerja luar kota" style="margin: 0px;width: 590px;height: 109px;"><?php if(isset($keterangan)) {echo $keterangan;} ?></textarea>
									<?php
									if(isset($error_keterangan)){
										echo '<span id="helpBlock" class="text-danger">'.$error_keterangan.'</span>';
									}
									?>
								</div>
							</div>
							<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
								<button type="submit" class="btn btn-primary" class="col-md-offset-5 col-md-2 " name="simpan">Simpan</button>
								<a class="btn btn-danger" href="daftar_mutasi_siswa.php">Batal</a>
							</div>
						</form>
						<!-- End SmartWizard Content -->
					</div>
				</div>
				</div>



				<script type="text/javascript">
	// alert("sdasd");
	if ($("#tahun_ajaran").val()!="") {
		var thn = $("#tahun_ajaran").val();		
		var nis = $("#nis").val();
		// alert(nis);
		$.ajax({
			url:"../siswa/ajax_func/ajax_func.php?listkelas=1&thn="+thn+"&nis="+nis,
			type:"GET",
			dataType:"html",
			
			beforeSend: function(){
				$("#kelas_diterima").html('Loading...');
			},
			success: function(data){
				$("#kelas_diterima").html(data);
			},
			error: function(){
				$("#kelas_diterima").html("");
			}
		});
	}		
</script>
<script type="text/javascript">	
	$("#tahun_ajaran").blur(function(){		
		if($("#tahun_ajaran").val()==undefined){
			var thn='';
		}else{
			var thn = $("#tahun_ajaran").val();
		}		
		$.ajax({
			url:"../siswa/ajax_func/ajax_func.php?listkelas=1&thn="+thn,
			type:"GET",
			dataType:"html",
			
			beforeSend: function(){
				$("#kelas_diterima").html('Loading...');
			},
			success: function(data){
				$("#kelas_diterima").html(data);
			},
			error: function(){
				$("#kelas_diterima").html("");
			}
		});
	});
</script>

<?php
include_once('../footer.php');
?>