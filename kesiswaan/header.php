<?php
session_start();
if ($index=="index") {
  include ("../base_url.php");
}elseif ($index=="false") {
  include ("../../base_url.php");
}

if (!isset($_SESSION['user'])) {
  header("location:".base_url("login.php")."");
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <!-- jQuery -->
    <script src="<?php echo base_url("assets/template/vendors/jquery/dist/jquery.min.js")?>"></script>
    <!-- Bootstrap -->
    <link href="<?php echo base_url("assets/template/vendors/bootstrap/dist/css/bootstrap.min.css")?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url("assets/template/vendors/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url("assets/template/vendors/nprogress/nprogress.css")?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url("assets/template/vendors/iCheck/skins/flat/green.css")?>" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url("assets/template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url("assets/template/vendors/jqvmap/dist/jqvmap.min.css")?>" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url("assets/template/vendors/bootstrap-daterangepicker/daterangepicker.css")?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url("assets/template/build/css/custom.min.css")?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/template/build/css/style.css")?>">
  </head>

<body class="nav-md ">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col menu_fixed">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><span>Hallo Kesiswaan!</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="<?php echo base_url("assets/template/production/images/img.jpg")?>" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span style="color: #f5f5f5 ">Welcome,</span> </br>
              <span style="color: #f5f5f5"><?php echo $_SESSION["nama"];?></span>
              <!-- <span><?php echo $_SESSION["username"];?></span> -->
            </div>
            <div style="color:#f5f5f5" align="center" >
            <?php 
              $tanggal = mktime(date('m'), date("d"), date('Y'));
              echo date("d-m-Y", $tanggal ) . "</b>";
              date_default_timezone_set("Asia/Jakarta");
              $jam = date ("H:i");
              echo " | Pukul : <b> " . $jam . " " ." </b> ";
              $a = date ("H");
              if (($a>=6) && ($a<=11))  {
                  echo "</br> Selamat Pagi !! </b>";
              }else if(($a>=11) && ($a<=15)){
                  echo "</br> Selamat  Siang !! ";
              }elseif(($a>15) && ($a<=18)){
                  echo "</br> Selamat Sore !!";
              }else{
                  echo "</br> <b> Selamat Malam </b>";
              }
           ?> </div>
          </div>
          <!-- /menu profile quick info -->
          
          <br/>

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu">
                <li><a href="<?php echo base_url("index.php")?>"><i class="fa fa-home"></i> Panel Utama </a></li>
                <li><a href="<?php echo base_url("kesiswaan/index.php")?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a><i class="fa fa-users"></i> Kelola Siswa <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="<?php echo base_url("kesiswaan/siswa/daftar_siswa.php")?>">Daftar Siswa</a></li>
                    <li><a href="<?php echo base_url("kesiswaan/siswa/tambah_data_siswa.php")?>">Tambah Data Siswa</a></li>
                    <li><a href="<?php echo base_url("kesiswaan/siswa/tambah_foto_siswa.php")?>">Tambah Foto Siswa</a></li>
                    <li><a href="<?php echo base_url("kesiswaan/siswa/import_data_siswa.php")?>">Import Data Siswa</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-pencil"></i> Kelola Kelas <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="<?php echo base_url("kesiswaan/kelas/lihat_kelas.php")?>">Tambah Data Kelas</a></li>
                    <li><a href="<?php echo base_url("kesiswaan/kelas/penempatan_kelas.php")?>">Penempatan Kelas</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-th-large"></i> Kelola Mutasi<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="<?php echo base_url("kesiswaan/mutasi/daftar_mutasi_siswa.php")?>">Daftar Mutasi Siswa </a></li>
                    <li><a href="<?php echo base_url("kesiswaan/mutasi/mutasi_masuk.php")?>">Tambah Mutasi Masuk</a></li>
                    <li><a href="<?php echo base_url("kesiswaan/mutasi/mutasi_keluar.php")?>">Tambah Mutasi Keluar</a></li>
                  </ul>
                </li>
                <li><a href="<?php echo base_url("kesiswaan/guru/lihat_guru.php")?>"><i class="fa fa-users"></i> Kelola Guru </a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
           <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class=" user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 110%;"><span class=" fa fa-lock">&ensp;</span>
                    <?php echo $_SESSION["nama"]; ?> <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a href="<?php echo base_url("ajax/ubah_password.php")?>"><i class="fa fa-key pull-right"></i>Ubah Password</a></li>
                  <li><a href="<?php echo base_url("logout.php")?>"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->


<!-- Modal Ubah Password -->
<div class="modal fade" id="mdl-ubh-pass" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Ubah Password</h4>
      </div>
      <div class="modal-body bg-info">
        <form role="form" method="post" class="form-ajx-pass"
        action="<?php echo base_url("ajax/ubah_password.php")?>">
          <div class="form-group">
            <label for="ubh-pass">Password lama:</label>
            <input type="password" class="form-control" name="ubh-pass-lama"
            id="ubh-pass-lama" placeholder="Masukkan password lama" required>
          </div>
          <div class="form-group">
            <label for="ubh-pass">Password baru:</label>
            <input type="password" class="form-control" name="ubh-pass-baru"
            id="ubh-pass-baru" placeholder="Masukkan password baru" required>
          </div>
          <div class="form-group">
            <label for="ubh-pass-konf">Konfirmasi password baru:</label>
            <input type="password" class="form-control" name="ubh-pass-konf"
            id="ubh-pass-konf" placeholder="Masukkan password baru kembali"
            required>
          </div>
          <button type="submit" class="btn btn-success">Kirim</button>
          <button type="reset" class="btn btn-info">Ulang</button>
        </form>
      </div>
      <div class="modal-footer bg-info">
        <button type="submit" class="btn btn-danger" data-dismiss="modal">
          Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Notifikasi Ubah Password -->
<div class="modal fade" id="mdl-pass-not" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Notifikasi</h4>
      </div>
      <div class="modal-body bg-info">
        <p id="psn-pass-not"></p>
      </div>
      <div class="modal-footer bg-info">
        <button type="submit" class="btn btn-danger" data-dismiss="modal">
          Tutup
        </button>
      </div>
    </div>
  </div>
</div>
<!-- page content -->
<div class="right_col" role="main">
