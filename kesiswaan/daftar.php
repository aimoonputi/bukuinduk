<?php 
    $sukses=TRUE;

    // eksekusi tombol daftar
    if (isset($_POST['daftar'])) {
        // Cek NIK
        $nik=test_input($_POST['nik']);
        if ($nik=='') {
            $errorNik='wajib diisi';
            $validNik=FALSE;
        }elseif (!preg_match("/^[0-9]{16}$/",$nik)) {
            $errorNik='NIK harus terdiri dari 16 digit angka';
            $validNik=FALSE;
        }else{
            $query = " SELECT * FROM masyarakat WHERE nik='".$nik."'";
            $result = $con->query( $query );
            if($result->num_rows!=0){
                $errorNik="NIK sudah pernah digunakan, harap masukkan NIK lain";
                $validNik=FALSE;
            }
            else{
                $validNik = TRUE;
            }
        }
    
        // Cek Nama
        $nama=test_input($_POST['nama']);
        if ($nama=='') {
            $errorNama='wajib diisi';
            $validNama=FALSE;
        }elseif (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
            $errorNama='hanya mengizinkan huruf dan spasi';
            $validNama=FALSE;
        }else{
            $validNama=TRUE;
        }

        // Cek password
        $password=test_input($_POST['password']);
        $password = md5($password);
        if ($password=='') {
            $errorPass='wajib diisi';
            $validPass=FALSE;
        }else{
            $validPass=TRUE;
        }

        // cek alamat
        $alamat=test_input($_POST['alamat']);
        if ($alamat=='') {
            $errorAlamat='wajib diisi';
            $validAlamat=FALSE;
        }else{
            $validAlamat=TRUE;
        }

        // cek email
        $email=test_input($_POST['email']);
        if ($email=='') {
            $errorEmail='wajib diisi';
            $validEmail=FALSE;
        }else{
            $validEmail=TRUE;
        }

        // cek nomor HP
        $noHp=test_input($_POST['telpon']);
        if ($noHp=='') {
            $errorHp='wajib diisi';
            $validHp=FALSE;
        }elseif (!preg_match("/^[0-9]*$/",$noHp)) {
            $errorHp='hanya mengizinkan angka 0-9';
            $validHp=FALSE;
        }else{
            $validHp=TRUE;
        }

        
        // jika tidak ada kesalahan input
        if ($validNik && $validNama && $validPass && $validAlamat  && $validEmail && $validHp) {
            $nik=$con->real_escape_string($nik);
            $nama=$con->real_escape_string($nama);
            $password=$con->real_escape_string($password);
            $alamat=$con->real_escape_string($alamat);
            $email=$con->real_escape_string($email);
            $noHp=$con->real_escape_string($noHp);
              
            $query = "INSERT INTO masyarakat (nik, nama, password, alamat, email, no_HP) VALUES ('$nik','$nama','$password','$alamat','$email','$noHp')";
        
            $hasil=$con->query($query);
            if (!$hasil) {
                die("Tidak dapat menjalankan query database: <br>".$con->error);
            $pesan_sukses="gagal";

            }else{
                // $sukses=TRUE;
                // $nim=$con->real_escape_string($nim);
                // $nama=$con->real_escape_string($nama);
                // $password=$con->real_escape_string($password);
                // $alamat=$con->real_escape_string($alamat);
                // $email=$con->real_escape_string($email);
                // $noHp=$con->real_escape_string($noHp);
            $pesan_sukses="Berhasil menambahkan data.";

            }

        }
        else{
            $sukses=FALSE;
        }
    }
        
?>


<div class="row" >
    <div class="">
        <!-- Form Elements -->
        <div class="panel panel-default" >
            <div class="panel-heading" style="height: 40px;padding-top: 8px; background-color: #1c7fef " align="center" >
                <span class="label label-success" style="font-size: 16px;margin-top: 30px;"><?php if(isset($pesan_sukses)) echo $pesan_sukses; ?></span>
            </div>
            <div class="panel-body" style="border: none;">
                <div class="row">
                   
                        <form method="POST" role="form" autocomplete="on" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
                          <div class="col-md-6">
            
                            <div class="form-group">
                                <label>NIK</label>&nbsp;<span class="label label-warning" style="background: #1c7fef">* <?php if(isset($errorNik)) echo $errorNik;?></span>
                                <input class="form-control" type="text" name="nik" maxlength="16" size="30" placeholder="nik 16 digit angka" required autofocus value="<?php if(!$sukses&&$validNik){echo $nik;} ?>">
                            </div>
                            <div class="form-group">
                                <label>Nama</label>&nbsp;<span class="label label-warning" style="background: #1c7fef">* <?php if(isset($errorNama)) echo $errorNama;?></span>
                                <input class="form-control" type="text" name="nama" maxlength="30" size="30" placeholder="masukan nama" required value="<?php if(!$sukses&&$validNama){echo $nama;} ?>">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>&nbsp;<span class="label label-warning" style="background: #1c7fef">* <?php if(isset($errorAlamat)) echo $errorAlamat;?></span>
                                <textarea class="form-control" name="alamat" placeholder="masukan alamat rumah" cols="26" rows="5" required maxlength="75"><?php if(!$sukses&&$validAlamat){echo $alamat;} ?></textarea>
                            </div>
                            </div>
                            <div class="col-md-6">
                          
                            <div class="form-group">
                                <label>No HP</label>&nbsp;<span class="label label-warning" style="background: #1c7fef">* <?php if(isset($errorHp)) echo $errorHp;?></span>
                                <input class="form-control" type="text" name="telpon" minlength="8" maxlength="14" size="30" placeholder="nomor HP yang aktif" required value="<?php if(!$sukses&&$validHp){echo $noHp;} ?>">
                            </div>
                        
                            <div class="form-group">
                                <label>Email</label>&nbsp;<span class="label label-warning" style="background: #1c7fef">* <?php if(isset($errorEmail)) echo $errorEmail;?></span>
                                <input class="form-control" type="email" name="email" maxlength="30" size="30" placeholder="example@email.com" required value="<?php if(!$sukses&&$validEmail){echo $email;} ?>">
                            </div>
                            <div class="form-group">
                                <label>Password</label>&nbsp;<span class="label label-warning" style="background: #1c7fef">* <?php if(isset($errorPass)) echo $errorPass;?></span>
                                <input class="form-control" type="password" name="password" minlength="8"  maxlength="12" size="30" placeholder="minimal 8 digit" required value="<?php if(!$sukses&&$validPass){echo $_POST['password'];} ?>">
                            </div>
                        </div>

                    <div class="row ">
                        <div class="col-md-5"  ></div>
                     <div class="form-group col-md-2"  >
                         <div>
                                <input class="form-control" type="submit" name="daftar" value="Daftar" style="background: #1c7fef; color: #fff">
                        </div>
                     </div>
                      </form>
                 </div>
                    
                </div>
            </div>
        </div>
    </div>

<?php

$con->close();
?>
