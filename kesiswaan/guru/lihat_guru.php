<?php
$index = "false";
include("../header.php");
include("../../koneksi.php");
?>

<?php
// ==================================================================
// PROSES SUBMIT
// ==================================================================
if (isset($_POST["submit"])) {
  $nip = test_input($_POST['nip']);
  $nama = test_input($_POST['name']);
  $jabatan = test_input($_POST['jabatan']);

  $panjang_nip = 18;
  if ($nip == "") {
    // $error_nip = "nip harus diisi";
  }elseif (strlen($nip)!=$panjang_nip){
    // $error_nip='196601111998021002 (18 digit)';
    echo "<script>alert('Format NIP Salah')</script>";
    $valid_nip = FALSE;
  }else {
    $valid_nip = TRUE;
  }

  if ($nama == "") {
    // $error_nama = "nama harus diisi";
    $valid_nama = FALSE;
  }elseif (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
      echo "<script>alert('Format Nama hanya mengizinkan huruf dan spasi')</script>";
      $valid_nama=FALSE;
  }else {
    $valid_nama = TRUE;
  }

  if ($jabatan == "") {
    // $error_jabatan = "jabatan harus diisi";
    $valid_jabatan = FALSE;
  }elseif (!preg_match("/^[a-zA-Z ]*$/",$jabatan)) {
      echo "<script>alert('Format Jabatan hanya mengizinkan huruf dan spasi')</script>";
      $valid_jabatan=FALSE;
  }else {
    $valid_jabatan = TRUE;
  }

  if ($valid_nama && $valid_jabatan && $valid_nip) {
    $nip = $conn->real_escape_string($nip);
    $nama = $conn->real_escape_string($nama);
    $jabatan = $conn->real_escape_string($jabatan);

    $query = "INSERT INTO guru (nip_guru, nama_guru, jabatan_guru) VALUES ('".$nip."', '".$nama."', '".$jabatan."')";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data Guru Berhasil Ditambah')</script>";
    }else {
      echo "<script>alert('NIP Sudah digunakan')</script>";
    }
  }
}

// ==================================================================
// PROSES EDIT
// ==================================================================
if (isset($_POST["ubah-submit"])) {
  $nip = test_input($_POST['ubah-nip']);
  $nama = test_input($_POST['ubah-nama']);
  $jabatan = test_input($_POST['ubah-jabatan']);

  if ($nama == "") {
    $error_nama = "nama harus diisi";
    $valid_nama = FALSE;
  }elseif (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
      echo "<script>alert('Format Nama hanya mengizinkan huruf dan spasi')</script>";
      $valid_nama=FALSE;
  }else {
    $valid_nama = TRUE;
  }

  if ($jabatan == "") {
    $error_jabatan = "jabatan harus diisi";
    $valid_jabatan = FALSE;
  }elseif (!preg_match("/^[a-zA-Z ]*$/",$jabatan)) {
      echo "<script>alert('Format Jabatan hanya mengizinkan huruf dan spasi')</script>";
      $valid_jabatan=FALSE;
  }else {
    $valid_jabatan = TRUE;
  }

  if ($valid_nama && $valid_jabatan) {
    $nip = $conn->real_escape_string($nip);
    $nama = $conn->real_escape_string($nama);
    $jabatan = $conn->real_escape_string($jabatan);

  $query = "UPDATE guru SET jabatan_guru = '$jabatan', nama_guru = '$nama' WHERE nip_guru = '$nip'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data Guru Berhasil Diubah')</script>";
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
  }
}

// ==================================================================
// PROSES HAPUS
// ==================================================================
if (isset($_POST["hapus-submit"])) {
  $nip = test_input($_POST['hapus-nip']);

  $query = "DELETE FROM guru WHERE nip_guru = '$nip'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data Guru Berhasil Dihapus')</script>";
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
}

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar Guru</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
      <div class="panel panel-default" style="background-color: #2a3f54">
        <div class="panel-body">
          <p class="text-muted font-20 m-b-50" style="color: #f5f5f5 ">
            Menambahkan Data Guru Baru Menggunakan Nomer Induk/ Identitas Pegawai (NIP)
          </p>
          <div class="col-md-10">
            <button data-toggle="modal" data-target="#modalTambah" type="button" class="btn btn-success" style="background-color: #51a6b5" id="btn-tbh-guru"
            data-toggle="tooltip" title="Menambahkan Guru"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Tambah
            </button>
          </div>
        </div>
      </div>
      

        <?php
          require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
          $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
          if ($db->connect_errno) {
            die("Could not connet to database:<br/> ".$db->connect_error);
          }
          $query = "SELECT * FROM guru";
          $result = $db->query($query);
          $i = 1;
        ?>
        <table id="datatable" class="table table-striped table-bordered">
        
         <!--  <div class="col-md-2 col-xs-12"></div>
          <form> -->
          <thead >
            <tr >
              <th style=" text-align: center;" width="5%">No</th>
              <th style=" text-align: center;" width="25%">NIP Guru</th>
              <th style=" text-align: center;" width="25%">Nama Guru</th>
              <th style=" text-align: center;" width="20%">Jabatan Guru</th>
              <th style=" text-align: center;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while ($row = $result->fetch_object())
            {
              echo "<tr>";
              echo "<td style=' text-align: center;'>".$i."</td>";
              echo "<td style=' text-align: center;''>".$row->nip_guru."</td>";
              echo "<td>".$row->nama_guru."</td>";
              echo "<td>".$row->jabatan_guru."</td>";
              echo "<td style='text-align: center;' colspan='2'>";

              echo "<button class='btn btn-warning btn-ubah-guru'";
              echo "data-nip='".$row->nip_guru."'";
              echo "data-nama='".$row->nama_guru."'";
              echo "data-jabatan='".$row->jabatan_guru."'>";
              echo "<i class='fa fa-edit'></i> Ubah </button>";

              
              echo "<button class='btn btn-danger btn-hapus-guru'";
              echo "data-nip='".$row->nip_guru."'>";
              echo "<i class='fa fa-remove'></i> Hapus</button>";
              
              echo "</td>";
              echo "</tr>";
              $i++;
            }
            ?>
          </tbody>
        
      <!-- </form> -->
      </table>
      <!-- </div> -->
    <!-- </div> -->
  <!-- </div> -->
<!-- </div> -->

<!-- </div> -->

<!-- ======================================================================= -->
<!-- MODAL GURU -->
<!-- ======================================================================= -->
<div class="modal fade" id="modalTambah" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Tambah Guru Baru</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">NIP :</label>
              <div class="col-sm-8">
                  <input type="text" name="nip" class="form-control" placeholder="196601111998021002 (18 digit)" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama :</label>
              <div class="col-sm-8">
                  <input type="text" name="name" class="form-control" placeholder="Masukkan Nama Guru" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Jabatan :</label>
              <div class="col-sm-8">
                  <input type="jabatan" name="jabatan" class="form-control" placeholder="Masukkan Jabatan Guru" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
      </div>
        <div class="modal-footer">
          <!-- <div class="col-sm-12"> -->
            <!-- <div class="form-group"> -->
              <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <button type="submit" class="btn btn-primary" name="submit" >Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- MODAL UBAH guru -->
<!-- ======================================================================= -->
<div class="modal fade" id="modal-ubah-guru" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Ubah Guru</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="form-ubah-guru">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nip :</label>
              <div class="col-sm-8">
                  <input id="ubah-nip" type="text" name="ubah-nip" class="form-control" placeholder=" 196601111998021002 (18 digit)" readonly style="cursor: not-allowed;">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama :</label>
              <div class="col-sm-8">
                  <input id="ubah-nama" type="text" name="ubah-nama" class="form-control" placeholder="Masukkan Nama" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Jabatan :</label>
              <div class="col-sm-8">
                  <input id="ubah-jabatan" type="jabatan" name="ubah-jabatan" class="form-control" placeholder="Masukkan jabatan" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')">
              </div>
            </div>
          </div>
      </div>
        <div class="modal-footer">
          <!-- <div class="col-sm-12"> -->
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
            <!-- <div class="form-group"> -->
              <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- ================================================================ -->
<!-- MODAL HAPUS guru -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-hapus-guru" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Hapus Guru</h4>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" style="text-align:center;">Apa anda yakin ingin menghapus guru ini?</h4>
        <!-- <p>Apakah Anda yakin menghapus guru ini?</p> -->
      </div>
      <div class="modal-footer">
        <form role="form" method="post" class="form-hapus-guru" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
        <!-- <form role="form" method="post" class="form-hapus-guru"> -->
          <input type="hidden" name="hapus-nip" id="hapus-nip">
          <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
            <button type="submit" class="btn btn-primary" name="hapus-submit">Hapus</button>
          <!-- <button type="submit" class="btn btn-success" name="hapus-submit">OK</button> -->
          <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- <html lang="en">
  <head>
    Datatables
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
  </head>
</html> -->

<script>
$(document).ready(function() {
  // ======================================================
  // INISIALISASI MODAL UBAH GURU
  // ======================================================
    $(".btn-ubah-guru").on("click", function(){
        var nip = $(this).data("nip");
        var nama = $(this).data("nama");
        var jabatan = $(this).data("jabatan");

    $("#ubah-nip").val(nip);
        $("#ubah-nama").val(nama);
        $("#ubah-jabatan").val(jabatan);
        $("#modal-ubah-guru").modal();
    });

  // ======================================================
  // INISIALISASI MODAL HAPUS GURU
  // ======================================================
    $(".btn-hapus-guru").on("click", function(){
        var nip = $(this).data("nip");
    $("#hapus-nip").val(nip);

        $("#modal-hapus-guru").modal();
    });
});
</script>


<?php
include("../footer.php");

?>
