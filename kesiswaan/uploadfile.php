<?php
	// echo "FILES =".json_encode($_FILES)."<br><br>";
	// echo "POST =".json_encode($_POST)."<br>";
	require_once("functions.php");
	$nis=test_input($conn,$_POST['nis']);
	if($level==1 && $nis!=$username){
		echo "Anda tidak diperbolehkan mengubah foto siswa lain.";
		exit;
	}
	// Upload File
		if ($_FILES['foto_masuk']['error'] > 0){
			switch ($_FILES['foto_masuk']['error'])
			{
				case 1: echo 'File exceeded upload_max_filesize<br/>';
						break;
				case 2: echo 'File exceeded max_file_size<br/>';
						break;
				case 3: echo 'File only partially uploaded<br/>';
						break;
				case 4: echo 'Tidak ada file yang diupload<br/>';
						break;
				case 6: echo 'Cannot upload file: No temp directory specified<br/>';
						break;
				case 7: echo 'Upload failed: Cannot write to disk<br/>';
						break;
			}
		}
		$status_upload='';
		if($_POST['jenis_foto']=='masuk'){
			$target_dir = $folder_foto_masuk;
		}elseif($_POST['jenis_foto']=='lulus'){
			$target_dir = $folder_foto_lulus;
		}
		
		$src_file=$target_dir.basename($_FILES['foto_masuk']['name']);
		$nama_file=$_POST['nis'].".".pathinfo($src_file,PATHINFO_EXTENSION);
		$target_file = $target_dir.$nama_file;
		$upload_ok = 1;
		$file_type = pathinfo($target_file,PATHINFO_EXTENSION);
		
		if(!isset($_POST['nis']) || $_POST['nis']==''){
			$upload_ok=0;
			$status_upload.= "NIS tidak boleh kosong<br/>";
		}else{
			$nis=mysqli_real_escape_string($conn, $_POST['nis']);
			$query=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			if(mysqli_num_rows($query)==0){
				$status_upload.="NIS tidak ditemukan.<br/>";
				$upload_ok=0;
			}
		}
		
		
		// Check if file already exists
		/*
		if (file_exists($target_file)) {
			echo "Sorry, file already exists.<br />";
			$upload_ok = 0;
		}
		*/
		// Check file size if you not use hidden input 'MAX_FILE_SIZE'
		
		// Allow certain file formats
		$allowed_type = array("jpg", "png", "jpeg","JPG","PNG","JPEG");
		if($_FILES['foto_masuk']['name']!=""){
			if(!in_array($file_type, $allowed_type)) {
				$status_upload.= "File yang diizinkan hanya gambar JPG, JPEG, & PNG.<br/>";
				$upload_ok = 0;
			}
		}
		if ($_FILES['foto_masuk']['size'] > 1000000) {
			$status_upload.= "Ukuran foto terlalu besar. Maksimal 1Mb<br />";
			$upload_ok = 0;
		}
		// Does the file have the right MIME type?
		/*if ($_FILES['foto_masuk']['type'] != 'text/plain'){
			echo 'Problem: file is not plain text';
			$uploadOk = 0;
		}*/
		// put the file where we'd like it
		if ($upload_ok != 0){
			if (file_exists($folder_foto_masuk.''.$nis.'.jpg')) {
				unlink($folder_foto_masuk.''.$nis.'.jpg');
			}elseif (file_exists($folder_foto_masuk.''.$nis.'.jpeg')) {
				unlink($folder_foto_masuk.''.$nis.'.jpeg');
			}elseif (file_exists($folder_foto_masuk.''.$nis.'.png')) {
				unlink($folder_foto_masuk.''.$nis.'.png');
			}
			if (is_uploaded_file($_FILES['foto_masuk']['tmp_name'])){
				if (!move_uploaded_file($_FILES['foto_masuk']['tmp_name'], $target_file)){
					echo 'Gambar gagal terupload';
				}else{
					echo '<div class="col-md-3">';
					echo '<img class="img-responsive" src="'.$target_file.'?'.time().'" />';
					echo '</div>';
					echo '<div class="col-md-9">';
					echo "Gambar berhasil diupload.<br/>";
					echo "NIS : ".$_POST['nis']."<br/>";
					echo "Jenis foto : Foto ".$_POST['jenis_foto']."<br/>";
					echo "Nama File : ".$nama_file;
					echo '</div>';
				}
			}else{
				echo 'Gambar gagal terupload';
			}
		}else{
			echo $status_upload;
		}
		
?>