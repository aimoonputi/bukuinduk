<?php
session_start();
include("../koneksi.php");

if (isset($_POST['submit'])) {
    $jenis_foto = $_POST['jenis_foto'];
    $nis = $_POST['nis'];

    $file_name = $_FILES['foto']['name'];
    $file_size = $_FILES['foto']['size'];
    $file_type = pathinfo($file_name, PATHINFO_EXTENSION);

    if ($jenis_foto == 'masuk') {
        $target_dir = 'foto_masuk';
    } elseif ($jenis_foto == 'lulus') {
        $target_dir = 'foto_keluar';
    }

    $target_file = $target_dir . '/' . $file_name;
    $upload_ok = 1;

    // Cek tipe file
    $allowed_type = array('jpg', 'jpeg', 'png');
    if (!in_array($file_type, $allowed_type)) {
        $psn = "Hanya dapat mengunggah gambar bertipe jpg, jpeg, atau png";
        $upload_ok = 0;
    }

    // Cek apakah file sudah ada
    if (file_exists($target_file)) {
        $psn = "File sudah ada";
        $upload_ok = 0;
    }

    // Cek ukuran file
    if ($file_size > 2097152) {
        $psn = "File lebih dari 2 MB";
        $upload_ok = 0;
    }

    // Cek apakah upload ok
    if ($upload_ok) {
        if (move_uploaded_file($_FILES['foto']['tmp_name'], $target_file)) {
            $nma = $file_name;
            $tgl = date("Y-m-d");
            $ukr = $file_size;

            if ($jenis_foto == 'masuk') {
                $query = "UPDATE siswa SET foto_masuk = '$file_name' WHERE nis = '$nis'";
            } elseif ($jenis_foto == 'lulus') {
                $query = "UPDATE siswa SET foto_keluar = '$file_name' WHERE nis = '$nis'";
            }

            $result = mysqli_query($conn, $query);
            if ($result) {
                  $psn = 'Foto berhasil ditambah';
                }else {
                  die("Could not query the database: <br/>". $conn->error);
            }

        } else {
            $psn = "Terjadi kesalahan saat mengunggah file";
        }
    }
}

    $_SESSION['alert'] = $psn;
    header('location: tambah_foto_siswa.php');
?>
