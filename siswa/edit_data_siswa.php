<?php
  $index = "false";
  include("header.php");
  include("../koneksi.php");
?>
<?php
if (isset($_SESSION['username'])) {
	$nis = $_SESSION['username'];
}
if (!isset($_POST['simpan'])) {
	$query = "SELECT * FROM siswa WHERE nis=".$nis."";
	$result = $conn->query($query);
	while ($dt = $result->fetch_object()) {
		$query2 = "SELECT tahun_ajaran FROM di_kelas WHERE nis='".$dt->nis."'";
		$result2 = $conn->query($query2);
		$kls = $result2->fetch_object();
		// var_dump($kls);
		if ($kls == null) {
			$tahun_ajaran = "";
		}else{
			$tahun_ajaran = $kls->tahun_ajaran;
		}		
		$nis = $dt->nis;
		$nisn = $dt->nisn;
		$nama = $dt->nama;
		$tempat_lahir = $dt->tempat_lahir;
		$tanggal_lahir = $dt->tanggal_lahir;
		$agama = $dt->agama;
		$desa = $dt->desa;
		$kecamatan = $dt->kecamatan;
		$kabupaten = $dt->kabupaten;
		$jenis_kel = $dt->jenis_kel;
		$anak_ke = $dt->anak_ke;
		$dari_bersaudara = $dt->dari_bersaudara;
		$status_anak = $dt->status_anak;
		$telepon = $dt->telepon;
		$nokk = $dt->nokk;
		if ($nokk != "") {
			$query3 = "SELECT * FROM orangtua_siswa WHERE nokk='".$nokk."'";
			$result3 = $conn->query($query3);
			while($row = $result3->fetch_object()){
				$nama_ayah = $row->nama_ayah;
				$nama_ibu = $row->nama_ibu;
				$alamat_ortu = $row->alamat_ortu;
				$telepon_ortu = $row->telepon_ortu;
				$pekerjaan_ayah = $row->pekerjaan_ayah;
				$pekerjaan_ibu = $row->pekerjaan_ibu;
			}
		}		
		$nik_wali = $dt->nik_wali;		
		if ($nik_wali != "") {
			$query4 = "SELECT * FROM wali_siswa WHERE nik_wali='".$nik_wali."'";
			$result4 = $conn->query($query4);			
			while ($row2 = $result4->fetch_object()) {
				$nama_wali = $row2->nama_wali;
				$alamat_wali = $row2->alamat_wali;
				$telepon_wali = $row2->telepon_wali;
				$pekerjaan_wali = $row2->pekerjaan_wali;
			}			
		}
		$kelas_diterima = $dt->kelas_diterima;
		$tanggal_diterima = $dt->tanggal_diterima;
		$sekolah_asal = $dt->sekolah_asal;
		$alamat_sekolah_asal = $dt->alamat_sekolah_asal;		
	}
}else{	
 	$tahun_ajaran=test_input($_POST['tahun_ajaran']);	
	if($tahun_ajaran==''){
		$error_tahun_ajaran='Tahun Pelajaran harus diisi';
		$valid_tahun_ajaran=FALSE;
	}else{
		$valid_tahun_ajaran=TRUE;		
	}
	$panjang_nis = 4;	
	$nis=test_input($_POST['nis']);	
	if($nis==''){	
		$error_nis='NIS harus diisi';
		$valid_nis=FALSE;
	}elseif (strlen($nis)!=$panjang_nis) {
		$error_nis='Harap cek format NIS. (cth: 1234)';
		$valid_nis = FALSE;
	}else{
		$valid_nis=TRUE;
	}

	$nisn=test_input($_POST['nisn']);	
	if($nisn==''){
		$error_nisn='NISN harus diisi';
		$valid_nisn=FALSE;
	}else{
		$valid_nisn=TRUE;		
	}

	$nama=test_input($_POST['nama']);
		if($nama==''){
			$error_nama='Nama Lengkap harus diisi';
			$valid_nama=FALSE;
		}else{
			$valid_nama=TRUE;			
		}
		
	$tempat_lahir=test_input($_POST['tempat_lahir']);
			$valid_tempat_lahir=TRUE;			

	$tanggal_lahir=test_input($_POST['tanggal_lahir']);
		$valid_tanggal_lahir=TRUE;		

	if (isset($_POST['jenis_kel'])) {
		$jenis_kel=test_input($_POST['jenis_kel']);
		$valid_jenis_kel=TRUE;
	}else {
		$error_jenis_kel='Pilih Jenis Kelamin';
		$valid_jenis_kel=FALSE;
	}

	$anak_ke=test_input($_POST['anak_ke']);		
			// $valid_anak_ke=TRUE;		

	if($anak_ke!='' && !filter_var($anak_ke,FILTER_VALIDATE_INT)){
			$error_anak_ke='Harus Berupa Angka';
			$valid_anak_ke=FALSE;
	}else{		
			$valid_anak_ke=TRUE;
			$value_anak_ke= 'value="'.$anak_ke.'"';
	}	

	// $dari_bersaudara=test_input($_POST['dari_bersaudara']);		
	// 		$valid_dari_bersaudara=TRUE;
	$dari_bersaudara=test_input($_POST['dari_bersaudara']);
	if($dari_bersaudara!='' && !filter_var($dari_bersaudara,FILTER_VALIDATE_INT)){
			$error_dari_bersaudara='Harus Berupa Angka';
			$valid_dari_bersaudara=FALSE;
	}elseif($dari_bersaudara<$anak_ke){
			$error_dari_bersaudara='Jumlah Anak Tidak Boleh Lebih Kecil daripada Anak-ke';
			$valid_dari_bersaudara=FALSE;
	}else{				
			$valid_dari_bersaudara=TRUE;
			$value_dari_bersaudara=' value="'.$dari_bersaudara.'"';			
	}			

	$status_anak=test_input($_POST['status_anak']);
			$valid_status_anak = TRUE; 			

	$agama=test_input($_POST['agama']);
			$valid_agama = TRUE;			

	$desa=test_input($_POST['desa']);
			$valid_desa = TRUE;			

	$kecamatan=test_input($_POST['kecamatan']);
			$valid_kecamatan = TRUE;			

	$kabupaten=test_input($_POST['kabupaten']);
			$valid_kabupaten = TRUE;

	$telepon=test_input($_POST['telepon']);
			$valid_telepon = TRUE;				

	$kelas_diterima=test_input($_POST['kelas_diterima']);
	if($kelas_diterima==''){
			$error_kelas_diterima='Kelas Diterima harus diisi';
			$valid_kelas_diterima=FALSE;
		}else{
			$valid_kelas_diterima= TRUE; 			
		}

	$tanggal_diterima=test_input($_POST['tanggal_diterima']);
	if($tanggal_diterima==''){
			$error_tanggal_diterima='Tanggal Diterima harus diisi';
			$valid_tanggal_diterima=FALSE;
		}else{
			$valid_tanggal_diterima = TRUE; 			
		}

	$sekolah_asal=test_input($_POST['sekolah_asal']);
			$valid_sekolah_asal = TRUE; 			

	$alamat_sekolah_asal=test_input($_POST['alamat_sekolah_asal']);
			$valid_alamat_sekolah_asal = TRUE;			
	
	$nokk=test_input($_POST['nokk']);
			$valid_nokk=TRUE;			

	$nama_ayah=test_input($_POST['nama_ayah']);
			$valid_nama_ayah=TRUE;			

	$nama_ibu=test_input($_POST['nama_ibu']);
			$valid_nama_ibu=TRUE;			
		
	$alamat_ortu=test_input($_POST['alamat_ortu']);
			$valid_alamat_ortu=TRUE;

	$telepon_ortu=test_input($_POST['telepon_ortu']);
			$valid_telepon_ortu=TRUE;			

	$pekerjaan_ayah=test_input($_POST['pekerjaan_ayah']);
			$valid_pekerjaan_ayah=TRUE;			

	$pekerjaan_ibu=test_input($_POST['pekerjaan_ibu']);
			$valid_pekerjaan_ibu=TRUE;			

	$nik_wali=test_input($_POST['nik_wali']);
			$valid_nik_wali=TRUE;

	$nama_wali=test_input($_POST['nama_wali']);
			$valid_nama_wali=TRUE;			

	$alamat_wali=test_input($_POST['alamat_wali']);
			$valid_alamat_wali=TRUE;			

	$telepon_wali=test_input($_POST['telepon_wali']);
			$valid_telepon_wali=TRUE;			

	$pekerjaan_wali=test_input($_POST['pekerjaan_wali']);
			$valid_pekerjaan_wali=TRUE;			

		    
	$result=false;
	if($valid_tahun_ajaran && $valid_nis && $valid_nisn && $valid_nama && $valid_tempat_lahir && $valid_tanggal_lahir && $valid_agama && $valid_desa && $valid_kecamatan && $valid_kabupaten && $valid_jenis_kel && $valid_anak_ke && $valid_dari_bersaudara && $valid_status_anak && $valid_telepon &&	$valid_kelas_diterima && $valid_tanggal_diterima && $valid_sekolah_asal && $valid_alamat_sekolah_asal) {	

		$tahun_ajaran = $conn->real_escape_string($tahun_ajaran);
		$nis = $conn->real_escape_string($nis);
		$nisn = $conn->real_escape_string($nisn);
		$nama = $conn->real_escape_string($nama);
		$tempat_lahir = $conn->real_escape_string($tempat_lahir);
		$tanggal_lahir = $conn->real_escape_string($tanggal_lahir);
		$agama = $conn->real_escape_string($agama);
		$desa = $conn->real_escape_string($desa);
		$kecamatan = $conn->real_escape_string($kecamatan);
		$kabupaten = $conn->real_escape_string($kabupaten);
		$jenis_kel = $conn->real_escape_string($jenis_kel);
		$anak_ke = $conn->real_escape_string($anak_ke);
		$dari_bersaudara = $conn->real_escape_string($dari_bersaudara);
		$status_anak = $conn->real_escape_string($status_anak);
		$telepon = $conn->real_escape_string($telepon);
		$nokk = $conn->real_escape_string($nokk);
		$nik_wali = $conn->real_escape_string($nik_wali);
		$kelas_diterima = $conn->real_escape_string($kelas_diterima);
		$tanggal_diterima = $conn->real_escape_string($tanggal_diterima);
		$sekolah_asal = $conn->real_escape_string($sekolah_asal);
		$alamat_sekolah_asal = $conn->real_escape_string($alamat_sekolah_asal);	

		$query = "UPDATE siswa SET nama='".$nama."', tempat_lahir='".$tempat_lahir."', agama='".$agama."', desa='".$desa."', kecamatan='".$kecamatan."', kabupaten='".$kabupaten."', jenis_kel='".$jenis_kel."', anak_ke='".$anak_ke."', dari_bersaudara='".$dari_bersaudara."',status_anak='".$status_anak."',telepon='".$telepon."',nokk='".$nokk."', nik_wali='".$nik_wali."', kelas_diterima='".$kelas_diterima."', tanggal_lahir='".$tanggal_lahir."',tanggal_diterima='".$tanggal_diterima."', sekolah_asal='".$sekolah_asal."', alamat_sekolah_asal='".$alamat_sekolah_asal."' WHERE nis = '".$nis."'";

        $result = $conn->query($query);

        // if ($valid_kelas_diterima && $valid_nis) {
	   		// $nis = $conn->real_escape_string($nis);
	   		// $kelas_diterima = $conn->real_escape_string($kelas_diterima);

	   		$query1 = "UPDATE di_kelas SET kd_kelas='".$kelas_diterima."' WHERE nis = '".$nis."'";
	   		$result1 = $conn->query($query1);
	   	// }
   		if($valid_nokk && $valid_nama_ayah) {	
			// echo "sampai sini";die;
			$nokk = $conn->real_escape_string($nokk);
			$nama_ayah = $conn->real_escape_string($nama_ayah);
			$nama_ibu = $conn->real_escape_string($nama_ibu);
			$alamat_ortu = $conn->real_escape_string($alamat_ortu);
			$telepon_ortu = $conn->real_escape_string($telepon_ortu);
			$pekerjaan_ayah = $conn->real_escape_string($pekerjaan_ayah);
			$pekerjaan_ibu = $conn->real_escape_string($pekerjaan_ibu);				
				$query_cek_nokk = "SELECT * FROM orangtua_siswa WHERE nokk ='".$nokk."'";
				$result_nokk = $conn->query($query_cek_nokk);
				$count = mysqli_num_rows($result_nokk);			
				if ($count > 0) {
					$query2 ="UPDATE orangtua_siswa SET nama_ayah='".$nama_ayah."', nama_ibu='".$nama_ibu."', alamat_ortu='".$alamat_ortu."', telepon_ortu='".$telepon_ortu."', pekerjaan_ayah='".$pekerjaan_ayah."', pekerjaan_ibu='".$pekerjaan_ibu."' WHERE nokk='".$nokk."'";
				}else{
					$query2="INSERT INTO orangtua_siswa (nokk, nama_ayah, nama_ibu, alamat_ortu, telepon_ortu, pekerjaan_ayah, pekerjaan_ibu ) VALUES ('".$nokk."', '".$nama_ayah."', '".$nama_ibu."', '".$alamat_ortu."', '".$telepon_ortu."', '".$pekerjaan_ayah."', '".$pekerjaan_ibu."')";	
				}		
			
	        $result = $conn->query($query2);        
	    }

		if($valid_nik_wali) {	
			$nik_wali = $conn->real_escape_string($nik_wali);
			$nama_wali = $conn->real_escape_string($nama_wali);
			$alamat_wali = $conn->real_escape_string($alamat_wali);
			$telepon_wali = $conn->real_escape_string($telepon_wali);
			$pekerjaan_wali = $conn->real_escape_string($pekerjaan_wali);		
			$query_cek_wali = "SELECT * FROM wali_siswa WHERE nik_wali ='".$nik_wali."'";
				$result_wali = $conn->query($query_cek_wali);
				$count = mysqli_num_rows($result_wali);
				if ($count > 0) {
					$query3="UPDATE wali_siswa SET nik_wali='".$nik_wali."', nama_wali='".$nama_wali."', alamat_wali='".$alamat_wali."', telepon_wali='".$telepon_wali."', pekerjaan_wali='".$pekerjaan_wali."' WHERE nik_wali='".$nik_wali."'";
				}else{
					$query3 = "INSERT INTO wali_siswa (nik_wali, nama_wali, alamat_wali, telepon_wali, pekerjaan_wali) VALUES ('$nik_wali', '$nama_wali', '$alamat_wali', '$telepon_wali', '$pekerjaan_wali')";
				}	   
			$result = $conn->query($query3);
		}


   	}

    if ($result) {
      echo "<script>alert('Data Siswa Berhasil Diubah')</script>";
      echo "<script>window.location.replace('".base_url('siswa/lihat_data_diri.php')."')</script>";      
    }
}
 
 

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<!-- <script src="<?php echo base_url("assets/template/vendors/jquery/dist/jquery.min.js")?>"></script> -->

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
		  <div class="x_title">
	        <h2>Edit Data Siswa</h2>
	         <ul class="pull-right nav navbar-right panel_toolbox">
	        	<li><a class="collapse-link"></a></li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
		  <div id="wizard" class="form_wizard wizard_horizontal">
              <ul class="wizard_steps">
                <li>
                  <a href="#step-1">
                    <span class="step_no">1</span>
                    <span class="step_descr"> Step 1 <br/> <small> Data Pribadi </small></span>
                  </a>
                </li>
                <li>
                  <a href="#step-2">
                    <span class="step_no">2</span>
                    <span class="step_descr"> Step 2 <br/> <small> Data Orang Tua </small></span>
                  </a>
                </li>
                <li>
                  <a href="#step-3">
                    <span class="step_no">3</span>
                    <span class="step_descr"> Step 3 <br/> <small> Data Wali</small></span>
                  </a>
                </li>
              </ul>
            <form class="form-horizontal form-label-left" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
              	
              	<div id="step-1">    
                     	<div class="form-group">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tahun_ajaran_masuk">Tahun Pelajaran (Masuk) 
	                        <span class="required">*</span></label>
	                        <div class="col-md-7 col-sm-7 col-xs-12">
	                        	<input type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control col-md-7 col-xs-12"  placeholder="____/____" value="<?php if(isset($tahun_ajaran)){echo $tahun_ajaran;} ?>" readonly style="cursor: not-allowed;"/>
	                        	<?php
		                        if(isset($error_tahun_ajaran)){
										echo '<span id="helpBlock" class="text-danger">'.$error_tahun_ajaran.'</span>';
									}
		                        ?>
	                        </div>
	                    </div>

	                    <div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >NIS *</label>
								<div class="col-sm-3">
									<input class="form-control" type="text" id="nis" name="nis" maxlength="<?php echo $jml_kar_nis?>" placeholder="Contoh : 6366" value="<?php if(isset($nis)){echo $nis;} ?>" readonly style="cursor: not-allowed;"/>
									<?php
		                        if(isset($error_nis)){
										echo '<span id="helpBlock" class="text-danger">'.$error_nis.'</span>';
									}
		                        ?>
								</div>
							<label class="control-label col-md-1 col-sm-2 col-xs-12" >NISN *</label>
								<div class="col-sm-3">
									<input class="form-control" type="text" name="nisn" maxlength="<?php echo $jml_kar_nisn?>" 
									placeholder="Contoh : 0047363119" value="<?php if(isset($nisn)){echo $nisn;} ?>" readonly style="cursor: not-allowed;"/>
								<?php
		                        if(isset($error_nisn)){
										echo '<span id="helpBlock" class="text-danger">'.$error_nisn.'</span>';
									}
		                        ?>
								</div>
						</div>

						<div class="form-group ">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama">Nama Lengkap
	                        	<span class="required">*</span></label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" maxlength="<?php echo $jml_kar_nama?>" value="<?php if(isset($nama)){echo $nama;} ?>"/>
		                          <?php
			                        if(isset($error_nama)){
											echo '<span id="helpBlock" class="text-danger">'.$error_nama.'</span>';
										}
			                       ?>
	                       		</div>
	                    </div>

						<div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Tempat, Tanggal Lahir</label>
								<div class="col-sm-4">
									<input class="form-control" type="text" name="tempat_lahir" maxlength="<?php echo $jml_kar_kabupaten ?>" placeholder="Tempat Lahir" value="<?php if(isset($tempat_lahir)){echo $tempat_lahir;} ?>"/>
									<?php
			                        if(isset($error_tempat_lahir)){
											echo '<span id="helpBlock" class="text-danger">'.$error_tempat_lahir.'</span>';
										}
			                        ?>
								</div>
								<div class="col-sm-3">
									<input class="form-control" type="date" name="tanggal_lahir" value="<?php if(isset($tanggal_lahir)){echo $tanggal_lahir;} ?>"/>
									<?php
			                        if(isset($error_tanggal_lahir)){
											echo '<span id="helpBlock" class="text-danger">'.$error_tanggal_lahir.'</span>';
										}
			                        ?>
								</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Jenis Kelamin <span class="required">*</span></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<div class="col-md-12" style="padding: 0">
									<input type="radio" id="jenis_kel" name="jenis_kel" value="L" <?php 
									if (isset($jenis_kel) && $jenis_kel == "L") {
									 	echo "checked";
									 } ?>/> Laki-Laki 
									<input type="radio" id="jenis_kel" name="jenis_kel" value="P" <?php 
									if (isset($jenis_kel) && $jenis_kel == "P") {
									 	echo "checked";
									 } ?>/> Perempuan									
								</div>																
								<?php
									if(isset($error_jenis_kel)){
										echo '<span id="helpBlock" class="text-danger">'.$error_jenis_kel.'</span>';
									}
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-2 col-xs-12" >Anak-ke </label>
								<div class="col-sm-2">
									<input class="form-control" type="text" name="anak_ke" maxlength="<?php echo $jml_kar_anak_ke?>" placeholder="2" value="<?php if(isset($anak_ke)){echo $anak_ke;} ?>"/>
									<?php
			                        if(isset($error_anak_ke)){
											echo '<span id="helpBlock" class="text-danger">'.$error_anak_ke.'</span>';
										}
			                        ?>
								</div>
							<label class="control-label col-md-1 col-sm-2 col-xs-12" >dari</label>
								<div class="col-sm-2">
									<input class="form-control" type="text" name="dari_bersaudara" maxlength="<?php echo $jml_kar_dari_bersaudara?>" 
									placeholder="3" value="<?php if(isset($dari_bersaudara)){echo $dari_bersaudara;} ?>" />	
									<?php
                        if(isset($error_dari_bersaudara)){
								echo '<div><span id="helpBlock" class="text-danger">'.$error_dari_bersaudara.'</span></div>';
							}
                        ?>
								</div>								
							<label class="control-label col-md-1 col-sm-2 col-xs-12" >bersaudara</label>
							
						</div>

						
			      
			       

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Status Anak</label>
							<div class="col-sm-7">
								<input class="form-control" list="status_anak" name="status_anak" maxlength="<?php echo $jml_kar_status_anak ?>" placeholder="Status Anak" value="<?php if (isset($status_anak)){echo $status_anak; } ?>"/>
								<?php
		                        if(isset($error_status_anak)){
										echo '<span id="helpBlock" class="text-danger">'.$error_status_anak.'</span>';
									}
		                        ?>
								<datalist id="status_anak">
									<option value="Anak Kandung">Anak Kandung</option>
									<option value="Anak Angkat">Anak Angkat</option>
									<option value="Yatim">Yatim</option>
									<option value="Piatu">Piatu</option>
									<option value="Yatim Piatu">Yatim Piatu</option>
								</datalist>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Agama</label>
							<div class="col-sm-7">
								<input class="form-control" list="agama" name="agama" maxlength="<?php echo $jml_kar_agama ?>" placeholder="Agama" value="<?php if (isset($agama)){echo $agama; } ?>"/>
								<?php
		                        if(isset($error_agama)){
										echo '<span id="helpBlock" class="text-danger">'.$error_agama.'</span>';
									}
		                        ?>
								<datalist id="agama">
									<option value="Islam">Islam</option>
									<option value="Kristen">Kristen</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Konghuchu">Konghuchu</option>
								</datalist>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat</label>
							<div class="col-sm-7">
								<input class="form-control" list="desa" name="desa" id="desa" maxlength="<?php echo $jml_kar_desa ?>" placeholder="Jl. ____ Desa ____ RT __/__" value="<?php if (isset($desa)){echo $desa; } ?>" />
								<?php
		                        if(isset($error_desa)){
										echo '<span id="helpBlock" class="text-danger">'.$error_desa.'</span>';
									}
		                        ?>
							</div>
							<div class="col-sm-4">
							</div>
							<div class="col-sm-4">
								<input class="form-control" list="kecamatan" name="kecamatan" id="kecamatan" placeholder="Kecamatan" maxlength="<?php echo $jml_kar_kecamatan ?>" value="<?php if (isset($kecamatan)){echo $kecamatan; } ?>"/>
								<?php
		                        if(isset($error_kecamatan)){
										echo '<span id="helpBlock" class="text-danger">'.$error_kecamatan.'</span>';
									}
		                        ?>
							</div>
							<div class="col-sm-3">
								<input class="form-control" list="kabupaten" name="kabupaten" id="kabupaten" placeholder="Kabupaten" maxlength="<?php echo $jml_kar_kabupaten ?>" value="<?php if (isset($kabupaten)){echo $kabupaten; } ?>"/>
								<?php
		                        if(isset($error_kabupaten)){
										echo '<span id="helpBlock" class="text-danger">'.$error_kabupaten.'</span>';
									}
		                        ?>
							</div>
						</div>

						<div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Telepon</label>
								<div class="col-sm-4">
									<input class="form-control" type="text" name="telepon" id="telepon" placeholder="Nomor Telepon" value="<?php if(isset($telepon)){echo $telepon;} ?>"/>
									<?php
		                        if(isset($error_telepon)){
										echo '<span id="helpBlock" class="text-danger">'.$error_telepon.'</span>';
									}
		                        ?>
								</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Tanggal Diterima *</label>
								<div class="col-sm-4">
									<input class="form-control" type="date" id="tanggal_diterima" name="tanggal_diterima" value="<?php if(isset($tanggal_diterima)){echo $tanggal_diterima;}?>" />
									<?php
			                        if(isset($error_tanggal_diterima)){
											echo '<span id="helpBlock" class="text-danger">'.$error_tanggal_diterima.'</span>';
										}
			                        ?>
								</div>
						</div>

						<div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Kelas Diterima *</label>
								<div class="col-sm-7">
									<select class="form-control" id="kelas_diterima" name="kelas_diterima" readonly style="cursor: not-allowed;" 	value="<?php if (isset($kelas_diterima)){echo $kelas_diterima; } ?>">
										<option value="none">-- Pilih Kelas --</option>
									</select>
								</div>
						</div>

						<div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Sekolah Asal</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="sekolah_asal" placeholder="Nama Sekolah Asal" maxlength="<?php echo $jml_kar_nama_sekolah_asal ?>" value="<?php if(isset($sekolah_asal)){echo $sekolah_asal;} ?>" />
									<?php
			                        if(isset($error_sekolah_asal)){
											echo '<span id="helpBlock" class="text-danger">'.$error_sekolah_asal.'</span>';
										}
			                        ?>
								</div>
						</div>

						<div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat Sekolah Asal</label>
								<div class="col-sm-7">
									<input class="form-control" type="text" name="alamat_sekolah_asal" placeholder="Jln__, Desa__, Kec__, Kab__" maxlength="<?php echo $jml_kar_alamat_sekolah_asal ?>" value="<?php if(isset($alamat_sekolah_asal)){echo $alamat_sekolah_asal;} ?>" />
									<?php
			                        if(isset($error_alamat_sekolah_asal)){
											echo '<span id="helpBlock" class="text-danger">'.$error_alamat_sekolah_asal.'</span>';
										}
			                        ?>
								</div>
						</div>
						<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
							<a class="btn btn-danger " href="edit_data_siswa.php">Batal</a>
						</div>
                </div>
                  


                <div id="step-2" class="form-horizontal form-label-left">
                     	<div class="form-group ">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >NoKK </label>
								<div class="col-sm-7 col-sm-7 col-xs-12">
									<input class="form-control" type="text" name="nokk" maxlength="<?php echo $jml_kar_nokk?>" placeholder="Contoh : 3275110106070203" value="<?php if(isset($nokk)){echo $nokk;} ?>"/>
									<?php
			                        if(isset($error_nokk)){
											echo '<span id="helpBlock" class="text-danger">'.$error_nokk.'</span>';
										}
			                        ?>
								</div>
						</div>

                     	<div class="form-group ">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama_ayah">Nama Ayah</label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" name="nama_ayah" class="form-control" maxlength="<?php echo $jml_kar_nama?>"  placeholder="Nama Ayah" value="<?php if(isset($nama_ayah)){echo $nama_ayah;} ?>"/>
		                          <?php
			                        if(isset($error_nama_ayah)){
											echo '<span id="helpBlock" class="text-danger">'.$error_nama_ayah.'</span>';
										}
			                        ?>
	                       		</div>
	                    </div>

	                    <div class="form-group ">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama_ibu">Nama Ibu</label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" name="nama_ibu" maxlength="<?php echo $jml_kar_nama?>" class="form-control" placeholder="Nama Ibu" value="<?php if(isset($nama_ibu)){echo $nama_ibu;} ?>"/>
		                          <?php
			                        if(isset($error_nama_ibu)){
											echo '<span id="helpBlock" class="text-danger">'.$error_nama_ibu.'</span>';
										}
			                        ?>
	                       		</div>
	                    </div>

	                    <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat Orang Tua</label>
								<div class="col-md-7 col-sm-7 col-xs-12">
									<input class="form-control" type="text" id="alamat_ortu" name="alamat_ortu" maxlength="<?php echo $jml_kar_alamat ?>" placeholder="Jl.__, Desa__, RT__/Rw__, Kec__ , Kab__" value="<?php if(isset($alamat_ortu)){echo $alamat_ortu;} ?>"/>
									<?php
			                        if(isset($error_alamat_ortu)){
											echo '<span id="helpBlock" class="text-danger">'.$error_alamat_ortu.'</span>';
										}
			                        ?>
			                        <a class="input-group-addon" id="samakan_alamat">Samakan Alamat dengan Alamat Anak</a>
								</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Telepon</label>
								<div class="col-sm-4">
									<input class="form-control" type="text" name="telepon_ortu" id="telepon_ortu" placeholder="Nomor Telepon" maxlength="<?php echo $jml_kar_telepon ?>" value="<?php if(isset($telepon_ortu)){echo $telepon_ortu;} ?>" />
									<?php
			                        if(isset($error_telepon_ortu)){
											echo '<span id="helpBlock" class="text-danger">'.$error_telepon_ortu.'</span>';
										}
			                        ?>
								</div>
						</div>

						<div class="form-group">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pekerjaan_ayah">Pekerjaan Ayah</label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" name="pekerjaan_ayah" class="form-control" maxlength="<?php echo $jml_kar_pekerjaan ?>" value="<?php if(isset($pekerjaan_ayah)){echo $pekerjaan_ayah;} ?>" />
		                          <?php
			                        if(isset($error_pekerjaan_ayah)){
											echo '<span id="helpBlock" class="text-danger">'.$error_pekerjaan_ayah.'</span>';
										}
			                        ?>
	                       		</div>
	                    </div>

	                    <div class="form-group">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pekerjaan_ibu">Pekerjaan Ibu</label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" name="pekerjaan_ibu"  class="form-control"
		                          maxlength="<?php echo $jml_kar_pekerjaan ?>" value="<?php if(isset($pekerjaan_ibu)){echo $pekerjaan_ibu;} ?>" />
		                          <?php
			                        if(isset($error_pekerjaan_ibu)){
											echo '<span id="helpBlock" class="text-danger">'.$error_pekerjaan_ibu.'</span>';
										}
			                        ?>
	                       		</div>
	                    </div>
	                    <<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
							<a class="btn btn-danger " href="edit_data_siswa.php">Batal</a>
						</div>
                  </div>


                <div id="step-3" class="form-horizontal form-label-left">
	                    <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >NIK</label>
								<div class="col-sm-7 col-sm-7 col-xs-12">
									<input class="form-control" type="text" id="nik_wali" name="nik_wali" maxlength="<?php echo $jml_kar_nik?>" placeholder="Contoh : 01679765443368363" value="<?php if(isset($nik_wali)){echo $nik_wali;} ?>"/>
									<?php
			                        if(isset($error_nik_wali)){
											echo '<span id="helpBlock" class="text-danger">'.$error_nik_wali.'</span>';
										}
			                        ?>
								</div>
						</div>

                     	<div class="form-group">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama_wali">Nama Wali</label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" id="nama_wali" name="nama_wali" class="form-control col-md-7 col-xs-12" placeholder="Nama Wali" value="<?php if(isset($nama_wali)){echo $nama_wali;} ?>"/>
		                          <?php
			                        if(isset($error_nama_wali)){
											echo '<span id="helpBlock" class="text-danger">'.$error_nama_wali.'</span>';
										}
			                        ?>
	                       		</div>
	                    </div>

	                    <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Alamat Wali </label>
								<div class="col-sm-7 col-sm-7 col-xs-12">
									<input class="form-control" type="text" id="alamat_wali" name="alamat_wali" maxlength="<?php echo $jml_kar_nik?>" placeholder="Jl.__, Desa__, RT__/Rw__, Kec__ , Kab__" value="<?php if(isset($alamat_wali)){echo $alamat_wali;} ?>"/>
									<?php
			                        if(isset($error_alamat_wali)){
											echo '<span id="helpBlock" class="text-danger">'.$error_alamat_wali.'</span>';
										}
			                        ?>
								</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" >Telepon Wali</label>
								<div class="col-sm-4">
									<input class="form-control" type="text" name="telepon_wali" id="telepon_wali" placeholder="telp" maxlength="<?php echo $jml_kar_telp ?>" value="<?php if(isset($telepon_wali)){echo $telepon_wali;} ?>"/>
									<?php
			                        if(isset($error_telepon_wali)){
										echo '<span id="helpBlock" class="text-danger">'.$error_telepon_wali.'</span>';
									}
			                        ?>
								</div>
						</div>

						<div class="form-group">
	                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pekerjaan_wali">Pekerjaan Wali
	                        	<!-- <span class="required">*</span> --></label>
		                        <div class="col-md-7 col-sm-7 col-xs-12">
		                          <input type="text" id="pekerjaan_wali" name="pekerjaan_wali" class="form-control col-md-7 col-xs-12" value="<?php if(isset($pekerjaan_wali)){echo $pekerjaan_wali;} ?>"/>
		                          <?php
			                        if(isset($error_pekerjaan_wali)){
											echo '<span id="helpBlock" class="text-danger">'.$error_pekerjaan_wali.'</span>';
										}
			                        ?>
	                       		</div>
	                    </div>
	                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
	                    <button type="submit" class="btn btn-primary" class="col-md-offset-5 col-md-2 " name="simpan">Simpan</button>
						<a class="btn btn-danger " href="edit_data_siswa.php">Batal</a>
						</div>
	                     
                </div>
            </form>
                <!-- End SmartWizard Content -->
		  </div>
	</div>
<?php
	include_once('footer.php');
?>

<script type="text/javascript">
	// alert("sdasd");
	if ($("#tahun_ajaran").val()!="") {
		var thn = $("#tahun_ajaran").val();		
		var nis = $("#nis").val();
		// alert(nis);
		$.ajax({
			url:"../kesiswaan/siswa/ajax_func/ajax_func.php?listkelas=1&thn="+thn+"&nis="+nis,
			type:"GET",
			dataType:"html",
			
			beforeSend: function(){
				$("#kelas_diterima").html('Loading...');
			},
			success: function(data){
				$("#kelas_diterima").html(data);
			},
			error: function(){
				$("#kelas_diterima").html("");
			}
		});
	}		
</script>
<script type="text/javascript">	
	$("#tahun_ajaran").blur(function(){		
		if($("#tahun_ajaran").val()==undefined){
			var thn='';
		}else{
			var thn = $("#tahun_ajaran").val();
		}		
		$.ajax({
			url:"ajax_func/ajax_func.php?listkelas=1&thn="+thn,
			type:"GET",
			dataType:"html",
			
			beforeSend: function(){
				$("#kelas_diterima").html('Loading...');
			},
			success: function(data){
				$("#kelas_diterima").html(data);
			},
			error: function(){
				$("#kelas_diterima").html("");
			}
		});
	});

	if($("#tahun_ajaran").val()!=null){		
		var thn = $("#tahun_ajaran").val();		
		var split = thn.split("/");
		
		if (split[0]!=null) {			
			var min = new Date();				
			min = split[0]+'-01-01';				
			document.getElementById("tanggal_diterima").setAttribute("min", min);
		}if(split[1]!=null){
			var max = new Date();
			max = split[1]+'-12-31';
			document.getElementById("tanggal_diterima").setAttribute("max", max);
		}
	}
	$("#tahun_ajaran").change(function(){
		if($("#tahun_ajaran").val()==undefined){
			var thn='';
		}else{
			var thn = $("#tahun_ajaran").val();
			var split = thn.split("/");
			if (split[0]!="") {
				var min = new Date();				
				min = split[0]+'-01-01';				
				document.getElementById("tanggal_diterima").setAttribute("min", min);
			}if(split[1]!=""){
				var max = new Date();
				max = split[1]+'-12-31';
				document.getElementById("tanggal_diterima").setAttribute("max", max);
			}
		}		
	})

	$('#samakan_alamat').click(function(){
			var desa= $("#desa").val();
			var kecamatan= $("#kecamatan").val();
			var kabupaten= $("#kabupaten").val();
			$.ajax({
				beforeSend: function(){
					$("#alamat_ortu").val('Loading...');
				},
				success: function(data){
					$("#alamat_ortu").val(desa+" "+kecamatan+" "+kabupaten);
				},
				error: function(){
					$("#alamat_ortu").val("");
				}
			});
		});
</script>