<?php
  include("base_url.php");
  session_start();
  if (isset($_SESSION["user"])) {
    if ($_SESSION["level"]=="Admin") {
      header("location:".base_url("admin")."");
    }elseif ($_SESSION["level"]=="Kesiswaan") {
      header("location:".base_url("kesiswaan")."");
    }elseif ($_SESSION["level"]=="Siswa") {
      header("location:".base_url("siswa")."");
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
<title>Buku Induk Siswa</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap CSS -->
<link href="<?php echo base_url("assets/css/bootstrap.min.css")?>" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.min.css")?>">
<!-- Portalku CSS -->
<link rel="stylesheet" href="<?php echo base_url("assets/css/main.css")?>">
<!-- jQuery -->
<script src="<?php echo base_url("assets/js/jquery-3.2.1.min.js")?>"></script>
<!-- Bootstrap JS -->
<script src="<?php echo base_url("assets/js/bootstrap.min.js")?>"></script>
<!-- Portalku JS -->
<script src="<?php echo base_url("assets/js/portalku.js")?>"></script>
</head>

<!-- Body Opening -->
<body>
<div class="bg-login">
<div class="container-fluid knt-login" id="formlogin">

  <!-- Judul -->
  <div class="row">
    <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
	  <h1 class="text-center" style="color:white">Login Buku Induk Siswa</h1>
    </div>
  </div>

  <!-- Form Login -->
  <div class="row">
    <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
      <div class="panel panel-default">
        <div class="panel-body bg-info">
          <form role="form" method="post" id="form-ajx-login"
          action="/BukuInduk/ajax/masuk.php">
            <!-- <div class="form-group">
              <select class="form-control" name="login-jenis">
                <option value="kosong">Level Pengguna</option>
                <option value="admin">Admin</option>
                <option value="kesiswaan">Kesiswaan</option>
                <option value="siswa">Siswa</option>
              </select>
            </div> -->

            <div class="form-group" id="username">
              <input class="form-control" type="text" name="login-user"
              id="login-user" placeholder="Username">
            </div>

            <div class="form-group" id="password">
              <input class="form-control" type="password" name="login-pass"
              id="login-pass" placeholder="Password">
            </div>

            <button class="btn btn-success btn-block" type="submit" id="btn-login">
              Masuk
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Instansi -->
  <div class="row">
    <div class="col-md-12">
      <div id="txt-instansi">
        <h3><font size="3">SMP NEGERI 33 SEMARANG<br>
          Jl. Kompol R. Soekanto Mangunharjo, Tembalang Semarang, Jateng 50272<br>
          (024)76580644 smpn33semarang@yahoo.co.id www.smpn33smg.sch.id
        </font></h3>
      </div>
    </div>
  </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="mdl-not" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Notifikasi</h4>
      </div>
      <div class="modal-body bg-info">
        <p id="psn-not"></p>
      </div>
      <div class="modal-footer bg-info">
        <button type="submit" class="btn btn-danger" data-dismiss="modal">
          Tutup
        </button>
      </div>
    </div>
  </div>
</div>

<!-- Trigger Modal Notifikasi -->
<div id="knt-not"></div>
</div>

<!-- Script -->
<script>
$(document).ready(function(){
    // Submit form login dgn ajax
    $("#form-ajx-login").submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            dataType: "html",
            success: function(data){
                $(".modal").modal("hide");
                $("#knt-not").html(data);
            }
        });
    });
});
</script>

<!-- Body Closing -->
</body>
</html>
