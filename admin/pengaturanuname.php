<?php
$index = "index";
include("header.php");
?>
<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
  require_once('../koneksi.php');
  $dbedit = new mysqli($db_host, $db_user, $db_pass, $db_name);
  if ($db->connect_errno){
    die ("Could not connect to the database: <br />". $dbedit->connect_error);
  }


  
  if (!isset($_POST["edit_post"])){
    $queryget = " SELECT * FROM users WHERE users.username='".$_SESSION["user"]."' ";
    // Execute the query
    $resultget = $dbedit->query( $queryget );
    if (!$resultget){
      die ("Kesalahan database: <br />". $dbedit->error);
    }else{
      while ($rowget = $resultget->fetch_object()){
        $username = $rowget->username;
      }
    }
    
  }else{
    $username = test_input($_POST['username']);
    if($username != "'"){
      require_once('../koneksi.php');
      $dbchecks = new mysqli($db_host, $db_user, $db_pass, $db_name);
      $queryduplikasi = "SELECT username FROM users WHERE username='".$username."' AND username NOT IN (SELECT username FROM users WHERE username='".$_SESSION["user"]."') ";
      $resultduplikasi = $dbchecks->query($queryduplikasi);
      $rowduplikasi = $resultduplikasi->fetch_object();
    }
    if ($username == ''){
      $error_username = "Masukkan dahulu Username";
      $valid_username = false;
    }elseif(isset($rowduplikasi)){
      $error_username = "Username sudah digunakan";
      $valid_username = false;
    }elseif($username == "'"){
      $error_username = "Dilarang menggunakan hanya karakter '";
      $valid_username = false;
    }
    else{
      $valid_username = true;
    }
    

    if ($valid_username ){

      $query = " UPDATE users SET username= '".$username."' WHERE username=".$_SESSION["user"]." ";

      // Execute the query
      $result = $dbedit->query( $query );
      if (!$result){
         die ("Could not query the database: <br />". $dbedit->error);
      }else{
        // echo "<script>alert('Data telah diperbarui')</script>";
        // setcookie('pengaturanprofil', 1, time() + (1), "/");
        $_SESSION['user']=$username;
        echo "<script>window.open('pengaturanakun.php','_self')</script>";
        $db->close();
        exit;
      }
    }
  }

  function test_input($data) {
     $data = trim($data);
     $data = stripslashes($data);
     $data = htmlspecialchars($data);
     return $data;
}

?>
<div class="">
            <div class="page-title">
              <div class="title_left">
                <!-- <h3>Kelola Pengguna <small> </small></h3> -->
              </div>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <h2><small>&ensp;</small>Pengaturan Umum Akun </h2>
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">                 
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left " autocomplete="on" action="pengaturanuname.php" method="post" enctype=>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Pengguna</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 <?php if(isset($error_username)){echo "has-error";}?> ">
                            <input class="form-control" type="text" name="username" autofocus value="<?php if (isset($username) && !isset($error_username)) {echo $username;}?>" maxlength="20" placeholder="<?php if(isset($error_username)){echo $error_username;}?>" title="Masukkan Username">
                            </div>
                          </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <!-- <button class="btn btn-primary" type="button">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button> -->
                          <button type="submit" name="edit_post" class="btn btn-success">Selesai</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>

              <br><br>

            </div>
          </div>
        </div>
        <!-- /page content -->

<?php
include("footer.php");
?>
