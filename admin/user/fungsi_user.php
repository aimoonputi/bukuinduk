<?php
include("../../koneksi.php");
// ==================================================================
// PROSES SUBMIT
// ==================================================================
if (isset($_POST["submit"])) {
  $username = test_input($_POST['username']);
  $nama = test_input($_POST['name']);
  $password = test_input($_POST['password']);
  $jenis = test_input($_POST['jenis']);
  if ($username == "") {
    $error_username = "username harus diisi";
    $valid_username = FALSE;
  }else {
    $valid_username = TRUE;
  }
  if ($nama == "") {
    $error_nama = "nama harus diisi";
    $valid_nama = FALSE;
  }else {
    $valid_nama = TRUE;
  }
  if ($password == "") {
    $error_password = "password harus diisi";
    $valid_password = FALSE;
  }else {
    $valid_password = TRUE;
  }

  if ($valid_nama && $valid_password && $valid_username) {
    $username = $conn->real_escape_string($username);
    $nama = $conn->real_escape_string($nama);
    $password = $conn->real_escape_string($password);
    $jenis = $conn->real_escape_string($jenis);
    $password = md5($password);

    $query = "INSERT INTO users (username, password, nama, level) 
    VALUES ('".$username."', '".$password."', '".$nama."', '".$jenis."')";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data User Berhasil Ditambah')</script>";
      header("location:lihat_user.php");
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
  }
}

// ==================================================================
// PROSES EDIT
// ==================================================================
if (isset($_POST["ubah-submit"])) {
  $username = test_input($_POST['ubah-username']);
  $nama = test_input($_POST['ubah-nama']);
  // $password = test_input($_POST['ubah-password']);
  $jenis = test_input($_POST['ubah-level']);

  if ($nama == "") {
    $error_nama = "nama harus diisi";
    $valid_nama = FALSE;
  }else {
    $valid_nama = TRUE;
  }
  // if ($password == "") {
  //   $error_password = "password harus diisi";
  //   $valid_password = FALSE;
  // }else {
    // $valid_password = TRUE;
  // }

  if ($valid_nama) {
    $username = $conn->real_escape_string($username);
    $nama = $conn->real_escape_string($nama);
    // $password = $conn->real_escape_string($password);
    $jenis = $conn->real_escape_string($jenis);
    // if ($password!=null) {
    //   $password = md5($password);
    //   $query = "UPDATE users SET password = '$password', nama = '$nama', level = '$jenis' WHERE username = '$username'";
    // }else{
      $query = "UPDATE users SET nama = '$nama', level = '$jenis' WHERE username = '$username'";
    // }

    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data User Berhasil Diubah')</script>";
      header("location:lihat_user.php");
    }else {
      // echo "<script>alert('Data User Gagal Diubah')</script>";
      die("Could not query the database: <br/>". $conn->error);
    }
  }
}

// ==================================================================
// PROSES HAPUS
// ==================================================================
if (isset($_POST["hapus-submit"])) {
  $username = test_input($_POST['hapus-username']);

	$query = "DELETE FROM users WHERE username = '$username'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data User Berhasil Dihapus')</script>";
      header("location:lihat_user.php");
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
}
// ==================================================================
// PROSES RESETs
// ==================================================================
if (isset($_POST["reset-submit"])) {
  $username = test_input($_POST['reset-password']);
  $password = md5($username);
  $query = "UPDATE users set password ='$password' WHERE username = '$username'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Reset Password ".$username." Berhasil')</script>";
      header("location:lihat_user.php");
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
}

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
