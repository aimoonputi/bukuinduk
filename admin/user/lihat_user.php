<?php
  $index = "false";
  include("../header.php");
  include("../../koneksi.php");
?>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar User</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
      <p class="text-muted font-13 m-b-30">
         Admin Buku Induk Siswa dapat menambahkan data user baru (Admin, Kesiswaan dan Siswa) </br>
         &nbsp;&nbsp;&nbsp;* User Admin     : Masukkan NIP sebagai username </br>
         &nbsp;&nbsp;&nbsp;* User Kesiswaan : Masukkan NIP sebagai username </br>
         &nbsp;&nbsp;&nbsp;* User Siswa     : Masukkan NIS sebagai username </br>
      </p>
      <div class="col-md-8">
        <button data-toggle="modal" data-target="#modalTambah" type="button" 
          class="btn btn-success" id="btn-tbh-user" data-toggle="tooltip" title="Menambahkan User">
          <i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Tambah
        </button>
      </div>

      <table id="datatable" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th  style=" text-align: center;" width="5%">No</th>
            <th  style=" text-align: center;" width="20%">Username</th>
            <th  style=" text-align: center;" width="20%">Nama</th>
            <th  style=" text-align: center;" width="15%">Level </th>
            <th  style=" text-align: center;" width="30%">Aksi</th>
          </tr>
        </thead>
        
        <tbody>
        <?php
          require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
          $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
          if ($db->connect_errno) {
            die("Could not connet to database:<br/> ".$db->connect_error);
          }
          $query = "SELECT * FROM users ORDER BY level";
          $result = $db->query($query);
          $i = 1;
        ?>

        <?php
        while ($row = $result->fetch_object())
        {
          echo "<tr>";
            echo "<td align=center>".$i."</td>";
            echo "<td align=center>".$row->username."</td>";
            echo "<td>".$row->nama."</td>";
            echo "<td align=center>".$row->level."</td>";
            if ($row->username == $_SESSION["user"]) {
            echo "<td colspan='3'>";
              
              echo "<button class='btn btn-warning btn-ubah-pengguna'";
              echo "data-username='".$row->username."'";
              echo "data-nama='".$row->nama."'";
              echo "data-level='".$row->level."' disabled>";
              echo "<i class='fa fa-edit'></i> Ubah </button>";

              echo "<button class='btn btn-primary btn-reset-password' disabled>";
              echo "<i class='fa fa-refresh'></i> Reset </button>";

              echo "<button class='btn btn-danger btn-hapus-pengguna'";
              echo "data-username='".$row->username."' disabled>";
              echo "<i class='fa fa-remove'></i> Hapus</button>";

            echo "</td>";
              }else{
            echo "<td colspan='3'>";
              
      			  echo "<button class='btn btn-warning btn-ubah-pengguna'";
      			  echo "data-username='".$row->username."'";
      			  echo "data-nama='".$row->nama."'";
      			  echo "data-level='".$row->level."'>";
      			  echo "<i class='fa fa-edit'></i> Ubah </button>";

              echo "<button class='btn btn-primary btn-reset-password'";
              echo "data-username='".$row->username."'>";
              echo "<i class='fa fa-refresh'></i> Reset </button>";

              echo "<button class='btn btn-danger btn-hapus-pengguna'";
      			  echo "data-username='".$row->username."'>";
      			  echo "<i class='fa fa-remove'></i> Hapus</button>";

            echo "</td>";
            }
          echo "</tr>";
          $i++;
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<?php
  include('modal_user.php');
?>

<script>
$(document).ready(function() {
	// ======================================================
  // INISIALISASI MODAL ADD USER
  // ======================================================
  $("#add-username").keyup(function(){
    var value = $(this).val();
    $("#add-password").val(value);
  })
  // ======================================================
	// INISIALISASI MODAL UBAH USER
	// ======================================================
    $(".btn-ubah-pengguna").on("click", function(){
        var username = $(this).data("username");
        var nama = $(this).data("nama");
        var level = $(this).data("level");

		    $("#ubah-username").val(username);
        $("#ubah-nama").val(nama);
        $("#ubah-level").val(level);

        $("#modal-ubah-pengguna").modal();
    });
	// ======================================================
	// INISIALISASI MODAL HAPUS USER
	// ======================================================
    $(".btn-hapus-pengguna").on("click", function(){
        var username = $(this).data("username");
		$("#hapus-username").val(username);

        $("#modal-hapus-pengguna").modal();
    });
  // ======================================================
  // INISIALISASI MODAL RESET USER
  // ======================================================
    $(".btn-reset-password").on("click", function(){
        var username = $(this).data("username");
        $("#reset-password").val(username);
        $("#modal-reset-password").modal();
    });
});
</script>

<?php
include("../footer.php");
?>