<?php
  $index = "false";
  include("../header.php");
  include("../../koneksi.php");
?>

<?php
if (isset($_POST["submit"])) {
  $username = test_input($_POST['username']);
  $nama = test_input($_POST['name']);
  $password = test_input($_POST['password']);
  $jenis = test_input($_POST['jenis']);
  
  $panjang_username = 18; 
  if (strlen($username)!=$panjang_username) {
   echo "<script>alert('Harap Cek format NIP (Contoh: 196012201985031014)')</script>";
   $valid_username = FALSE;
  }elseif (!preg_match("/^[0-9]*$/",$username)) {
   echo "<script>alert('NIP hanya mengizinkan angka')</script>";
  	$valid_username=FALSE;
  }else {
    $valid_username = TRUE;
  }

  if ($nama == "") {
    // $error_nama = "nama harus diisi";
    $valid_nama = FALSE;
  }elseif (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
      echo "<script>alert('Format Nama hanya mengizinkan huruf dan spasi')</script>";
      $valid_nama=FALSE;
  }else {
    $valid_nama = TRUE;
  }

  if ($password == "") {
    $error_password = "password harus diisi";
    $valid_password = FALSE;
  }else {
    $valid_password = TRUE;
  }

  if ($valid_nama && $valid_password && $valid_username) {
    $username = $conn->real_escape_string($username);
    $nama = $conn->real_escape_string($nama);
    $password = $conn->real_escape_string($password);
    $jenis = $conn->real_escape_string($jenis);
    $password = md5($password);

    $query = "INSERT INTO users (username, password, nama, level) 
    VALUES ('".$username."', '".$password."', '".$nama."', '".$jenis."')";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data User Berhasil Ditambah')</script>";
      // header("location:lihat_user.php");
    }else {
      // die("Could not query the database: <br/>". $conn->error);
    	echo "<script>alert('Username Sudah digunakan')</script>";
    }
  }
}

// ==================================================================
// PROSES EDIT
// ==================================================================
if (isset($_POST["ubah-submit"])) {
  $username = test_input($_POST['ubah-username']);
  $nama = test_input($_POST['ubah-nama']);
  // $password = test_input($_POST['ubah-password']);
  $jenis = test_input($_POST['ubah-level']);

  if ($nama == "") {
    $error_nama = "nama harus diisi";
    $valid_nama = FALSE;
  }elseif (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
      echo "<script>alert('Format Nama hanya mengizinkan huruf dan spasi')</script>";
      $valid_nama=FALSE;
  }else {
    $valid_nama = TRUE;
  }
  // if ($password == "") {
  //   $error_password = "password harus diisi";
  //   $valid_password = FALSE;
  // }else {
    // $valid_password = TRUE;
  // }

  if ($valid_nama) {
    $username = $conn->real_escape_string($username);
    $nama = $conn->real_escape_string($nama);
    // $password = $conn->real_escape_string($password);
    $jenis = $conn->real_escape_string($jenis);
    // if ($password!=null) {
    //   $password = md5($password);
    //   $query = "UPDATE users SET password = '$password', nama = '$nama', level = '$jenis' WHERE username = '$username'";
    // }else{
      $query = "UPDATE users SET nama = '$nama', level = '$jenis' WHERE username = '$username'";
    // }

    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data User Berhasil Diubah')</script>";
      // header("location:lihat_user.php");
    }else {
      // echo "<script>alert('Data User Gagal Diubah')</script>";
      die("Could not query the database: <br/>". $conn->error);
    }
  }
}

// ==================================================================
// PROSES HAPUS
// ==================================================================
if (isset($_POST["hapus-submit"])) {
  $username = test_input($_POST['hapus-username']);

	$query = "DELETE FROM users WHERE username = '$username'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Data User Berhasil Dihapus')</script>";
      // header("location:lihat_user.php");
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
}
// ==================================================================
// PROSES RESETs
// ==================================================================
if (isset($_POST["reset-submit"])) {
  $username = test_input($_POST['reset-password']);
  $password = md5($username);
  $query = "UPDATE users set password ='$password' WHERE username = '$username'";
    $result = $conn->query($query);
    if ($result) {
      echo "<script>alert('Reset Password ".$username." Berhasil')</script>";
      // header("location:lihat_user.php");
    }else {
      die("Could not query the database: <br/>". $conn->error);
    }
}

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar User</h2>
        <!-- <ul class="pull-right nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul> -->
        <div class="clearfix"></div>
      </div>
      <p class="text-muted font-13 m-b-30">
         Admin Buku Induk Siswa dapat menambahkan data user baru (Admin, Kesiswaan dan Siswa) </br>
         &nbsp;&nbsp;&nbsp;* User Admin     : Masukkan NIP sebagai username </br>
         &nbsp;&nbsp;&nbsp;* User Kesiswaan : Masukkan NIP sebagai username </br>
         <!-- &nbsp;&nbsp;&nbsp;* User Siswa     : Masukkan NIS sebagai username </br> -->
      </p>
      <div class="col-md-8">
        <button data-toggle="modal" data-target="#modalTambah" type="button" 
          class="btn btn-success" id="btn-tbh-user" data-toggle="tooltip" title="Menambahkan User">
          <i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Tambah
        </button>
      </div>

      <table id="datatable" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th  style=" text-align: center;" width="5%">No</th>
            <th  style=" text-align: center;" width="20%">Username</th>
            <th  style=" text-align: center;" width="20%">Nama</th>
            <th  style=" text-align: center;" width="15%">Level </th>
            <th  style=" text-align: center;" width="30%">Aksi</th>
          </tr>
        </thead>
        
        <tbody>
        <?php
          require $_SERVER["DOCUMENT_ROOT"] . "/BukuInduk/koneksi.php";
          $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
          if ($db->connect_errno) {
            die("Could not connet to database:<br/> ".$db->connect_error);
          }
          $query = "SELECT * FROM users ORDER BY level";
          $result = $db->query($query);
          $i = 1;
        ?>

        <?php
        while ($row = $result->fetch_object())
        {
          echo "<tr>";
            echo "<td align=center>".$i."</td>";
            echo "<td align=center>".$row->username."</td>";
            echo "<td>".$row->nama."</td>";
            echo "<td align=center>".$row->level."</td>";
            if ($row->username == $_SESSION["user"]) {
            echo "<td colspan='3'>";
              
              echo "<button class='btn btn-warning btn-ubah-pengguna'";
              echo "data-username='".$row->username."'";
              echo "data-nama='".$row->nama."'";
              echo "data-level='".$row->level."' disabled>";
              echo "<i class='fa fa-edit'></i> Ubah </button>";

              echo "<button class='btn btn-primary btn-reset-password' disabled>";
              echo "<i class='fa fa-refresh'></i> Reset </button>";

              echo "<button class='btn btn-danger btn-hapus-pengguna'";
              echo "data-username='".$row->username."' disabled>";
              echo "<i class='fa fa-remove'></i> Hapus</button>";

            echo "</td>";
              }else{
            echo "<td colspan='3'>";
              
      			  echo "<button class='btn btn-warning btn-ubah-pengguna'";
      			  echo "data-username='".$row->username."'";
      			  echo "data-nama='".$row->nama."'";
      			  echo "data-level='".$row->level."'>";
      			  echo "<i class='fa fa-edit'></i> Ubah </button>";

              echo "<button class='btn btn-primary btn-reset-password'";
              echo "data-username='".$row->username."'>";
              echo "<i class='fa fa-refresh'></i> Reset </button>";

              echo "<button class='btn btn-danger btn-hapus-pengguna'";
      			  echo "data-username='".$row->username."'>";
      			  echo "<i class='fa fa-remove'></i> Hapus</button>";

            echo "</td>";
            }
          echo "</tr>";
          $i++;
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<!-- ======================================================================= -->
<!-- MODAL TAMBAH USER -->
<!-- ======================================================================= -->
<div class="modal fade" id="modalTambah" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Tambah User Baru</h4>
      </div>
      <div class="modal-body">
        <form  class="form-horizontal" method="post" id="form-login">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Username :</label>
              <div class="col-sm-8">
                  <input type="text" name="username" id="add-username" class="form-control" placeholder="Masukkan Username (NIP)" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama :</label>
              <div class="col-sm-8">
                  <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Password :</label>
              <div class="col-sm-8">
                  <input type="text" name="password" id="add-password" class="form-control" placeholder="Password" readonly required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Jenis User :</label>
              <div class="col-sm-8">
                  <select class="form-control" name="jenis">
                    <option value="Admin">Admin</option>
                    <option value="Kesiswaan">Kesiswaan</option>
                    <!-- <option value="Siswa">Siswa</option> -->
                  </select>
              </div>
            </div>
          </div>
      </div>
        <div class="modal-footer">
          <!-- <div class="col-sm-12"> -->
            <!-- <div class="form-group"> -->
              <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- MODAL UBAH USER -->
<!-- ======================================================================= -->
<div class="modal fade" id="modal-ubah-pengguna" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Ubah User</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="form-ubah-pengguna" action="fungsi_user.php">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Username :</label>
              <div class="col-sm-8">
                  <input id="ubah-username" type="text" name="ubah-username" class="form-control" readonly style="cursor: not-allowed;">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama :</label>
              <div class="col-sm-8">
                  <input id="ubah-nama" type="text" name="ubah-nama" class="form-control" placeholder="Masukkan Nama" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Jenis User :</label>
              <div class="col-sm-8">
                  <select id="ubah-level" class="form-control" name="ubah-level">
                    <option value="Admin">Admin</option>
                    <option value="Kesiswaan">Kesiswaan</option>
                    <option value="Siswa">Siswa</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="box-body">
          </div>
      </div>
        <!-- <div class="modal-footer">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
              <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
            </div>
          </div>
        </div> -->
         <div class="modal-footer">
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
               <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- ================================================================ -->
<!-- MODAL HAPUS USER -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-hapus-pengguna" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Hapus User</h4>
      </div>
      <div class="modal-body">
        <h4 style="text-align: center">Apakah anda yakin menghapus user ini?</h4>
      </div>
      <div class="modal-footer">
        <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <form role="form" method="post" class="form-hapus-pengguna" action="fungsi_user.php">
          <input type="hidden" name="hapus-username" id="hapus-username">
          <button type="submit" class="btn btn-primary" name="hapus-submit">Hapus</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">
            Batal
          </button>
        </form>
      </div></div>
    </div>
  </div>
</div>
<!-- ================================================================ -->
<!-- MODAL RESET USER -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-reset-password" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Reset Password</h4>
      </div>
      <div class="modal-body">
        <h4 style="text-align: center">Apakah anda yakin mereset password user ini?</h4>
      </div>
      <div class="modal-footer">
        <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <form role="form" method="post" class="form-hapus-pengguna" action="fungsi_user.php">
          <input type="hidden" name="reset-password" id="reset-password">
          <button type="submit" class="btn btn-primary" name="reset-submit">Reset</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">
            Batal
          </button>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
	// ======================================================
  // INISIALISASI MODAL ADD USER
  // ======================================================
  $("#add-username").keyup(function(){
    var value = $(this).val();
    $("#add-password").val(value);
  })
  // ======================================================
	// INISIALISASI MODAL UBAH USER
	// ======================================================
    $(".btn-ubah-pengguna").on("click", function(){
        var username = $(this).data("username");
        var nama = $(this).data("nama");
        var level = $(this).data("level");

		$("#ubah-username").val(username);
        $("#ubah-nama").val(nama);
        $("#ubah-level").val(level);

        $("#modal-ubah-pengguna").modal();
    });
	// ======================================================
	// INISIALISASI MODAL HAPUS USER
	// ======================================================
    $(".btn-hapus-pengguna").on("click", function(){
        var username = $(this).data("username");
		$("#hapus-username").val(username);

        $("#modal-hapus-pengguna").modal();
    });
  // ======================================================
  // INISIALISASI MODAL RESET USER
  // ======================================================
    $(".btn-reset-password").on("click", function(){
        var username = $(this).data("username");
        $("#reset-password").val(username);
        $("#modal-reset-password").modal();
    });
});
</script>

<?php
include("../footer.php");
?>