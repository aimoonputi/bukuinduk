<!-- ======================================================================= -->
<!-- MODAL TAMBAH USER -->
<!-- ======================================================================= -->
<div class="modal fade" id="modalTambah" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Tambah User Baru</h4>
      </div>
      <div class="modal-body">
        <form  class="form-horizontal" method="post" id="form-login" action="fungsi_user.php">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Username :</label>
              <div class="col-sm-8">
                  <input type="text" name="username" id="add-username" class="form-control" placeholder="Masukkan Username(NIP/NIS)" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama :</label>
              <div class="col-sm-8">
                  <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" oninvalid="this.setCustomValidity('Tolong isi data ini')" oninput="setCustomValidity('')" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Password :</label>
              <div class="col-sm-8">
                  <input type="text" name="password" id="add-password" class="form-control" placeholder="Password" readonly required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Jenis User :</label>
              <div class="col-sm-8">
                  <select class="form-control" name="jenis">
                    <option value="Admin">Admin</option>
                    <option value="Kesiswaan">Kesiswaan</option>
                    <option value="Siswa">Siswa</option>
                  </select>
              </div>
            </div>
          </div>
      </div>
        <div class="modal-footer">
          <!-- <div class="col-sm-12"> -->
            <!-- <div class="form-group"> -->
              <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- MODAL UBAH USER -->
<!-- ======================================================================= -->
<div class="modal fade" id="modal-ubah-pengguna" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times</button>
        <h4 class="modal-title">Ubah User</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="form-ubah-pengguna" action="fungsi_user.php">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Username :</label>
              <div class="col-sm-8">
                  <input id="ubah-username" type="text" name="ubah-username" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama :</label>
              <div class="col-sm-8">
                  <input id="ubah-nama" type="text" name="ubah-nama" class="form-control" placeholder="Masukkan Nama" required>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Jenis User :</label>
              <div class="col-sm-8">
                  <select id="ubah-level" class="form-control" name="ubah-level">
                    <option value="Admin">Admin</option>
                    <option value="Kesiswaan">Kesiswaan</option>
                    <option value="Siswa">Siswa</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="box-body">
          </div>
      </div>
        <!-- <div class="modal-footer">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
              <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
            </div>
          </div>
        </div> -->
         <div class="modal-footer">
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <button type="submit" class="btn btn-primary" name="ubah-submit">Simpan</button>
               <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-bottom:5px">Batal</button>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- ================================================================ -->
<!-- MODAL HAPUS USER -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-hapus-pengguna" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Hapus User</h4>
      </div>
      <div class="modal-body">
        <h4 style="text-align: center">Apakah anda yakin menghapus user ini?</h4>
      </div>
      <div class="modal-footer">
        <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <form role="form" method="post" class="form-hapus-pengguna" action="fungsi_user.php">
          <input type="hidden" name="hapus-username" id="hapus-username">
          <button type="submit" class="btn btn-success" name="hapus-submit">Hapus</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">
            Batal
          </button>
        </form>
      </div></div>
    </div>
  </div>
</div>
<!-- ================================================================ -->
<!-- MODAL RESET USER -->
<!-- ================================================================ -->
<div class="modal fade" id="modal-reset-password" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Reset Password</h4>
      </div>
      <div class="modal-body">
        <h4 style="text-align: center">Apakah anda yakin mereset password user ini?</h4>
      </div>
      <div class="modal-footer">
        <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <form role="form" method="post" class="form-hapus-pengguna" action="fungsi_user.php">
          <input type="hidden" name="reset-password" id="reset-password">
          <button type="submit" class="btn btn-primary" name="reset-submit">Reset</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">
            Batal
          </button>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>