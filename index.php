<?php
  include("base_url.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css")?>" rel="stylesheet">
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font/4.3.0/css/font-awesome.min.css"> -->
    <title>Portal SMPN 33 Semarang</title>
    <style type="text/css">
      .jumbotron {
          position: relative;
          background: url(<?php echo base_url("assets/images/proro.jpg") ?>) center center;
          color:#fff;
          width: 100%;
          height: 100%;
          /*background-color: #000000a6;*/
          background-size: cover;
          overflow: hidden;
      }
      .index{
        position: relative;
        height: 100vh;
      }
      .navbar{
        padding-right: 20px;
        padding-left: 20px;
      }
      .index-padding{
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        padding-left: 35px;
        padding-right: 20px;
      }
      .index-padding-bis{
        padding-left: 40px;
        padding-right: 20px;
      }
      .index-padding-er{
        padding-left: 40px;
        padding-right: 20px;
      }
    </style>
  </head>

  <body>
    <div class="bg-index">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#target-list">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="index.php" class="navbar-brand"> Portal SMP 33 Semarang </a>
      </div>

      <div class="collapse navbar-collapse " id="target-list">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href=""<?php echo base_url("index.php")?>""> Home </a></li>
          <li ><a href="<?php echo base_url("login.php")?>" > Buku Induk Siswa </a></li>
          <li><a href="#"> E-Raport </a></li>
        </ul>
      </div>
    </nav> 

    <div class="jumbotron index";">
      <div class="index-padding">
        <h1>Selamat Datang di Portal<br>
        SMPN 33 Semarang</h1>
        <p> Portal SMPN 33 Semarang menyediakan layanan untuk menunjang <br>
            kegiatan sekolah dibidang kesiswaan dan guru-guru <br>
            dalam merekap nilai raport siswa<br>
            Terdiri dari Buku Induk Siswa dan E-Raport </p>
      </div>
    </div>

  <!--  <div class="container"> -->
      <div class="jumbotron ">
        <div class="index-padding-bis">
        <h1>Buku Induk Siswa</h1>
        <p> Membantu Tata Usaha bagian Kesiswaan dalam melakukan<br>
            rekap data siswa selama bersekolah di SMPNN 33 Semarang</p>
        <a href="<?php echo base_url("login.php")?>" class="btn btn-success" role="button"> Masuk </a>
      </div>
      </div>
  <!--  </div> -->

  <!--  <div class="container"> -->
      <div class="jumbotron">
        <div class="index-padding-er">
        <h1>E-Raport</h1>
        <p> Membantu Guru-guru dalam melakukan Penilaian Pembelajaran dan<br>
            Membuat Nilai Raport siswa di SMPNN 33 Semarang</p>
        <a href="#" class="btn btn-success" role="button"> Masuk </a>
        </div>
      </div>
    </div>
  <!--  </div> -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo base_url("assets/js/bootstrap.js")?>"></script>
  </body>
</html>
